require_relative "helper"

feature "Bookmark Management" do
  # You are are on the Object page
  background do
    @object = Occam::Object.create(:name => "foo", :type => "bar", :account => admin)
    @object.setPermission("read", "true")
    @object.setPermission("write", "true")
  end

  scenario "Bookmark an object while logged in" do
    login_as_user

    visit @object.url

    # There should be a way to create a bookmark
    assert page.has_css?("h1 li.bookmark form input.bookmark"),
      "Bookmark button not present"
  end

  scenario "Cannot bookmark an object while not logged in" do
    visit @object.url

    # There should not be a bookmark button
    assert page.has_no_css?("h1 li.bookmark form input.bookmark"),
      "Bookmark button present"
  end

  scenario "Cannot remove an existing bookmark while not logged in" do
    visit @object.url

    # There should not be a delete bookmark button
    assert page.has_no_css?("h1 li.bookmark_delete form input.bookmark_delete"),
      "Remove bookmark button present"
  end

  scenario "Remove your own existing bookmark while logged in" do
    # There should be a delete bookmark button
    login_as_user

    visit @object.url

    # There should be a way to remove a bookmark
    assert page.has_css?("h1 li.bookmark_delete form input.bookmark_delete"),
      "Remove bookmark button not present"
  end

  scenario "Cannot remove a bookmark that belongs to someone else" do
    visit @object.url

    # TODO: wait. this isn't exactly right... gotta log in, log out, log in as somebody else
    # There should not be a delete bookmark button
    assert page.has_no_css?("h1 li.bookmark_delete form input.bookmark_delete"),
      "Remove bookmark button present"
  end
end
