require_relative '../helper'

# Refer to our fake Occam root
specPath = File.join(File.dirname(__FILE__), "..")
occamPath = File.join(specPath, ".occam")
emptyOccamPath = File.join(specPath, ".empty-occam")

# Ensure we always point to our nonsense Occam root just in case
ENV['OCCAM_ROOT'] = occamPath

# Delete existing occam root
if File.exist?(occamPath)
  puts
  puts "Deleting existing test occam root"
  FileUtils.remove_dir(occamPath)
end

# Clone the occam instance for this test run
puts
print "Cloning an empty occam instance... "
FileUtils.copy_entry emptyOccamPath, occamPath
puts "Done"

# Ensure the new (test-only, fake) configuration takes hold
Occam::Config.configuration(true)

# Run the daemon at the requested port
port = (Occam::Config.configuration['daemon'] || {})['port']
server_port = Occam::Config.configuration['port']

puts
puts "Starting a test daemon on port #{port}"
`OCCAM_ROOT=#{occamPath} occam daemon start --port #{port}`

puts "Waiting for daemon"
sleep 2

puts "Let's do this"

Minitest.after_run do
  # Wait for all the other threads, if they exist
  #puts
  #puts "Waiting on pending threads..."
  #Thread.list.each do |t|
  #  t.join if t != Thread.current
  #end

  puts
  puts "Disconnecting from test daemon on port #{port}"
  daemon = Thread.current[:occam_daemon]
  if daemon
    daemon.disconnect
  end

  puts
  puts "Stopping test daemon on port #{port}"
  `OCCAM_ROOT=#{occamPath} occam daemon stop --port #{port}`
end

puts

silence_warnings do
  require 'capybara'
  require 'capybara/dsl'
  require 'capybara/minitest'
  require 'capybara/minitest/spec'
  require "selenium/webdriver"
end

# Some of this borrowed from:
# https://www.neotericdesign.com/articles/2018/04/running-your-rails-test-suite-with-dockerized-selenium-on-gitlab-ci/

Capybara.server = :puma, { Silent: true }

# When we have a SELENIUM_URL (remote Selenium)
# we need to ensure our test server can be pinged by that remote Selenium.
if ENV['SELENIUM_URL']
  Capybara.server_host = '0.0.0.0'
  Capybara.server_port = server_port

  # Get the application container's IP
  ip = Socket.ip_address_list.detect { |addr| addr.ipv4_private? }.ip_address

  # Use the IP instead of localhost so Capybara knows where to direct Selenium
  if not ENV['SELENIUM_URL'].include?("localhost")
    Capybara.app_host = "http://#{ip}:#{server_port}"
  else
    Capybara.app_host = "http://localhost:#{server_port}"
  end

  puts "Connecting to Selenium:"
  puts "  from: #{Capybara.app_host}"
  puts "    to: #{ENV['SELENIUM_URL']}"
end

Capybara.register_driver :chrome do |app|
  if ENV['SELENIUM_URL']
    # Allow Capybara to connect to the remote Selenium instance
    Capybara::Selenium::Driver.new(app, :browser => :remote,
                                        :url => ENV['SELENIUM_URL'],
                                        :desired_capabilities => Selenium::WebDriver::Remote::Capabilities.chrome)
  else
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end
end

Capybara.register_driver :headless_chrome do |app|
  options = ::Selenium::WebDriver::Chrome::Options.new
  options.add_argument("--headless")
  options.add_argument("--no-sandbox")
  options.add_argument("--disable-dev-shm-usage")
  options.add_argument("--disable-gpu")

  args = %w(headless disable-gpu no-sandbox disable-dev-shm-usage)

  if ENV['SELENIUM_URL']
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      chromeOptions: { args: args }
    )

    # Allow Capybara to connect to the remote Selenium instance
    Capybara::Selenium::Driver.new(app, :browser => :remote,
                                        :options => options,
                                        :url => ENV['SELENIUM_URL'],
                                        :desired_capabilities => capabilities)
  else
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      chromeOptions: { args: args,
                       binary: "/usr/bin/chromium" }
    )

    Capybara::Selenium::Driver.new(app, :browser => :chrome,
                                        :options => options,
                                        :desired_capabilities => capabilities)
  end
end

class AcceptanceTest < MiniTest::Spec
  include Capybara::DSL
  include Capybara::Minitest::Assertions

  instance_eval do
    # Allow scenario/given to be the DSL of choice
    alias :background :before
    alias :given      :let
  end

  def setup
    setup_base

    Capybara.reset_sessions!
    Capybara.default_max_wait_time = 60
    Capybara.ignore_hidden_elements = false
    Capybara.current_driver = :headless_chrome
  end

  def teardown
    teardown_base

    Capybara.reset_sessions!
    Capybara.current_driver = :headless_chrome
  end

  Capybara.app = Occam
  Capybara.register_driver :rack_test do |app|
    Capybara::RackTest::Driver.new(app, :headers => {
      'User-Agent' => 'Capybara'
    })
  end

  extend MiniTest::Spec::DSL

  register_spec_type(self) do |desc, *add|
    add.length > 0 and add[0][:type] == :feature
  end
end

def admin
  ret = nil
  silence_warnings do
    if !defined?(@@admin)
      @@admin = Occam::Account.create("admin", "admin")
    end
    ret = @@admin
  end
  ret
end

# Create the initial administrator account
admin()

# Login as an administrator
def login_as_admin
  login_as(admin(), "admin", "admin")
end

# Login as a basic user
def login_as_user
  ret = nil
  silence_warnings do
    if !defined?(@@user)
      @@username = "user-#{uuid()}"
      @@password = "foobar-#{uuid()}"
      @@user = Occam::Account.create(@@username, @@password)
    end

    ret = login_as(@@user, @@username, @@password)
  end

  ret
end

# Create a new account with the given username and password
# Log in as that account
def login_as_new_account(username, password)
  account = Occam::Account.create(username, password)

  login_as(account, username, password)
end

# Use the given account and attempt to log in with the given username/password
def login_as(account, username, password)
  @account = account
  @person  = @account.person

  @username = username
  @password = password

  visit '/login'

  fill_in 'username', :with => username
  fill_in 'password', :with => password

  click_button "login"

  @person
end

# Allow "feature" to be the DSL of choice

def feature(*args, &block)
  opts = {}
  if args[-1].is_a? Hash
    opts = args.pop
  end
  opts[:type] = :feature
  describe(*args, opts, &block)
end

def scenario(name, *args, &block)
  it(name, *args, &block)
end
