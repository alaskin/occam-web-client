require_relative "helper"

describe Occam::Git do
  describe "#issue" do
    before do
      @acc = new_account()
      @obj = new_object(:account => @acc)
      @git = Occam::Git.new
    end

    it "should pass along the action to the daemon service" do
      Occam::Daemon.any_instance.expects(:execute).with("git", "view", includes("action"), anything, anything, anything).returns({
        :code => 0, :data => {
        }.to_json
      })

      @git.issue(@obj, "action", "foo", "bar")
    end

    it "should pass along the remaining arguments as arguments to the daemon" do
      Occam::Daemon.any_instance.expects(:execute).with("git", "view", all_of(includes("foo"), includes("bar")), anything, anything, anything).returns({
        :code => 0, :data => {
        }.to_json
      })

      @git.issue(@obj, "action", "foo", "bar")
    end

    it "should not pass along any options hashes to the daemon as arguments" do
      Occam::Daemon.any_instance.expects(:execute).with("git", "view", Not(any_of(includes(:bogus), includes(:stdin), includes("barf"), includes("baz"))), anything, anything, anything).returns({
        :code => 0, :data => {
        }.to_json
      })

      @git.issue(@obj, "action", "foo", "bar", :bogus => "barf", :stdin => "baz")
    end

    it "should pass along any given data as the stdin of the daemon call" do
      Occam::Daemon.any_instance.expects(:execute).with("git", "view", anything, anything, "baz", anything).returns({
        :code => 0, :data => {
        }.to_json
      })

      @git.issue(@obj, "action", "foo", "bar", :stdin => "baz")
    end

    it "should use the given object" do
      Occam::Daemon.any_instance.expects(:execute).with("git", "view", includes(@obj.fullID), anything, anything, anything).returns({
        :code => 0, :data => {
        }.to_json
      })

      @git.issue(@obj, "action", "foo", "bar")
    end

    it "should authenticate using the given object's account token" do
      Occam::Daemon.any_instance.expects(:execute).with("git", "view", anything, has_entry("-T", @obj.account.token), anything, anything).returns({
        :code => 0, :data => {
        }.to_json
      })

      @git.issue(@obj, "action", "foo", "bar")
    end

    it "should return the data from the daemon" do
      expected_data = uuid()
      Occam::Daemon.any_instance.stubs(:execute).with("git", "view", anything, anything, anything, anything).returns({
        :code => 0, :data => expected_data
      })

      @git.issue(@obj, "action", "foo", "bar").must_equal expected_data
    end
  end
end
