require_relative "helper"

describe Occam::Controller::Helpers::ConfiguratorHelpers, :type => :helper do
  class HelperTest < HelperContext
    include Occam::Controller::Helpers::ConfiguratorHelpers
  end

  require 'nokogiri'
  require 'base64'

  before do
    @app = HelperTest.new
  end

  describe "resolveKey" do
    it "should return nil if the key is not within the hash" do
      @app.resolveKey({}, "foo").must_be_nil
    end

    it "should return the value of the requested key" do
      @app.resolveKey({"foo" => 42}, "foo").must_equal 42
    end

    it "should return the value of the requested key within a hash" do
      @app.resolveKey({"foo" => {"bar" => 42}}, "foo.bar").must_equal 42
    end

    it "should return the value of the requested key within an array" do
      @app.resolveKey({"foo" => [24, 42, -15]}, "foo[1]").must_equal 42
    end

    it "should return the value of the requested key within an array within an array" do
      @app.resolveKey({"foo" => [[24, -3, -23], [-4, 23, 42], [-3, -15, 72]]}, "foo[1][2]").must_equal 42
    end

    it "should return the value of the requested key within a hash within an array" do
      @app.resolveKey({"foo" => [{"bar" => 24}, {"bar" => 42}, {"bar" => -15}]}, "foo[1].bar").must_equal 42
    end
  end

  describe "render_ranged_nav" do
  end

  describe "render_id" do
  end

  describe "render_nav" do
  end

  describe "render_config" do
  end

  describe "render_binary_form" do
  end

  describe "render_ranged_form" do
  end

  describe "render_item" do
  end
end
