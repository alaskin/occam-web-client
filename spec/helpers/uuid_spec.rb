require_relative "helper"

describe Occam::Controller::Helpers::UUIDHelpers, :type => :helper do
  class HelperTest < HelperContext
    include Occam::Controller::Helpers::UUIDHelpers
  end

  before do
    @app = HelperTest.new
  end

  describe "isUUID" do
    it "should return false when the given uuid is nil" do
      @app.isUUID(nil).must_equal false
    end

    it "should return false when the given uuid is not a string" do
      @app.isUUID(0).must_equal false
    end

    it "should return false when the given string is not a uuid" do
      @app.isUUID("bogus weasel").must_equal false
    end

    it "should return false when the given string is a uuid" do
      @app.isUUID(uuid()).must_equal true
    end
  end

  describe "isHash" do
    it "should return false when the given hash is not a string" do
      @app.isHash(0).must_equal false
    end

    it "should return false when the given hash is nil" do
      @app.isHash(nil).must_equal false
    end

    it "should return false when the given hash is not a valid multihash" do
      @app.isHash("bogus weasel").must_equal false
    end

    it "should return true when the given hash is a valid SHA256 multihash" do
      @app.isHash("QmaMp5GTGtUxVnCFsWsHKWtFbh8GuLJ9hysgUnoqaMC6xw").must_equal true
    end
  end
end
