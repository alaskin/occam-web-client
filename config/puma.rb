require 'puma'

require_relative "../lib/config"
require_relative "../lib/https"

pwd = Dir.pwd

pidfile "#{pwd}/puma.pid"

puts "Running server..."

config = Occam::Config.configuration

host = config["host"] || "0.0.0.0"
port = config["port"] || 9292

workers config["workers"] || 1
threads (config["min-threads"] || 1),
        (config["max-threads"] || 5)

Occam::HTTPS.ensureKey

bind "tcp://#{host}:#{port}?key=#{pwd}/key/server.key&cert=#{pwd}/key/server.crt"
