class Occam
  class System
    class Theme
      include Occam::Controller::Helpers::HashHelpers

      attr_reader :path
      attr_reader :config
      attr_reader :name
      attr_reader :slug

      # Retrieves a list of themes
      def self.list
        # Grab list of theme names
        if !defined?(@@list)
          @@list = {}
          Dir[File.join("config", "themes", "**", "*.yml")].each do |path|
            path = File.basename(path, ".yml")
            if not @@list.has_key?(path)
              @@list[path] = Occam::System::Theme.new(path)
            end
          end
        end

        @@list
      end

      # Loads the theme for the given name
      def initialize(name)
        file_path = File.join("config", "themes", name.to_s + ".yml")
        @config = deep_symbolize_keys(YAML.load_file(file_path) || {})
        @slug = name
        @name = @config[:name] || "Unnamed"
        @path = file_path
      end

      # Returns the HTML hex string for the given key.
      def hexColorFor(key)
        value = self.backgroundColorFor(self.valueFor(key))
        value = Chroma.paint(value).to_hex

        if value.start_with?("#")
          value = value[1..-1]
        end

        value
      end

      # Returns the background color part of the given background value.
      def backgroundColorFor(value)
        value.strip.split(/[ ]+(?![^\(]*[\)])/).first
      end

      # Returns the value for the given key
      def valueFor(key)
        hash = self.config

        key.split(".").each do |k|
          hash = hash[k.intern]
        end

        hash
      rescue
        nil
      end

      def devicePrologue(device)
        case device
        when :"4k"
          "@media \#{$media-4k} {\n"
        else
          ""
        end
      end

      def deviceEpilogue(device)
        case device
        when :"4k"
          "}\n"
        else
          ""
        end
      end

      def mediaPrologue(query)
        if query
          "@media #{query} {\n"
        else
          ""
        end
      end

      def mediaEpilogue(query)
        if query
          "}\n"
        else
          ""
        end
      end

      def _scss(hash, values)
        hash.map do |k, v|
          if values.has_key?(k)
            if v.is_a?(Array)
              v.each_with_index.map do |item, i|
                selector = item[:selector]
                key = item[:key]
                definitions = values[k]

                media = item[:media]

                if !definitions.is_a?(Hash) || !definitions.has_key?(:sd)
                  definitions = { :sd => definitions }
                end

                definitions.each.map do | device, value |
                  if item[:hover]
                    if item[:hover].is_a? String
                      selector.sub!(/(?:\s|^)(#{Regexp.escape(item[:hover])})(\s|[:]|$)/, " \\1:hover\\2")
                    else
                      selector << ":hover"
                    end
                  end

                  if key != "background" && value.include?(" ")
                    # Split by spaces ignoring anything inside parentheses
                    # So, this would ignore any extra 'background' parameters and
                    # potentially get the background color. (It's not perfect)
                    value = self.backgroundColorFor(value)
                  end

                  if item[:transform]
                    value = item[:transform].gsub("$value", value)
                  end

                  # Craft the key sandwiched inside media queries, if any.
                  # The "media" section is defined by the theme configuration generally.
                  # The "device" section are overrides provided by the individual theme.

                  if key == "box-shadow-color"
                    mediaPrologue(media) +
                    devicePrologue(device) +
                    "#{selector} {\n" +
                    "  box-shadow: #{item[:with]} #{value} #{item[:inset] ? "inset" : ""};\n" +
                    "}\n" +
                    deviceEpilogue(device) +
                    mediaEpilogue(media)
                  elsif key == "box-shadow" || key == "text-shadow"
                    mediaPrologue(media) +
                    devicePrologue(device) +
                    "#{selector} {\n" +
                    "  #{key.to_s}: #{value[:style]} #{value[:color]} #{item[:inset] ? "inset" : ""};\n" +
                    "}\n" +
                    deviceEpilogue(device) +
                    mediaEpilogue(media)
                  else
                    mediaPrologue(media) +
                    devicePrologue(device) +
                    "#{selector} {\n" +
                    "  #{key.to_s}: #{value};\n" +
                    "}\n" +
                    deviceEpilogue(device) +
                    mediaEpilogue(media)
                  end
                end.join("\n")
              end.join("\n")
            elsif v.is_a?(Hash)
              _scss(v, values[k])
            end
          end
        end.join("\n")
      end

      # Returns the generated scss for this theme
      def scss
        "/* Contains site theme */\n" +
        "/* stylelint-disable */" +
        "@import \"../../device.scss\";\n" +
        self._scss(self.selectors, self.config) +
        "/* stylelint-enable */"
      end

      # Gives the structure of the theme editor and the possible theme options.
      def selectors
        key_path = File.join("config", "theme.yml")
        ret = (YAML.load_file(key_path) || {})
        deep_deep_symbolize_keys(ret)
      end

      # Yields a list of rows for the given section of the theme.
      #
      # This is used to render the theme configuration table.
      def selectorList(key, subkey)
        self.selectors[key][subkey].map do |tag, value|
          if value.is_a? Hash
            [{tag => 1}, value.map do |a,b|
              if b.is_a? Hash
                [{a => 2}, b.map{|x,y| {x => y}}].flatten
              else
                {a => b}
              end
            end]
          else
            {tag => value}
          end
        end.flatten
      end
    end
  end
end
