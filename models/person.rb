# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  require_relative 'object'

  # Represents a Person object in the Occam system.
  class Person < Occam::Object
    # Creates a Person (group or organization)
    def newPerson(name, subtypes = [])
      arguments  = []
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      cmdOptions["--group"] = name

      subtypes.each do |subtype|
        cmdOptions["--subtype"] ||= []
        cmdOptions["--subtype"] << [subtype]
      end

      result = Occam::Worker.perform("accounts", "new", arguments, cmdOptions)

      if result[:code] != 0
        return nil
      end

      info = {
        :id => result[:data],
        :name => name,
        :type => "person",
        :subtype => subtypes
      }

      Occam::Person.new(:info => info, :id => info[:id], :account => @account)
    end

    # Returns True if this Person has bookmarked this Object.
    def bookmarked?(object)
      (self.linksTo(object)["bookmark"] || []).map(&:target).map(&:id).include?(object.id)
    end

    def bookmarkFor(object)
      (self.linksTo(object)["bookmark"] || [])[
        (self.linksTo(object)["bookmark"] || []).map(&:target).map(&:id).index(object.id)
      ]
    end

    # Returns the association metadata for the given object.
    def associationFor(object)
    end

    # Returns all associations known to this Person.
    def associations(kind)
      arguments  = [kind]
      cmdOptions = {
        "-f" => true,
      }

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("associations", "list", arguments, cmdOptions)
      list = JSON.parse(result[:data], :symbolize_names => true) || {}

      list.each do |type, values|
        (values || {}).each do |subtype, associations|
          associations.each do |info|
            info[:object] = Occam::Object.new(:id => info[:object][:id], :uid => info[:object][:uid], :revision => info[:revision], :info => info[:object], :account => @account, :tokens => @tokens)
          end
        end
      end

      list
    end

    # Adds an association between object and the given options
    def addAssociation(kind, major, object, options)
      arguments  = [object.fullID, kind, major]
      cmdOptions = {
      }

      if options[:minor]
        cmdOptions["-n"] = options[:minor]
      end

      if @account
        cmdOptions["-T"] = @account.token
      end

      Occam::Worker.perform("associations", "new", arguments, cmdOptions)
    end

    # Removes an existing association between object and the given options
    def removeAssociation(kind, major, object, options)
      arguments  = [object.fullID, kind, major]
      cmdOptions = {
      }

      if options[:minor]
        cmdOptions["-n"] = options[:minor]
      end

      if @account
        cmdOptions["-T"] = @account.token
      end

      Occam::Worker.perform("associations", "delete", arguments, cmdOptions)
    end

    # Returns the TrustAssociation metadata for the given object for this Person.
    def trustAssociationFor(object)
    end

    # Returns True if the Person trusts the given object.
    def trust?(object)
    end

    def has_avatar?
      self.info[:images] && self.info[:images].is_a?(Array) && self.info[:images].length > 0
    end

    # Returns the avatar image url for the Person for the given size.
    def avatar_url(size = 48)
      if self.has_avatar?
        self.objectURL(:path => "raw/#{self.info[:images][0]}")
      else
        Occam::Person.default_avatar_url(self.id || "anonymous", size)
      end
    end

    # Returns the avatar data
    def avatar(size = 48)
      if self.has_avatar?
        self.retrieveFile(self.info[:images][0])
      else
        Occam::Person.default_avatar(self.id || "anonymous", size)
      end
    end

    # Returns an Occam::Account if the Person is authorized on our system.
    def account
      if self.status.has_key? :account
        Occam::Account.new(:person => self)
      else
        nil
      end
    end

    # Returns the list of bookmarks for this Person.
    def bookmarks
      self.links("bookmark")
    end

    # Creates a new bookmark
    #
    # Returns:
    #   The link identifier.
    def createBookmark(object)
      self.createLink(:relationship => "bookmark",
                      :object       => object)
    end

    # Returns the list of recently used objects for this Person.
    def recentlyUsed
      self.links("recently-used")
    end

    # Stores a new recently used link.
    #
    # Returns:
    #   The link identifier.
    def createRecentlyUsed(object)
      self.createLink(:relationship => "recently-used",
                      :object       => object,
                      :limit        => 10)
    end

    # Returns the list of active objects/worksets for this Person.
    def active
      self.links("active", :order => :descending, :force => true)
    end

    # Returns a list of running tasks assigned to this Person.
    def running_runs
      []
    end

    # Returns a list of Workset objects that are owned by this Person.
    def worksets
      []
    end

    def collaborations
      []
    end

    def dashboard_runs
      []
    end

    # Returns the public key for this Person.
    #
    # This key is used to verify verification keys.
    def publicKey(options = {})
      self.identity.publicKey(options)
    end

    # Returns all known public keys for this Person.
    def publicKeys(options = {})
      self.identity.publicKeys(options)
    end

    # Returns all public verification keys for this Person.
    def verifyKeys(options = {})
      self.identity.verifyKeys(options)
    end

    # Returns the public verification key for this Person.
    #
    # This key is used to verify signatures.
    def verifyKey(options = {})
      self.identity.verifyKey(options)
    end

    # Gather Person objects for each member of this group
    def members
      arguments  = [self.identity]
      cmdOptions = {
        "-j" => true
      }

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("accounts", "list", arguments, cmdOptions)
      list = JSON.parse(result[:data], :symbolize_names => true)

      return (list[:members] || []).map do |info|
        Occam::Person.new(:info => info, :id => info[:id], :revision => info[:revision], :account => @account)
      end
    end

    # Gather Person objects for the groups this person is a direct member of.
    def memberOf
      arguments  = [self.identity]
      cmdOptions = {
        "-j" => true
      }

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("accounts", "list", arguments, cmdOptions)
      list = JSON.parse(result[:data], :symbolize_names => true)

      return (list[:memberOf] || []).map do |info|
        Occam::Person.new(:info => info, :id => info[:id], :revision => info[:revision], :account => @account)
      end
    end

    # Preserve old Object#url method for other things
    if not Person.method_defined? :objectURL
      alias_method :objectURL, :url
    end

    def addMember(member)
      arguments  = [member.identity, self.identity]
      cmdOptions = {
        "--role" => "member"
      }

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("accounts", "add", arguments, cmdOptions)
      JSON.parse(result[:data], :symbolize_names => true)
    end

    def removeMember(member)
      arguments  = [member.identity, self.identity]
      cmdOptions = {
        "--role" => "member"
      }

      if @account
        cmdOptions["-T"] = @account.token
      end

      Occam::Worker.perform("accounts", "remove", arguments, cmdOptions)
    end

    # Updates the given Account to use this Person object
    def setForAccount(identity)
      arguments  = []
      cmdOptions = {
        "-i" => identity,
        "-p" => self.fullID
      }

      if @account
        cmdOptions["-T"] = @account.token
      end

      Occam::Worker.perform("accounts", "update", arguments, cmdOptions)
    end

    # Override url for a person to the dashboard specific views
    def url(options = {})
      ret = "/people/#{self.identity.uri}"

      if options[:path]
        ret = "#{ret}/#{options[:path]}"
      end

      ret
    end

    # Returns the default avatar url for the given size.
    def self.default_avatar_url(name, size)
      if !defined?(@@avatar_url_cache)
        @@avatar_url_cache = {}
      end

      token = name.to_s + size.to_s
      if @@avatar_url_cache[token].nil?
        @@avatar_url_cache[token] = begin
          image_data = self.default_avatar(name, size)
          if image_data
            image_data = Base64.encode64(image_data)
            "data:image/png;base64,#{image_data}"
          else
            # The plain default avatar image fallback
            "/images/icons/objects/person.svg"
          end
        end
      end

      @@avatar_url_cache[token]
    end

    # Returns the default avatar image for the given size.
    def self.default_avatar(name, size)
      # Will select from different avatar generators that it may find installed
      if defined?(::Avatarly)
        # TODO: hash background color
        image_options = {:size => size, :background_color => "#bbccdd"}
        Avatarly.generate_avatar(name, image_options)
      elsif defined?(::RubyIdenticon)
        image_options = {}
        RubyIdenticon.create(name, image_options)
      else
        nil
      end
    end

    def self.roleIconURLFor(role, options={})
      role = role.to_s.gsub("/", "-").gsub("+", "-")

      # Detect if the icon exists for this role
      basePath = "/images/dynamic"
      if options[:color]
        basePath = basePath + "/color/#{options[:color]}"
      elsif options[:hue]
        basePath = basePath + "/hue/#{options[:hue]}/sat/#{options[:sat]}/light/#{options[:light]}"
      elsif options[:hex]
        basePath = basePath + "/hex/#{options[:hex]}"
      else
        basePath = "/images"
      end

      publicPath = File.join("public", "images", "icons", "roles", role)

      relativePath = nil

      if options[:small] && File.exist?(publicPath + ".small.svg")
        relativePath = "/icons/roles/#{role}.small.svg"
      elsif options[:small] && File.exist?(publicPath + ".small.png")
        relativePath = "/icons/roles/#{role}.small.png"
      elsif File.exist?(publicPath + ".svg")
        relativePath = "/icons/roles/#{role}.svg"
      elsif File.exist?(publicPath + ".png")
        relativePath = "/icons/roles/#{role}.png"
      else
        relativePath = "/icons/roles/default.svg"
        if !options[:relative]
          relativePath = "#{basePath}#{relativePath}"
        end
        relativePath = options[:default] || relativePath
      end

      if !options[:relative]
        "#{basePath}#{relativePath}"
      else
        relativePath
      end
    end

    def self.personInfoFor(uri, options={})
      arguments  = [uri]
      cmdOptions = {}

      if options[:account]
        cmdOptions["-T"] = options[:account].token
      end

      cmdOptions["-j"] = true

      result = Occam::Worker.perform("people", "status", arguments, cmdOptions)
      JSON.parse(result[:data], :symbolize_names => true) || {}
    rescue Exception => _
      nil
    end

    def self.fromIdentity(uri, options={})
      info = Person.personInfoFor(uri)
      if info && info.has_key?(:id)
        if info.has_key?(:account)
          info.delete(:account)
        end
        Occam::Person.new(info.update(options))
      else
        nil
      end
    end
  end
end
