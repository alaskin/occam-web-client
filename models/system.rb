# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class System
    require 'json'

    INFO_CACHE_TIME = 120.0 # Time in seconds to keep system information

    def self.daemonPath(path = nil)
      if not defined?(@@daemonPath)
        @@daemonPath = File.realpath(File.join(File.realpath(`which occam`.strip), "..", ".."))
      end

      ret = @@daemonPath

      if path
        ret = File.realpath(File.join(@@daemonPath, path))

        if not ret.start_with?(@@daemonPath)
          ret = nil
        end
      end

      ret
    end

    def self.local
      if !defined?(@@local)
        @@local = Occam::System.new
      end
      @@local
    end

    # Creates an instance that represents the System.
    def initialize(options = {})
      @account = options[:account]
      @last = nil
    end

    def info
      if @last.nil? || (Time.now - @last) > INFO_CACHE_TIME
        cmdOptions = {}

        if @account
          cmdOptions["-T"] = @account.token
        end

        result = Occam::Worker.perform("system", "view", [], cmdOptions)
        @info = JSON.parse(result[:data], :symbolize_names => true)

        @last = Time.now
      end

      @info['domain'] = Occam.domain

      @info || {}
    end

    def hasComponent?(component)
      (self.info[:components] || []).include? component
    end
  end
end
