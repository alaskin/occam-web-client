require_relative 'object'

class Occam
  class Build < Occam::Object
    attr_reader :object

    def initialize(options = {})
      @object = options[:object]
      options[:account] = options[:account] || @object.account

      super(options)
    end

    # Retrieve the build log (the console output for that build)
    def log(options = {})
      arguments  = [@object.shortID, self.fullID]
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      if @object.tokens && @object.tokens.any?
        cmdOptions["-T"] = @object.tokens[0]
      end

      result = Occam::Worker.perform("builds", "view", arguments, cmdOptions, nil, options[:stream])

      if options[:stream]
        result
      else
        result[:data]
      end
    end

    # Builds cannot be edited, so this will return false.
    def canEdit?
      false
    end

    def canView?
      @object.canView?
    end

    def canClone?
      @object.canClone?
    end

    def canExecute?
      @object.canExecute?
    end

    # Determines if the build task object exists.
    def exists?
      self.as(Occam::Object).exists?
    end

    # Returns the node metadata for the stored object.
    def status
      if !defined?(@status) || @status.nil?
        arguments  = [@object.shortID, self.fullID]
        cmdOptions = {
          "-j" => true,  # Report as JSON
        }

        if @account
          cmdOptions["-T"] = @account.token
        end

        if @object.tokens && @object.tokens.any?
          cmdOptions["-T"] = @object.tokens[0]
        end

        begin
          result = Occam::Worker.perform("builds", "status", arguments, cmdOptions)
        rescue Occam::Daemon::Error => e
          result = e.response
        end

        if result[:code] != 0
          return nil
        end

        @status = JSON.parse(result[:data], :symbolize_names => true)
      end

      @status
    end

    # Determines a URL to something within this build.
    def url(options = {})
      @object.url(:path => "builds/#{self.id}/#{options[:path]}")
    end

    # Retrieves a file from the object store from the given path.
    def retrieveFileStat(path)
      if !defined?(@fileStats)
        @fileStats = {}
      end

      if !@fileStats.has_key?(path)
        arguments  = [@object.shortID, self.fullID(path)]
        cmdOptions = {
          "-j" => true,
        }

        if @account
          cmdOptions["-T"] = @account.token
        end

        if @object.tokens && @object.tokens.any?
          cmdOptions["-T"] = @object.tokens[0]
        end

        result = Occam::Worker.perform("builds", "status", arguments, cmdOptions)

        @fileStats[path] = JSON.parse(result[:data], :symbolize_names => true)
      end

      @fileStats[path]
    rescue
      nil
    end

    # Retrieves a file from the object store from the given path.
    def retrieveFile(path, options={})
      arguments  = [@object.shortID, self.fullID(path)]
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      if @object.tokens && @object.tokens.any?
        cmdOptions["-T"] = @object.tokens[0]
      end

      if options[:compress]
        cmdOptions["-c"] = options[:compress]
      end

      if options[:start]
        cmdOptions["-s"] = options[:start]
      end

      if options[:length]
        cmdOptions["-l"] = options[:length]
      end

      result = Occam::Worker.perform("builds", "view", arguments, cmdOptions, nil, options[:stream])

      if options[:stream]
        result
      else
        result[:data]
      end
    end

    # Lists the given directory from the object store.
    def retrieveDirectory(path)
      if !defined?(@directories)
        @directories = {}
      end

      if !@directories.has_key?(path)
        arguments  = [@object.fullID, self.fullID(path)]
        cmdOptions = {
          "-j" => true,
          "-l" => true,
        }

        if @account
          cmdOptions["-T"] = @account.token
        end

        if @object.tokens && @object.tokens.any?
          cmdOptions["-T"] = @object.tokens[0]
        end

        result = Occam::Worker.perform("builds", "list", arguments, cmdOptions)
        @directories[path] = JSON.parse(result[:data], :symbolize_names => true)
      end

      @directories[path]
    rescue
      []
    end

    # Retrieves a JSON document from the object store at the given path.
    #
    # Returns nil if there is any error
    def retrieveJSON(path)
      arguments  = [@object.shortID, self.fullID(path)]
      cmdOptions = {
        #"-a" => true,
      }

      if @account
        cmdOptions["-T"] = @account.token
      end

      if @object.tokens && @object.tokens.any?
        cmdOptions["-T"] = @object.tokens[0]
      end

      result = Occam::Worker.perform("builds", "view", arguments, cmdOptions)

      JSON.parse(result[:data], :symbolize_names => true)
    rescue
      nil
    end

    # Determines if the file at the given path is likely text data.
    def isText?(path)
      if path.nil?
        return false
      end

      type = self.fileType(path)
      media_type = type.split("/")[0]

      if type && media_type == "text"
        true
      elsif type && TEXT_MEDIA_TYPES.include?(type)
        true
      elsif self.info[:subtype] == "text/plain" || (self.info[:subtype].is_a?(Array) && self.info[:subtype].include?("text/plain"))
      else
        # Query the analysis engine to see what type of file it is
        arguments  = [@object.fullID, self.fullID(path)]
        cmdOptions = {
          "-a" => true
        }

        if @account
          cmdOptions["-T"] = @account.token
        end

        if @object.tokens && @object.tokens.any?
          cmdOptions["-T"] = @object.tokens[0]
        end

        result = Occam::Worker.perform("analysis", "is-text", arguments, cmdOptions)
        JSON.parse(result[:data], :symbolize_names => true)[:result]
      end
    end
  end
end
