# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Identity
    attr_reader :uri

    def initialize(uri, options = {})
      @uri = uri
    end

    # Returns the public key for this Identity.
    #
    # This key is used to verify verification keys.
    def publicKey(options = {})
      arguments = [self.uri]
      cmdOptions = {}

      if options[:encoding]
        cmdOptions["-e"] = options[:encoding]
      end

      if options[:verifyKey]
        cmdOptions["-v"] = true
      end

      if options[:verifyKeys] || options[:all]
        cmdOptions["-a"] = true
      end

      result = Occam::Worker.perform("keys", "view", arguments, cmdOptions)

      if options[:verifyKeys]
        result = JSON.parse(result[:data], :symbolize_names => true)
        result[:verifyingKeys]
      elsif options[:all]
        result = JSON.parse(result[:data], :symbolize_names => true)
        [{:key => result[:publicKey]}]
      else
        result[:data]
      end
    end

    # Returns the public verification key for this Identity.
    #
    # This key is used to verify signatures.
    def verifyKey(options = {})
      self.publicKey(options.update(:verifyKey => true))
    end

    # Returns all public verification keys for this Identity.
    def verifyKeys(options = {})
      self.publicKey(options.update(:verifyKeys => true))
    end

    # Returns all public keys for this Identity.
    def publicKeys(options = {})
      self.publicKey(options.update(:all => true))
    end

    def info
      arguments = [self.uri]

      cmdOptions = {
        "-a" => true
      }

      result = Occam::Worker.perform("keys", "view", arguments, cmdOptions)
      JSON.parse(result[:data], :symbolize_names => true)
    end

    # Returns the Occam::Person that relates to this Identity, if known.
    def person
      if !defined?(@person)
        @person = Person.fromIdentity(self.uri)
      end

      @person
    end
  end
end
