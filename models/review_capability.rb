# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class ReviewCapability
    require 'date'
    require 'json'

    attr_reader :id

    attr_reader :published

    attr_reader :object
    attr_reader :account

    # Creates an instance to represent an existing review link.
    #
    # Does not actually create a new review link. See #create.
    def initialize(options)
      @id        = options[:id]
      @published = options[:published] || Date.today()
      @object    = options[:object]
      @account   = options[:account]
    end

    # Creates a new ReviewCapability for the given Object.
    #
    # It will use the revision of the given object to create the link.
    #
    # Returns: The new Occam::ReviewCapability.
    def self.create(object)
      arguments  = [object.fullID]
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("permissions", "new", arguments, cmdOptions)
      data = JSON.parse(result[:data], :symbolize_names => true)

      Occam::ReviewCapability.new(:object    => object,
                                  :id        => data[:id],
                                  :published => DateTime.iso8601(data[:published]),
                                  :account   => object.account)
    end

    # Destroys this review link
    def destroy!
      arguments  = [self.object.fullID]
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      Occam::Worker.perform("permissions", "remove", arguments, cmdOptions)
    end
  end
end
