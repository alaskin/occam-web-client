# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # Represents a scheduled Job for the local system.
  class Job
    attr_reader :id

    INFO_CACHE_TIME = 2.0 # Time in seconds to keep status information

    # Creates a new instance of the given job
    #
    # options:
    #   id:      The identifier for the job
    #   account: The Occam::Account that authorizes any action
    def initialize(options)
      @id      = options[:id]
      @account = options[:account]

      @taskInfo    = nil
      @networkInfo = nil
      @last        = nil
      @info        = nil
    end

    # Sends data to the running job's stdin
    def send(data)
      arguments  = [self.id, "-"]
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      Occam::Worker.perform("jobs", "send", arguments, cmdOptions, data)
    end

    # Send a signal to the running process
    #
    # number: The integer number of the signal.
    def signal(number)
      arguments  = [self.id, number.to_s]
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      Occam::Worker.perform("jobs", "signal", arguments, cmdOptions, data)
    end

    def networkInfo
      if !@networkInfo
        arguments  = [self.id]
        cmdOptions = {}

        cmdOptions["--network"] = true

        if @account
          cmdOptions["-T"] = @account.token
        end

        result = Occam::Worker.perform("jobs", "view", arguments, cmdOptions)
        begin
          @networkInfo = JSON.parse(result[:data], :symbolize_names => true)
        rescue
        end
      end

      @networkInfo
    end

    def taskInfo
      if !@taskInfo
        arguments  = [self.id]
        cmdOptions = {}

        cmdOptions["--task"] = true

        if @account
          cmdOptions["-T"] = @account.token
        end

        result = Occam::Worker.perform("jobs", "view", arguments, cmdOptions)
        begin
          @taskInfo = JSON.parse(result[:data], :symbolize_names => true)
        rescue
        end
      end

      @taskInfo
    end

    # Opens a direct connection to the job
    def connect(options={})
      arguments  = [self.id]
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      Occam::Worker.perform("jobs", "connect", arguments, cmdOptions, nil, true)
    end

    # Reads the stdout for the job
    def log(options={})
      arguments  = [self.id]
      cmdOptions = {}

      if options[:from]
        cmdOptions["--start"] = options[:from].to_s
      end

      if options[:events]
        cmdOptions["--events"] = true
      end

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("jobs", "view", arguments, cmdOptions)
      result[:data]
    end

    # Reads the events for the job
    def eventLog(options={})
      self.log(options.update(:events => true))
    end

    # Cancel the job
    def cancel
      arguments  = [self.id]
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("jobs", "cancel", arguments, cmdOptions)
      result[:data]
    end

    # Returns the backend the job is using.
    def backend
      @backend ||= self.info[:backend]
    end

    def queueTime
      @queueTime ||= self.info[:queueTime]
    end

    def startTime
      @startTime ||= self.info[:startTime]
    end

    def finishTime
      @finishTime ||= self.info[:finishTime]
    end

    def scheduler
      @scheduler ||= self.info[:scheduler]
    end

    # Returns true when the job is currently running.
    def running?
      self.info[:running]
    end

    # Retrieves the status of the job ("queued", "started", "finished")
    def status
      self.info[:status]
    end

    # Retrieve the Occam::Object that represents the task begin executed
    def task
      if self.info[:task]
        @task ||= Occam::Object.new(:id       => self.info[:task][:id],
                                    :revision => self.info[:task][:revision],
                                    :account  => @account)
      else
        nil
      end
    end

    def info
      if @last.nil? || (Time.now - @last) > INFO_CACHE_TIME
        # Cache for a certain amount of time
        arguments  = [self.id]
        cmdOptions = {
          "-j" => true
        }

        if @account
          cmdOptions["-T"] = @account.token
        end

        result = Occam::Worker.perform("jobs", "status", arguments, cmdOptions)
        begin
          @info = JSON.parse(result[:data], :symbolize_names => true)
        rescue
          @info
        end

        @last = Time.now
      end

      @info || {}
    end
  end
end
