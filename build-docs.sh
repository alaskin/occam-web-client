# Delete the caches
rm -rf .yardoc .yardoc-api

# Build API Documentation
# Quiet, because we abuse rdoc so much it has tons of warnings
bundle exec yardoc --no-cache -q -e lib/apiParser.rb --plugin yard-sinatra -c .yardoc-api -o public/apidocs --debug --query '@api.text == "api"' controllers/*.rb controllers/**/*.rb

# Build normal Documentation
bundle exec yardoc --no-cache -o public/docs/ lib/**/*.rb lib/*.rb models/*.rb models/**/*.rb helpers/**/*.rb controllers/*.rb controllers/**/*.rb
