const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ImageminWebpWebpackPlugin= require("imagemin-webp-webpack-plugin");
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

/* This describes the behavior of webpack.
 */
module.exports = {
  /* The main javascript file for the application
   */
  entry: {
    occam: ["./public/js/occam.js", "./stylesheets/app.scss"],
    vendor: ["./public/js/vendor.js", "./public/js/vendor.scss"],
    xterm: ["./public/js/vendor-xterm.js", "./public/js/vendor-xterm.scss"],
    "polyfill-ie": ["./public/js/polyfill-ie.js", "./stylesheets/ie.scss"],
  },

  /* Just... ignore this for now.
   */
  performance: {
    hints: false
  },

  /* The default mode.
   */
  mode: "production",

  /* The eventual transpiled output file.
   */
  output: {
    path: __dirname + "/public/js",
    filename: "compiled-[name].js",
    sourceMapFilename: "compiled-[name].js.map"
  },

  /* We want source maps!
   */
  devtool: "source-map",

  /* What file types to filter.
   */
  resolve: {
    extensions: ['.js', '.css', '.scss', '.jpeg', '.jpg', '.png'],
  },

  /* How to load/import modules (for each file).
   */
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          "babel-loader",
          "eslint-loader",
        ]
      },
      {
        test: /\.s?css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '/public/css',
            },
          },
          {
            loader: 'css-loader'
          },
          {
            loader: "sass-loader",
            options: {
              sassOptions: (loaderContext) => {
                // More information about available properties https://webpack.js.org/api/loaders/
                const { resourcePath, rootContext } = loaderContext;
                const relativePath = path.relative(rootContext, resourcePath);

                if (relativePath == "stylesheets/app.scss") {
                  return {
                    includePaths: path.resolve(__dirname, 'stylesheets'),
                  };
                }
                else {
                  return {
                    includePaths: path.resolve(__dirname, 'node_modules'),
                  };
                }
              }
            }
          },
          //{
          //  loader: MiniCssExtractPlugin.loader,
          //}
        ]
      },
      {
        test: /\.(jpe?g|png|gif)/,
        loader: 'file-loader',

        options: {
          name: '[name].[ext]?[hash]',
          outputPath: 'assets/'
        }
      }
    ]
  },

  /* Minimize all vendored css */
  optimization: {
    minimizer: [
      new OptimizeCssAssetsPlugin({}),
      new UglifyJsPlugin()
    ]
  },

  /* What plugins to use.
   */
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "../css/[name].css",
      chunkFilename: "[id].css"
    }),
    //new webpack.HotModuleReplacementPlugin(),
    new ImageminWebpWebpackPlugin({
      config: [{
        test: /\.(jpe?g|png)/,
        options: {
          quality:  75
        }
      }],
      overrideExtension: true,
      detailedLogs: true,
      silent: false,
      strict: true
    }),
    new MomentLocalesPlugin(),
  ]
}
