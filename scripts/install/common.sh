#!/bin/bash

# Nothing here requires root.
_OCCAM_WEB_PATH=$(realpath $(dirname $0)/../..)

# We want to place ourselves in our vendor environment, if it exists
if [ -e "${_OCCAM_WEB_PATH}/vendor/bin" ]; then
	export PATH=${_OCCAM_WEB_PATH}/vendor/bin:$PATH
	export LD_LIBRARY_PATH=${_OCCAM_WEB_PATH}/vendor/lib:$LD_LIBRARY_PATH
fi

# Install ruby environment
echo "Installing Bundler"
gem install bundler --user-install -v "$(grep -A 1 "BUNDLED WITH" Gemfile.lock | tail -n 1)"

# Install ruby dependencies
echo "Installing Ruby Dependencies"
$(ruby -r rubygems -e 'puts Gem.user_dir')/bin/bundle install --binstubs=vendor/bin --path vendor/bundle --with test

# Install JavaScript dependencies
echo "Installing JavaScript dependencies"
npm install

# Build JavaScript
echo "Building JavaScript"
npm run build

# Build documentation
echo "Building API and Ruby Documentation"
npm run build-docs

# Build documentation
echo "Building Style Documentation"
npm run build-css-docs

# Compile assets
echo "Compiling Icon Assets"
npm run compile-assets

# Compile assets
echo "Minimizing images"
npm run minimize-assets

# Activate environment
./scripts/activate.sh
