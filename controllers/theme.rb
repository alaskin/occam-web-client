# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'fileutils'

# @api api
# @!group Theme API
class Occam
  module Controllers
    class ThemeController < Occam::Controller
      # Grab the themed version of the stylesheet
      get "/css/:theme/theme.css" do
        # Get the theme information
        begin
          theme = Occam::System::Theme.new(params[:theme])
        rescue
          status 404
          return nil
        end

        # Generate the theme file (if it exists, regenerate if theme is newer)
        # TODO: look at mtime
        themePath = File.join("stylesheets", "themes", params[:theme])
        FileUtils.mkdir_p themePath

        themeCSSPath = File.join("public", "css", "themes", params[:theme])
        FileUtils.mkdir_p themeCSSPath
        themeCSSPath = File.join(themeCSSPath, "theme.css")

        if Occam::Controller.development? or not File.exist?(themeCSSPath)
          themeSCSSPath = File.join(themePath, "theme.scss")

          File.open(themeSCSSPath, "w+") do |f|
            f.write(theme.scss)
          end

          File.open(themeCSSPath, "w+") do |f|
            f.write(render(:scss, :"../stylesheets/themes/#{params[:theme]}/theme"))
          end
        end

        content_type "text/css"
        send_file themeCSSPath
      end
    end
  end

  use Controllers::ThemeController
end
# @!endgroup
