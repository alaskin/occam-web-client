# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../people"

# @api api
# @!group People API
class Occam
  module Controllers
    # These routes handle the managing of memberships and groups for the logged in Person.
    class PersonMembershipsController < Occam::Controllers::PersonController
      # Renders the bookmark listing
      get '/people/:identity/memberships/?' do
        renderPersonView("memberships")
      end

      # Updates people memberships
      post '/people/:identity/members' do
        person = Occam::Person.new(:identity => params[:identity],
                                   :account  => current_account)

        if !person.exists?
          # TODO: confirm this is not a 406
          status 404
          return
        end

        member  = params["person"]
        member  = params["object-id"] || person

        if member
          # Pull out the object
          member = Occam::Person.new(:identity => member)
        end

        # Update permission
        person.addMember(member)

        if request.xhr?
          render :slim, :"people/memberships/_row",
                        :layout => false,
                        :locals => {
            :person => person,
            :member => member
          }
        else
          if request.referrer
            redirect request.referrer
          else
            redirect person.url(:path => "memberships")
          end
        end
      end

      # Deletes the membership of the given member in the given person
      delete '/people/:identity/members/:member_identity' do
        person = Occam::Person.new(:identity => params[:identity],
                                   :account  => current_account)

        if !person.exists?
          status 404
          return
        end

        member = Occam::Person.new(:identity => params[:member_identity],
                                   :account  => current_account)

        if !member.exists?
          # TODO: confirm this is not a 406
          status 404
          return
        end

        # Delete permission row
        person.removeMember(member)

        if request.xhr?
        else
          if request.referrer
            redirect request.referrer
          else
            redirect person.url(:path => "memberships")
          end
        end
      end
    end
  end

  use Controllers::PersonMembershipsController
end
