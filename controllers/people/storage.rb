# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../people"

# @api api
# @!group People API
class Occam
  module Controllers
    # These routes handle the managing of storage backends for the logged in Person.
    class PersonStorageController < Occam::Controllers::PersonController
      # Renders the storage listing
      get '/people/:identity/storage/?' do
        renderPersonView("storage")
      end

      post '/people/:identity/storage/:backend' do
        # Do not allow a non-logged in account to mess with this
        if current_person.nil? || params[:identity] != current_person.identity.uri
          status 404
        end

        record = Occam::System::Storage.info(:backend => params[:backend], :account => current_account)
        if record
          data = {}
          record.accountInfo.each do |k, v|
            if params.has_key? k.to_s
              data[k] = params[k.to_s]
            end
          end
          record = record.set(data)
        end

        # Determine if this should push an oauth token?
        if record.accountInfo[:oauthURL] && record.accountInfo[:oauthAccessToken] == ""
          redirect record.accountInfo[:oauthURL]
        else
          redirect request.referrer
        end

        # Render the updated record
        record
      end

      # Update a storage backend instantiation
      post '/people/:identity/storage/:backend/:info_id' do
        # Do not allow a non-logged in account to mess with this
        if current_person.nil? || params[:identity] != current_person.identity.uri
          status 404
        end

        records = Occam::System::Storage.view(params[:backend], current_account, params[:info_id])

        status 404 if records.empty?

        record = records.first
        data = {}
        record.accountInfo.each do |k, v|
          if params.has_key? k.to_s
            data[k] = params[k.to_s]
          end
        end
        record = record.set(data)

        redirect request.referrer
      end

      # Remove a storage backend instantiation
      delete '/people/:identity/storage/:backend/:info_id' do
        # Do not allow a non-logged in account to mess with this
        if current_person.nil? || params[:identity] != current_person.identity.uri
          status 404
        end

        records = Occam::System::Storage.view(params[:backend], current_account, params[:info_id])

        status 404 if records.empty?

        record = records.first
        record.destroy!

        redirect request.referrer
      end
    end
  end

  use Controllers::PersonStorageController
end
