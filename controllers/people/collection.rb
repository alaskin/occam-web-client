# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../people"

# @api api
# @!group People API
class Occam
  module Controllers
    # These routes handle the managing of the active objects for the logged in Person.
    class PersonCollectionController < Occam::Controllers::PersonController
      get '/people/:identity/collection/?' do
        renderPersonView("collection")
      end

      # Adds a new object to the collection.
      # It may create a new object if there is no "id" passed.
      post '/people/:identity/collection' do
        if current_person.nil? || params[:identity] != current_person.identity.uri
          status 404
          return
        end

        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        id = params["id"]

        type = params["type"]
        name = params["name"]

        if id.nil?
          # Perhaps we are forking a template
          if params["object-id"] && params["object-id"] != ""
            # Fork the template (removing the subtype)
            template = Occam::Template.new(:id => params["object-id"],
                                           :revision => params["object-revision"],
                                           :account => current_account)

            # Find the subtype... and remove it
            subtypes = template.info(true)[:subtype]
            if !subtypes.is_a?(Array)
              subtypes = [subtypes]
            end
            subtypes.delete("object-template")

            object = template.clone(nil, :name => name, :subtype => subtypes, :full => true)
            if object
              # Delete the clonedFrom
              items = [["clonedFrom"]]
              object = object.set(items)
            end
          else
            # Create a new object
            object = Occam::Object.create(:name    => name,
                                          :type    => type,
                                          :account => current_account)
          end

          if object.nil?
            status 422
            return
          end

          # Link to the object
          link_id = current_person.createLink(:relationship => "active",
                                              :object       => object,
                                              :account      => current_account,
                                              :tracked      => true)
          case format
          when 'application/json'
            content_type "application/json"
            {
              :url => object.url(:query => {:link => link_id}),
              :object => object.info.update({:id => object.id, :uid => object.uid, :revision => object.revision})
            }.to_json
          else
            redirect object.url(:query => {:link => link_id})
          end
        end
      rescue Occam::Daemon::Error => e
        status 422

        case format
        when 'application/json'
          {
            'errors': [e.to_s]
          }.to_json
        else
          render :slim, :"objects/new", :layout => !request.xhr?, :locals => {:errors => [e]}
        end
      end
    end
  end

  use Controllers::PersonCollectionController
end
