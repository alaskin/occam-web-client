# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../people"

# @api api
# @!group People API
class Occam
  module Controllers
    # These routes handle rendering and updating the profile page of a Person.
    #
    # This includes metadata and avatar images that represent this Person.
    class PersonProfileController < Occam::Controllers::PersonController
      # Renders the bookmark listing
      get '/people/:identity(/profile)?/?' do
        renderPersonView("profile")
      end

      # Form to edit an person's profile
      get '/people/:identity(/profile)?/edit' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)

        if not person.exists?
          status 404
        else
          # TODO: allow person editing for administrators
          if current_person.id != person.id
            status 406
          else
            render :slim, :"people/edit", :layout => !request.xhr?, :locals => {
              :errors  => nil,
              :person => current_person
            }
          end
        end
      end

      # Update person information
      post '/people/:identity(/profile)?/?' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)
        if not person.exists?
          status 404
          return
        end

        if not logged_in?
          status 404
          return
        end

        # Do not authorize this edit if they aren't logged in
        # TODO: allow person editing for administrators
        if person.id != current_person.id
          status 406
          return
        end

        originalId = person.id

        # Pull data from the form
        data = params[:data] || {}

        # Data can be a JSON document or a Hash of keys
        if !data.is_a?(Hash)
          begin
            data = JSON.parse(data)
          rescue
            data = {}
          end
        end

        # Sanitize data into the items block
        items = []
        data.each do |key, value|
          # Fix array references
          canonicalKey = key.gsub('(', '[').gsub(')', ']')

          # Delete the key when the value is blank
          # (instead of writing empty string)
          if params[:nullify] && params[:nullify][key] && value == ""
            items << [canonicalKey]
          elsif params[:datatype] && params[:datatype][key] == "list"
            # This creates an array with values that are set ('on')
            # This is used to interpret checkbox values that form an array
            #   in the metdata.
            if value.is_a?(Hash)
              value = value.filter{|_,v| v == "on"}.map{|k,v| k}
            end
            items << [canonicalKey, value.to_json]
          elsif params[:datatype] && params[:datatype][key] == "tags"
            # This converts data that is supplied by a tags input field,
            #   which is annoyingly a weird data format for no good reason.
            items << [canonicalKey, JSON.parse(value).map{|i| i["value"]}.to_json]
          else
            # The normal key = value setting...
            items << [canonicalKey, value.to_json]
          end
        end

        person = person.set(items, "object.json", :type => "json")
        if person.nil?
          status 422
          return
        end

        # Update avatar image if it is given
        if params["avatar"]
          if params["avatar"][:tempfile]
            person = person.set(params["avatar"][:tempfile].read, "avatar.jpg")
            person = person.set([ ["images", "[\"avatar.jpg\"]"] ], "object.json", :type => "json")
          end
        end

        # We also should update the Account if the Person id has now changed
        if person.id != originalId
          person.setForAccount(params[:identity])
        end

        current_account.revalidatePerson!

        redirect person.url
      end

      # Retrieves the avatar image for the particular person
      get '/people/:identity/avatar' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)

        if not person.exists?
          status 404
        else
          url = person.avatar_url((params["size"] || 64).to_i)

          if url.start_with?("data")
            content_type "image/png"
            person.avatar
          else
            redirect url
          end
        end
      end

      # Retrieves the avatar image for the particular person
      get '/people/:identity/:revision/avatar' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :revision => params[:revision],
                                            :account  => current_account)

        if not person.exists?
          status 404
        else
          url = person.avatar_url((params["size"] || 64).to_i)

          if url.start_with?("data")
            content_type "image/png"
            person.avatar
          else
            redirect url
          end
        end
      end
    end
  end

  use Controllers::PersonProfileController
end
