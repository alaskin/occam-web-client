# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group System API
class Occam
  module Controllers
    # Handles routes based on viewing system information, documentation, and configuration.
    class SystemController < Occam::Controller
      # Update the system configuration
      #post '/system' do
      #end

      #delete '/system/associations' do
      #end

      #post '/system/associations' do
      #end

      def daemonPath(path = nil)
        if not defined?(@@daemonPath)
          @@daemonPath = File.realpath(File.join(File.realpath(`which occam`.strip), "..", ".."))
        end

        ret = @@daemonPath

        if path
          ret = File.realpath(File.join(@@daemonPath, path))

          if not ret.start_with?(@@daemonPath)
            ret = nil
          end
        end

        ret
      end

      # Redirects to the current codebase being used by this Occam node.
      get '/system/code' do
        occam = Occam::Object.new(:id => "QmbDRC7BFzyeANfuedASwuc35hMCLRq3p5yRnFKrLJ87pd")
        redirect occam.url(:path => "files")
      end

      # Redirects to the current web client codebase being used by this Occam node.
      get '/system/client-code' do
        occam = Occam::Object.new(:id => "QmdCT3n8Z6ju2vLaWL5ioMGp7T37Hb64VEuiL8ztFFHKuA")
        redirect occam.url(:path => "files")
      end

      # Get the system configuration
      get '/system/?:tab?/?:subtab?/?' do
        system = Occam::System.new(:account => current_account)

        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        case format
        when 'text/html'
          # TODO: render nodes
          layout = Occam::Layout.new(:view   => :desktop,
                                     :tab    => params[:tab],
                                     :subtab => params[:subtab])

          page = :"system/index"
          if request.xhr?
            if not ["tests", "client-tests", "javascript-tests", "theme", "documentation"].include?(params[:tab])
              status 404
              return
            end

            if params[:subtab] && !["api", "web", "javascript", "daemon"].include?(params[:subtab])
              status 404
              return
            end

            if params[:subtab]
              page = :"system/#{params[:tab]}/_#{params[:subtab]}"
            else
              page = :"system/_#{params[:tab]}"
            end
          end

          render :slim, page, :layout => !request.xhr?, :locals => {
            :system   => system,
            :nodes    => [],
            :path     => "/system/tests",
            :layout   => layout
          }
        when 'application/json'
          content_type 'application/json'

          system.info.to_json
        end
      end
    end
  end

  use Controllers::SystemController
end
# @!endgroup
