# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Task API
class Occam
  module Controllers
    class ViewerController < Occam::Controller
      # This module represents the routes pertaining the viewer querying.

      # Retrieves a list of viewers for the given type and subtype of object.
      get '/viewers' do
        type    = params["type"]
        subtype = params["subtype"]

        if params.has_key?("discover") && params["discover"].nil?
          params["discover"] = true
        end

        content_type "application/json"

        Occam::Object.viewersFor(:type     => type,
                                 :subtype  => subtype,
                                 :discover => !!params["discover"],
                                 :json     => true)
      end
    end
  end

  use Controllers::ViewerController
end
# @!endgroup
