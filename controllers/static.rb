# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Static Routes
class Occam
  module Controllers
    class StaticController < Occam::Controller
      # Shows the default index page, that is, it redirects to /about.
      get '/' do
        slim :"index", :locals => {
          :content => :static
        }
      end

      # Redirects to the core repository's list of publications.
      get '/papers' do
        redirect "https://gitlab.com/occam-archive/occam/wikis/Publications/Papers"
      end

      # Renders the index page that shows what Occam is and what it does.
      get '/about' do
        slim :"static/about", :locals => {
          :content => :static
        }
      end

      # Shows the credits and acknowledgements page.
      #
      # We are indebted to many folks.
      get '/acknowledgements' do
        render(:slim, :"static/credits")
      end

      # Shows the publications page.
      get '/publications' do
        render(:slim, :"static/publications")
      end

      # Successfully get the 404 page (to check its rendering).
      get '/404' do
        render(:slim, :"static/404")
      end

      # Successfully get the 500 page (to check its rendering).
      get '/500' do
        render(:slim, :"static/500")
      end

      # Redirects to the index of the js coverage report.
      get '/coverage/js/?' do
        redirect "/coverage/js/index.html"
      end

      # Redirects to the index of the js docs.
      get '/jsdocs/?' do
        redirect "/jsdocs/index.html"
      end

      # Redirects to the index of the API docs.
      get '/apidocs/?' do
        redirect "/apidocs/index.html"
      end

      # Redirects to the index of the web docs.
      get '/docs/?' do
        redirect "/docs/index.html"
      end

      # Redirects to the index of the code docs.
      get '/daemon-docs/?' do
        redirect "/daemon-docs/index.html"
      end

      # Get the code docs, if they exist.
      get '/daemon-docs/?*' do
        if !defined?(@@occamDocPath) || @@occamDocPath.nil?
          # Get the location of the daemon
          path = `which occam`.strip

          if File.exist?(path)
            path = File.dirname(path)
            path = File.join(path, "..", "docs", "html")
            if File.exist?(path)
              @@occamDocPath = File.realpath(path)
            end
          end
        end

        if defined?(@@occamDocPath)
          dirs = params[:splat][0].split("/")
          path = @@occamDocPath
          dirs.each do |dir|
            if dir == ".."
              status 404
              return
            end
            path = File.join(path, dir)
          end

          path = File.realpath(path)

          if path.start_with? @@occamDocPath
            send_file path
          end
        end
      end

      # Retrieve the HTTPS Public Key.
      get '/public_key/?' do
        content_type "text/plain"
        send_file "key/server.crt"
      end
    end
  end

  use Controllers::StaticController
end
# @!endgroup
