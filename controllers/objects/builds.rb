# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../objects"

# @api api
# @!group Object API
class Occam
  module Controllers
    # This controller handles all file tab requests for an object.
    class ObjectBuildsController < Occam::Controllers::ObjectController
      # Displays an interface for building the object.
      get %r{#{OBJECT_ROUTE_REGEX}/build/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html'])

        case format
        when 'text/html'
          renderObjectView("build")
        end
      end

      # Retrieves the build queue form for setting up options for building the object.
      get %r{#{OBJECT_ROUTE_REGEX}/build/queue(?<build_path>.+)?} do
        @object = resolveObject()
        params[:build_path] = "/" if params[:build_path] == "" || params[:build_path].nil?

        # Re-resolve to parse path parameter
        @object = resolveObject()

        # Render a task generation form for this object for running
        render :slim, :"objects/_run-form", :layout => false, :locals => {
          :object => @object,
          :phase => :build,
          :running => true
        }
      end

      # Get the list of builds
      get %r{#{OBJECT_ROUTE_REGEX}/builds/?} do
        @object = resolveObject()
        content_type "application/json"

        @object.builds(:json => true)
      end

      # Render a previous build
      get %r{#{OBJECT_ROUTE_REGEX}/builds/(?<build_id>[^/]+)/?} do
        @object = resolveObject()
        build = Occam::Build.new(:id => params[:build_id], :object => @object, :account => current_account)

        if not build
          status 404
          return
        end

        if request.xhr?
          render :slim, :"objects/build/_show", :layout => !request.xhr?, :locals => {
            :object => @object,
            :build => build,
            :task => build,
            :noBuildList => true
          }
        else
          params[:tab] = "build"
          render :slim, :"objects/show", :locals => {
            :object => @object,
            :build  => build,
            :task   => build
          }
        end
      end

      # Render a build log
      get %r{#{OBJECT_ROUTE_REGEX}/builds/(?<build_id>[^/]+)/log/?} do
        @object = resolveObject()
        build = Occam::Build.new(:id => params[:build_id], :object => @object, :account => current_account)

        if not build
          status 404
          return
        end

        content_type "application/octet-stream"
        build.log
      end

      # Queue a build
      post %r{#{OBJECT_ROUTE_REGEX}/builds/?} do
        @object = resolveObject()
        # Pull out the object (as a workflow, if needed)
        job = @object.build(:fromClean => params.has_key?(:fromClean))

        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        case format
        when 'application/json'
          content_type "application/json"
          job.info.to_json
        else
          render :slim, :"objects/_run-list-item", :layout => false, :locals => {
            :run => job,
            :object => @object
          }
        end
      end

      # Retrieves actual file data for the given file within the object.
      #
      # This will be blocked by any agents listed in the 'raw-block' section of
      # the Occam Web configuration based on the User-Agent field of the incoming
      # request.
      #
      # @!macro object_route
      # @param [String] path Path to a file within the object.
      #
      # === Accepted Content Types:
      #
      # \*.\*
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/builds/QmYJZ3yDq6KPViV4vmZ9Gyx5r8c4HJoQQu6g2choi5qRpk/raw/system.ini.example
      #
      # Returns the text data for the file system.ini.example within this specific
      # build of the object.
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/builds/QmYJZ3yDq6KPViV4vmZ9Gyx5r8c4HJoQQu6g2choi5qRpk/raw/ini/
      #
      # Returns a JSON document describing the directory contents. All directories
      # have a default content type of JSON and act similarly to querying the path
      # using the "files" route with an +application/json+ Accept header.
      get %r{#{OBJECT_ROUTE_REGEX}/builds/((?<build_id>[^/]+)/)?raw/(?<build_path>.+)?} do
        @object = resolveObject()
        # Allow all external access
        handleCORS('*')

        # Block agents that are not allowed to pull raw content
        # (Some crawlers are especially bad actors)
        if agentBlockRaw()
          status 404
          return
        end

        if not params[:build_id]
          params[:build_id] = @object.builds.first.id
        end

        build = Occam::Build.new(:id => params[:build_id], :path => params[:build_path], :object => @object, :account => current_account)

        if not build.exists?
          status 404
          return
        end

        # Pull out file data
        meta = build.retrieveFileStat(build.path)

        # Return 404 if the file cannot be found.
        if meta.nil?
          status 404
          return
        end

        # Downloads the file with the given name
        if params[:as]
          attachment params[:as]
        end

        # Cache, if not staged
        if @object.link.nil?
          # Cache for a long time (this is static content, if a revision is given)
          cache_control :max_age => 31536000
        end

        # If this is a directory, it just reports the directory listing as JSON
        if meta[:type] == "tree"
          content_type "application/json"
          return build.retrieveDirectory(build.path).to_json
        end

        # Determine the content type
        content_type((meta[:mime] || [])[0] || "application/octet-stream")

        # Write out the expected length of the data
        if meta && meta.has_key?(:size)
          headers['Content-Length'] = meta[:size].to_s
        end

        # Ensure a reasonable content policy
        #
        # This means if the content is loaded by the browser, no matter the
        # actual origin... the scripts inside cannot interact with the current
        # user session or cookies (They get a NULL origin)
        #
        # Browser Support: https://caniuse.com/#feat=contentsecuritypolicy
        if not @object.trusted?
          sandbox = "sandbox allow-pointer-lock allow-scripts;"

          headers["X-WebKit-CSP"]              = sandbox # Fallback (Android)
          headers["X-Content-Security-Policy"] = sandbox # Fallback (IE)
          headers["Content-Security-Policy"]   = sandbox
        end

        # Support HTTP Range headers
        start = nil
        length = nil
        range = Rack::Utils.get_byte_ranges(request.env["HTTP_RANGE"], meta[:size])
        if range && range.any?
          start = range[0].first
          length = range[0].last - range[0].first
        end

        # Pull out a data stream
        data = build.retrieveFile(build.path, :start => start, :length => length, :stream => true)

        # Echo the data stream on the socket
        return chunked_stream :keep_open do |out|
          while true
            bytes = data.read(1024*10)
            if bytes.nil? || bytes.length == 0
              break
            else
              out << bytes
            end
          end
        end
      end

      # Retrieves the file stat or render a representation for the given file within a particular build of the object.
      #
      # @!macro object_route
      # @param [String] path Path to a file within the object.
      #
      # === Accepted Content Types:
      #
      # text/html (_default_), application/json, application/zip, application/x-tar+xz
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/builds/QmYJZ3yDq6KPViV4vmZ9Gyx5r8c4HJoQQu6g2choi5qRpk/files/system.ini.example
      #
      # Renders the file system.ini.example from this object build. Likely, this will render either a viewer
      # or editor that make sense for working with this file. It may, perhaps, depend on the associations
      # established by the person currently using the site.
      #
      #   (Accept: application/zip)
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/builds/QmYJZ3yDq6KPViV4vmZ9Gyx5r8c4HJoQQu6g2choi5qRpk/files/ini/DDR2_micron_16M_8b_x8_sg3E.ini
      #
      # Retrieves a zip file containing ini/DDR2_micron_16M_8b_x8_sg3E.ini (including the directory "ini").
      #
      #   (Accept: application/zip)
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/builds/QmYJZ3yDq6KPViV4vmZ9Gyx5r8c4HJoQQu6g2choi5qRpk/files/init
      #
      # Retrieves a zip file containing the directory "ini" including all files and
      # subdirectories within "ini".
      #
      #   (Accept: application/json)
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/builds/QmYJZ3yDq6KPViV4vmZ9Gyx5r8c4HJoQQu6g2choi5qRpk/files/ini/DDR2_micron_16M_8b_x8_sg3E.ini
      #
      # Retrieves the file stat for this same file as JSON. When invoked with curl, it will return this:
      #
      #   {"name": "DDR2_micron_16M_8b_x8_sg3E.ini", "size": 530, "type": "blob", "mode": 33184, "mime": ["text/ini", "application/octet-stream"]}
      get %r{#{OBJECT_ROUTE_REGEX}/builds/((?<build_id>[^/]+)/)?files(?<build_path>.+)?} do
        @object = resolveObject()
        handleCORS('*')

        if not params[:build_id]
          params[:build_id] = @object.builds.first.id
        end

        build = Occam::Build.new(:id => params[:build_id], :path => params[:build_path], :object => @object, :account => current_account)

        if not build.exists?
          status 404
          return
        end

        root = false
        if params[:build_path] == "" || params[:build_path].nil?
          root = true
        end

        if params[:build_path] == "" || params[:build_path].nil?
          params[:build_path] = "/"
        end

        format = request.preferred_type(['text/html', 'application/json', 'application/zip', 'application/x-tar+xz'])

        # Determine the output (the stat, or the file viewer rendering)
        case format
        when 'application/json'
          # Stat the file
          content_type "application/json"

          stat = build.retrieveFileStat(build.path)

          # Return 404 if the file is not found
          if stat.nil?
            status 404
            return
          end

          # If it is a directory, retrieve the file listing
          if stat[:type] == "tree"
            stat = build.retrieveDirectory(build.path)
          end

          # Convert to a JSON string
          stat.to_json
        when 'application/x-tar+xz', 'application/zip'
          # Returns the compressed version of the requested path/file

          # Pull out file data
          meta = build.retrieveFileStat(build.path)

          # Return 404 if the file is not found
          if meta.nil?
            status 404
            return
          end

          # Return an appropriate content type
          content_type format

          # Downloads the file with the given name
          if params[:as]
            attachment params[:as]
          end

          # Cache, if not staged
          if @object.link.nil?
            # We cache the zipped content
            cache_control :max_age => 31536000
          end

          case format
          when 'application/x-tar+xz'
            compress = "txz"
          else
            compress = "zip"
          end

          if build.status[:compress] && build.status[:compress][compress.intern] && build.status[:compress][compress.intern][:size]
            # Write out the expected length of the data
            headers['Content-Length'] = build.status[:compress][compress.intern][:size].to_s
          end

          # Retrieve a data stream for the 'compressed' content
          data = build.retrieveFile(build.path, :compress => compress, :stream => true)

          # Echo that stream to the socket
          return stream :keep_open do |out|
            while true
              bytes = data.read(1024*10)
              if bytes.nil? || bytes.length == 0
                break
              else
                out << bytes
              end
            end
          end
        else
          # Render the file viewer
          if request.xhr? && !params[:full] && !root
            # We are rendering a subset of the view (AJAX route)
            if build.isGroup?(build.path)
              render :slim, :"objects/_directory", :layout => false, :locals => {
                :object => @object,
                :build => build
              }
            else
              render :slim, :"objects/_files", :layout => false, :locals => {
                :object => @object,
                :build => build,
                :data_mime_type => params["type"] || nil
              }
            end
          else
            renderObjectView("files", :build => build)
          end
        end
      end

      options %r{#{OBJECT_ROUTE_REGEX}/builds/(?<build_id>[^/]+)/files/(?<path>.+)?} do
        handleCORS('*')

        status 200
      end

      options %r{#{OBJECT_ROUTE_REGEX}/builds/(?<build_id>[^/]+)/raw/(?<path>.+)?} do
        handleCORS('*')

        status 200
      end
    end
  end

  use Controllers::ObjectBuildsController
end
