# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../objects"

# @api api
# @!group Object API
class Occam
  module Controllers
    # This controller handles all metadata requests for an object.
    class ObjectMetadataController < Occam::Controllers::ObjectController
      # Retrieves the object metadata or the details page.
      #
      # Returns a 404 if the object is not found or the content cannot be
      # rendered with the requested type.
      #
      # @!macro object_route
      # @param [String] extension The extension to force the content to render as.
      #                           This can also be set using the Accept HTTP header.
      #
      # === Accepted Content Types:
      #
      # text/html (_default_), application/json
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b
      #
      # Retrieves an HTML document that renders the object (or a 404 if it does not exist.)
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata
      #
      # Retrieves the same HTML document that renders the object (or a 404 if it does not exist.)
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b.json
      #
      # Retrieves a JSON document that describes the object metadata (or a 404 if it does not exist.)
      get %r{#{OBJECT_ROUTE_REGEX}(?<route>/metadata)?(?<extension>\.json)?/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html', 'application/json'])

        if params[:extension] == ".json"
          format = "application/json"
        end

        case format
        when 'application/json'
          content_type "application/json"

          if params[:revision]
            @object.info.to_json
          else
            @object.status.to_json
          end
        else
          # Allow a bare route to go to the default tab, which may be the
          # metadata route, or it might be something else.
          if params[:route]
            renderObjectView("metadata")
          else
            renderObjectView()
          end
        end
      end

      # Retrieve form to edit the object's metadata
      get %r{#{OBJECT_ROUTE_REGEX}/metadata/edit/?} do
        @object = resolveObject()
        render :slim, :"objects/metadata-editor", :layout => !request.xhr?, :locals => {
          :object => @object,
          :errors => []
        }
      end

      # Retrieve form to edit the object's description
      get %r{#{OBJECT_ROUTE_REGEX}/metadata/description/edit/?} do
        @object = resolveObject()
        render :slim, :"objects/description-editor", :layout => !request.xhr?, :locals => {
          :object => @object,
          :errors => []
        }
      end

      # Retrieve form to edit the object's build/run metadata
      get %r{#{OBJECT_ROUTE_REGEX}/metadata/(?<section>run|build)/edit/?} do
        @object = resolveObject()
        render :slim, :"objects/metadata-section-editor", :layout => !request.xhr?, :locals => {
          :object => @object,
          :section => params[:section].intern,
          :errors => []
        }
      end

      get %r{#{OBJECT_ROUTE_REGEX}(?<route>/contents)?(?<extension>\.json)?/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html', 'application/json'])

        if params[:extension] == ".json"
          format = "application/json"
        end

        case format
        when 'application/json'
          content_type "application/json"

          (@object.info[:contains] || []).to_json
        else
          # Allow a bare route to go to the default tab, which may be the
          # metadata route, or it might be something else.
          if params[:route]
            renderObjectView("contents")
          else
            renderObjectView()
          end
        end
      end

      # Retrieves the first license text for the specified object.
      #
      # Returns a 404 if no license text exists for the given object.
      #
      # Generally, this will redirect to /licenses/0, which is the more general API
      # route for retrieving the first license.
      #
      # @!macro object_route
      # @param [String] extension The extension to force the content to render as.
      #                           This can also be set using the Accept HTTP header.
      #
      # === Accepted Content Types:
      #
      # text/html (_default_), application/json, text/plain
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/license
      #
      # Retrieves an HTML document that renders the license (or a 404 if none exist.)
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/license.json
      #
      # Retrieves a JSON document that describes the metadata and text of the license (or a 404 if none exist.)
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/license.txt
      #
      # Retrieves the same license document, but rendered as plain text (or a 404 if none exist.)
      get %r{#{OBJECT_ROUTE_REGEX}/license(?<extension>\.txt|\.json)?/?} do
        @object = resolveObject()
        redirect @object.url(:path => "/licenses/0#{params[:extension]}")
      end

      # Retrieves the specified license text for the specified object.
      #
      # Returns a 404 if there does not exist a license at the specified index in
      # the list of licenses for the given object.
      #
      # Occam objects may have more than one license. They may also have no licenses
      # described. In the situations where they have more than one, this route will
      # retrieve information or text for a specific license. The order here will match
      # the order they are described in the Object's metadata.
      #
      # @!macro object_route
      # @param [Integer] license_index The index within the list of licenses to retrieve.
      # @param [String] extension The extension to force the content to render as.
      #                           This can also be set using the Accept HTTP header.
      #
      # === Accepted Content Types:
      #
      # text/html (_default_), application/json, text/plain
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/licenses/0
      #
      # Retrieves an HTML document that renders the license at index 0 (or a 404 if none exist.)
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/licenses/1.json
      #
      # Retrieves a JSON document that describes the metadata and text of the license at index 1
      # (the second listed license) (or a 404 if none exist.)
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/licenses/0.txt
      #
      # Retrieves the same license document as the first route, but rendered as plain text
      # (or a 404 if none exist.)
      get %r{#{OBJECT_ROUTE_REGEX}/licenses/(?<license_index>\d+)(?<extension>\.txt|\.json)?/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html', 'text/plain', 'application/json'])

        if params[:extension] == ".txt"
          format = "text/plain"
        elsif params[:extension] == ".json"
          format = "application/json"
        end

        case format
        when 'application/json'
          content_type "application/json"

          @object.licenses[params[:license_index].to_i].info.to_json
        when 'text/plain'
          content_type "text/plain"

          @object.licenses[params[:license_index].to_i].text
        else # HTML
          render :slim, :"objects/license", :layout => !request.xhr?, :locals => {
            :object  => @object,
            :license => @object.licenses[params[:license_index].to_i]
          }
        end
      end

      # Retrieves the EULA license, if it exists for this object.
      #
      # @param [String] extension The extension to force the content to render as.
      #                           This can also be set using the Accept HTTP header.
      #
      # === Accepted Content Types:
      #
      # text/html (_default_), text/plain, application/json
      #
      # === Examples:
      #
      #   GET /QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6/5dswamd8qY1tfHt4javZmYLDvmBvp9/eula
      #
      # Views the EULA for this object, if it exists. It will respond with a 404 otherwise.
      #
      #   GET /QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6/5dswamd8qY1tfHt4javZmYLDvmBvp9/eula.txt
      #
      # Views the EULA as a plain text document. It will respond with a 404 otherwise.
      #
      #   GET /QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6/5dswamd8qY1tfHt4javZmYLDvmBvp9/eula.json
      #
      # Retrieves information about the EULA, if it exists. It will respond with a 404 otherwise.
      get %r{#{OBJECT_ROUTE_REGEX}/eula(?<extension>\.json|\.txt)?/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html', 'text/plain', 'application/json'])

        if params[:extension] == ".json"
          format = "application/json"
        elsif params[:extension] == ".txt"
          format = "text/plain"
        end

        if @object.eula.nil?
          status 404
          return
        end

        case format
        when 'application/json'
          content_type "application/json"

          @object.eula.info.to_json
        when 'text/plain'
          content_type "text/plain"

          @object.eula.text
        else
          # HTML
          render :slim, :"objects/eula", :layout => !request.xhr?, :locals => {
            :object  => @object,
            :license => @object.eula
          }
        end
      end

      # Retrieves formatted citations for the specified object.
      #
      # @!macro object_route
      # @param [String] format The citation format to use. The known possible values
      #                        are "bibtex", "mla", "harvard"
      # @param [String] extension The extension to force the content to render as.
      #                           This can also be set using the Accept HTTP header.
      #
      # === Accepted Content Types:
      #
      # text/html (_default_), text/plain
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/citations/bibtex
      #
      # Retrieves an HTML document that renders a BibTeX entry for the specified object.
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/citations/bibtex.txt
      #
      # Retrieves that same BibTeX entry, but as raw text.
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/citations/mla
      #
      # Retrieves a MLA style bibliography entry. You can then, also, use a +.txt+ extension as well, if desired.
      get %r{#{OBJECT_ROUTE_REGEX}/citations/(?<format>[^.]+)(?<extension>\.txt)?/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html', 'text/plain'])
        output = "html"

        if params[:extension] == ".txt"
          format = "text/plain"
          output = "text"
        end

        case format
        when "text/plain"
          content_type "text/plain"
          @object.citation(:format => params[:format], :output => output)
        else
          "<div class='card filled citation'>" +
            @object.citation(:format => params[:format]) +
            "</div>"
        end
      end

      # Retrieves a form for adding a new input or output.
      #
      # @!macro object_route
      #
      # === Accepted Content Types:
      #
      # text/html (_default_)
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata/inputs/new
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata/outputs/new
      get %r{#{OBJECT_ROUTE_REGEX}/metadata/(?<section>inputs|outputs)/new/?} do
        @object = resolveObject()
        key = params[:section].intern

        render :slim, :"objects/metadata/new-io", :layout => !request.xhr?, :locals => {
          :object => @object,
          :section => params[:section],
          :key => key,
          :errors => []
        }
      end

      # Retrieves a form for editing an existing input or output.
      #
      # @!macro object_route
      #
      # === Accepted Content Types:
      #
      # text/html (_default_)
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata/inputs/0/edit
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata/outputs/2/edit
      get %r{#{OBJECT_ROUTE_REGEX}/metadata/(?<section>inputs|outputs)/(?<ioIndex>\d+)/edit/?} do
        @object = resolveObject()
        key = params[:section].intern

        render :slim, :"objects/metadata/new-io", :layout => !request.xhr?, :locals => {
          :object => @object,
          :section => params[:section],
          :key => key,
          :value => (@object.info[key] || [])[params[:ioIndex].to_i],
          :index => params[:ioIndex].to_i,
          :errors => []
        }
      end

      # Retrieves a form for adding a new installation.
      #
      # @!macro object_route
      #
      # === Accepted Content Types:
      #
      # text/html (_default_)
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata/inputs/new
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata/outputs/new
      get %r{#{OBJECT_ROUTE_REGEX}/metadata/init/link/new/?} do
        @object = resolveObject()

        render :slim, :"objects/metadata/new-installation", :layout => !request.xhr?, :locals => {
          :object => @object,
          :key => "init.__action__",
          :errors => []
        }
      end

      # Retrieves a form for editing a existing installation.
      #
      # @!macro object_route
      #
      # === Accepted Content Types:
      #
      # text/html (_default_)
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata/init/link/1/edit
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata/init/copy/0/edit
      get %r{#{OBJECT_ROUTE_REGEX}/metadata/init/(?<action>link|copy)/(?<linkIndex>\d+)/edit/?} do
        @object = resolveObject()

        render :slim, :"objects/metadata/new-installation", :layout => !request.xhr?, :locals => {
          :object => @object,
          :key => "init.#{params[:action]}",
          :index => params[:linkIndex].to_i,
          :action => params[:action],
          :value => ((@object.info[:init] || {})[params[:action].intern] || [])[params[:linkIndex].to_i],
          :errors => []
        }
      end

      # Retrieves a form for adding a new dependency.
      #
      # @!macro object_route
      #
      # === Accepted Content Types:
      #
      # text/html (_default_)
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata/dependencies/new
      get %r{#{OBJECT_ROUTE_REGEX}/metadata/((?<section>run|build)/)?dependencies/new} do
        @object = resolveObject()
        key = "dependencies"
        if params[:section]
          key = "#{params[:section]}.#{key}"
        end

        render :slim, :"objects/metadata/new-dependency", :layout => !request.xhr?, :locals => {
          :object => @object,
          :section => params[:section],
          :key => key,
          :errors => []
        }
      end

      # Retrieves a form for adding a new resource.
      #
      # @!macro object_route
      #
      # === Accepted Content Types:
      #
      # text/html (_default_)
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata/install/new
      get %r{#{OBJECT_ROUTE_REGEX}/metadata/((?<section>run|build)/)?install/new} do
        @object = resolveObject()
        key = "install"
        if params[:section]
          key = "#{params[:section]}.#{key}"
        end

        render :slim, :"objects/metadata/new-resource", :layout => !request.xhr?, :locals => {
          :object => @object,
          :section => params[:section],
          :key => key,
          :errors => []
        }
      end

      # Mirrors a new resource.
      #
      # Returns the resource information.
      #
      # === Accepted Content Types:
      #
      # application/json (_default_)
      # text/html
      #
      # @!macro object_route
      #
      # === Examples:
      #
      #   POST /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/metadata/install/new
      post %r{#{OBJECT_ROUTE_REGEX}/metadata/install/new} do
        @object = resolveObject()
        format = request.preferred_type(['application/json', 'text/html'])

        info = Occam::Resource.create({
          :name => params[:name],
          :type => params[:type],
          :source => params[:source],
          :account => current_account
        })

        info[:to] = params[:to]

        case format
        when 'application/json'
          content_type "application/json"

          info.to_json
        else
          render :slim, :"objects/metadata/_resource", :layout => false, :locals => {
            :objects => [info],
            :object  => @object,
            :removeURL => "",
            :referrerURL => "",
            :removeKey => "install"
          }
        end
      end

      def render_qrcode(format = nil)
        @object = resolveObject()
        if format.nil?
          format = request.preferred_type(['image/png', 'image/svg+xml', 'application/svg'])
        end

        case format
        when 'application/svg', 'image/svg+xml'
          type = :svg
        else
          type = :png
        end

        if request.xhr?
          content_type "text/html"
          render :slim, :"objects/qrcode", :layout => false, :locals => {
            :object => @object
          }
        elsif type == :svg
          content_type "image/svg+xml"
          @object.qrcode(request, :type => :svg)
        else
          content_type "image/png"
          @object.qrcode(request, :type => :png)
        end
      end

      # Retrieve the qrcode
      get %r{#{OBJECT_ROUTE_REGEX}/qrcode} do
        @object = resolveObject()
        render_qrcode
      end

      # Retrieve the qrcode (as a png)
      get %r{#{OBJECT_ROUTE_REGEX}/qrcode.png} do
        @object = resolveObject()
        render_qrcode("image/png")
      end

      # Retrieve the qrcode (as an svg)
      get %r{#{OBJECT_ROUTE_REGEX}/qrcode.svg} do
        @object = resolveObject()
        render_qrcode("application/svg")
      end
    end
  end

  use Controllers::ObjectMetadataController
end
