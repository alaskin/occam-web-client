# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../objects"

# @api api
# @!group Object API
class Occam
  module Controllers
    # This controller handles all workflow tab requests for an object.
    class ObjectWorkflowController < Occam::Controllers::ObjectController
      get %r{#{OBJECT_ROUTE_REGEX}/workflow/?} do
        @object = resolveObject(Occam::Workflow)
        format = request.preferred_type(['text/html'])

        case format
        when 'text/html'
          renderObjectView("workflow")
        end
      end
    end
  end

  use Controllers::ObjectWorkflowController
end
