# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../objects"

# @api api
# @!group Object API
class Occam
  module Controllers
    # This controller handles all view tab requests for an object.
    class ObjectViewController < Occam::Controllers::ObjectController
      # Retrieves the run queue form for setting up options for viewing the object.
      get %r{#{OBJECT_ROUTE_REGEX}/view/queue(?<path>.+)?} do
        params[:path] = "/" if params[:path] == "" || params[:path].nil?

        # Resolve to parse path parameter
        @object = resolveObject()

        format = request.preferred_type(['text/html'])

        viewer = nil
        if params[:viewer]
          viewer = Occam::Object.new(:id => params[:viewer])
        else
          viewers = @object.viewers

          preferredViewer = nil

          if viewers.any?
            preferredViewer = viewers.first
          end
        end

        case format
        when 'text/html'
          if request.xhr?
            # Render a task generation form for this object for running
            render :slim, :"objects/_run-form", :layout => false, :locals => {
              :object => @object,
              :running => false,
              :viewer => viewer
            }
          else
            renderObjectView("view", :queue  => true,
                                     :viewer => viewer,
                                     :preferredViewer => preferredViewer)
          end
        end
      end

      get %r{#{OBJECT_ROUTE_REGEX}/view(?<path>.+)?} do
        # Resolve to parse path parameter
        @object = resolveObject()

        format = request.preferred_type(['text/html'])

        viewer = nil
        if params[:viewer]
          viewer = Occam::Object.new(:id => params[:viewer])
        else
          viewers = @object.viewers

          preferredViewer = nil

          if viewers.any?
            preferredViewer = viewers.first
          end
        end

        case format
        when 'text/html'
          renderObjectView("view", :viewer => viewer,
                                   :preferredViewer => preferredViewer)
        end
      end
    end
  end

  use Controllers::ObjectViewController
end
