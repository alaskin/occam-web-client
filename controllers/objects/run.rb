# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../objects"

# @api api
# @!group Object API
class Occam
  module Controllers
    # This controller handles all run requests for an object.
    class ObjectRunController < Occam::Controllers::ObjectController
      # Displays an interface for running the object.
      get %r{#{OBJECT_ROUTE_REGEX}/run/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html'])

        case format
        when 'text/html'
          renderObjectView("run")
        end
      end

      # Retrieves the run queue form for setting up options for running the object.
      get %r{#{OBJECT_ROUTE_REGEX}/run/queue(?<path>.+)?} do
        @object = resolveObject()
        params[:path] = "/" if params[:path] == "" || params[:path].nil?

        # Re-resolve to parse path parameter
        @object = resolveObject()

        # Render a task generation form for this object for running
        render :slim, :"objects/_run-form", :layout => false, :locals => {
          :object => @object,
          :phase => :run,
          :running => true
        }
      end
    end
  end

  use Controllers::ObjectRunController
end
