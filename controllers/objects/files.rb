# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../objects"

# @api api
# @!group Object API
class Occam
  module Controllers
    # This controller handles all file tab requests for an object.
    class ObjectFilesController < Occam::Controllers::ObjectController
      # Retrieves the data representation of the object.
      #
      # For resource objects, this is the raw data representing the resource.
      # This is only true for data resource objects and not protocol-driven
      # resource objects (such as git resources).
      #
      # For normal objects, this is shorthand for the 'file' of the object. If
      # multiple files are specified, the first file is used. If no file is
      # marked within the object, the object manifest (object.json) is the
      # default.
      get %r{#{OBJECT_ROUTE_REGEX}/data/?} do
        @object = resolveObject()

        if params[:as]
          attachment params[:as]
        end

        if @object.resource?
          resource = @object.as(Occam::Resource)

          meta = resource.retrieveFileStat(nil)

          start = nil
          length = nil

          if meta
            # Support HTTP Range headers
            range = Rack::Utils.get_byte_ranges(request.env["HTTP_RANGE"], meta[:size])
            if range && range.any?
              start = range[0].first
              length = range[0].last - range[0].first
            end

            # Write out the expected length of the data
            if meta.has_key?(:size)
              headers['Content-Length'] = meta[:size].to_s
            end
          end

          # Pull out a data stream
          data = resource.retrieveFile(nil, :start => start, :length => length, :stream => true)

          # Echo the data stream on the socket
          return stream :keep_open do |out|
            while true
              bytes = data.read(1024*10)
              if bytes.nil? || bytes.length == 0
                break
              else
                out << bytes
              end
            end
          end
        else
          file = "object.json"
          if @object.info[:file]
            file = @object.info[:file]
            if file.is_a?(Array)
              file = file.first
            end
          end

          redirect @object.url(:path => "raw/#{file}")
        end
      end

      # Retrieves actual file data for the given file within the object.
      #
      # This will be blocked by any agents listed in the 'raw-block' section of
      # the Occam Web configuration based on the User-Agent field of the incoming
      # request.
      #
      # @!macro object_route
      # @param [String] path Path to a file within the object.
      #
      # === Accepted Content Types:
      #
      # \*.\*
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/raw/package/AddressMapping.cpp
      #
      # Returns the text data for the file package/AddressMapping.cpp within the object.
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/raw/package/
      #
      # Returns a JSON document describing the directory contents. All directories
      # have a default content type of JSON and act similarly to querying the path
      # using the "files" route with an +application/json+ Accept header.
      get %r{#{OBJECT_ROUTE_REGEX}/raw/(?<path>.+)?} do
        # Allow all external access
        handleCORS('*')

        # Block agents that are not allowed to pull raw content
        # (Some crawlers are especially bad actors)
        if agentBlockRaw()
          status 403
          return
        end

        # Resolving the object retrieves and parses the path
        @object = resolveObject()

        # Downloads the file with the given name
        if params[:as]
          attachment params[:as]
        end

        # Pull out file data
        meta = @object.retrieveFileStat(@object.path)

        # Return 404 if the file cannot be found.
        if meta.nil?
          status 404
          return
        end

        # Cache if this is not staged content
        if @object.link.nil?
          # Cache for a long time (this is static content, if a revision is given)
          cache_control :max_age => 31536000
        end

        # If this is a directory, it just reports the directory listing as JSON
        if meta[:type] == "tree"
          content_type "application/json"
          return @object.retrieveDirectory(@object.path).to_json
        end

        # Determine the content type
        content_type((meta[:mime] || [])[0] || "application/octet-stream")

        # Write out the expected length of the data
        if meta && meta.has_key?(:size)
          headers['Content-Length'] = meta[:size].to_s
        end

        # Ensure a reasonable content policy
        #
        # This means if the content is loaded by the browser, no matter the
        # actual origin... the scripts inside cannot interact with the current
        # user session or cookies (They get a NULL origin)
        #
        # Browser Support: https://caniuse.com/#feat=contentsecuritypolicy
        if not @object.trusted?
          sandbox = "sandbox allow-pointer-lock allow-scripts;"

          headers["X-WebKit-CSP"]              = sandbox # Fallback (Android)
          headers["X-Content-Security-Policy"] = sandbox # Fallback (IE)
          headers["Content-Security-Policy"]   = sandbox
        end

        # Support HTTP Range headers
        start = params["start"]
        length = params["length"]
        range = Rack::Utils.get_byte_ranges(request.env["HTTP_RANGE"], meta[:size])
        if range && range.any?
          start = range[0].first
          length = range[0].last - range[0].first + 1
          headers["Content-Range"] = "bytes #{start}-#{start+length-1}/#{meta[:size].to_s}"
          status 206
        end

        # Pull out a data stream
        data = @object.retrieveFile(@object.path, :start => start, :length => length, :stream => true)

        # Echo the data stream on the socket
        return stream :keep_open do |out|
          while true
            bytes = data.read(1024*10)
            if bytes.nil? || bytes.length == 0
              break
            else
              out << bytes
            end
          end
        end
      end

      # Retrieves the file stat or render a representation for the given file within the object.
      #
      # @!macro object_route
      # @param [String] path Path to a file within the object.
      #
      # === Accepted Content Types:
      #
      # text/html (_default_), application/json, application/zip
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/files/package/AddressMapping.cpp
      #
      # Renders the file package/AddressMapping.cpp from this object. Likely, this will render either a viewer
      # or editor that make sense for working with this file. It may, perhaps, depend on the associations
      # established by the person currently using the site.
      #
      #   (Accept: application/zip)
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/files/package/AddressMapping.cpp
      #
      # Retrieves a zip file containing package/AddressMapping.cpp (including the directory "package").
      #
      #   (Accept: application/zip)
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/files/package/docs
      #
      # Retrieves a zip file containing the directory "package/docs" including all files and
      # subdirectories within "docs". The zip file also contains the directory "package", but
      # does not include files within "package" except the requested directory "docs".
      #
      #   (Accept: application/json)
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/files/package/AddressMapping.cpp
      #
      # Retrieves the file stat for this same file as JSON. When invoked with curl, it will return this:
      #
      #   {"stat":"100644","type":"blob","hash":"1508fdcddf8de68a5dacb05fe58a8b6143318faf","size":11321,"name":"AddressMapping.cpp","mime":["text/x-c","application/octet-stream"],"access":{"current":{"read":true,"write":false,"clone":true,"run":true},"object":{"person":{},"universe":{}},"children":{"person":{},"universe":{}}},"tokens":{"readOnly":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyZWFkT25seSI6IlFtWExNaUpQWUNudUJjSG9ZellMZUxnaHpRYllCZ1ZDNVJONWZ5OHlLQ3VUd2QifQ.6rC-Q1UWs7yxgP7lfc1In2QSeIFRSsrYM6lrA3UuF7E","anonymous":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhbm9ueW1vdXMiOiJRbVhMTWlKUFlDbnVCY0hvWXpZTGVMZ2h6UWJZQmdWQzVSTjVmeTh5S0N1VHdkIn0.SEaZCj4VeB1vBUxR0Rv4WuwifIvyVy8d1tymCdADUQY"}}
      get %r{#{OBJECT_ROUTE_REGEX}/files(?<path>.+)?} do
        handleCORS('*')

        # Block agents that are not allowed to pull raw content
        # (Some crawlers are especially bad actors)
        if agentBlockRaw()
          status 403
          return
        end

        root = false
        if params[:path] == "" || params[:path].nil?
          root = true
        end

        if params[:path] == "" || params[:path].nil?
          params[:path] = "/"
        end

        format = request.preferred_type(['text/html', 'application/json', 'application/zip'])

        # Re-resolving the object retrieves and parses the path
        @object = resolveObject()

        # Determine the output (the stat, or the file viewer rendering)
        case format
        when 'application/json'
          # Stat the file
          content_type "application/json"

          stat = @object.retrieveFileStat(@object.path)

          # Return 404 if the file is not found
          if stat.nil?
            status 404
            return
          end

          # If it is a directory, retrieve the file listing
          if stat[:type] == "tree"
            items = @object.retrieveDirectory(@object.path)
            stat["items"] = items
          end

          # Convert to a JSON string
          stat.to_json
        when 'application/zip'
          # Returns the compressed version of the requested path/file

          # Pull out file data
          meta = @object.retrieveFileStat(@object.path)

          # Return 404 if the file is not found
          if meta.nil?
            status 404
            return
          end

          # Cache, if not staged
          if @object.link.nil?
            # We cache the zipped content
            cache_control :max_age => 31536000
          end

          # Return an appropriate content type
          content_type "application/zip"

          # Retrieve a data stream for the 'compressed' content
          data = @object.retrieveFile(@object.path, :compress => "zip", :stream => true)

          # Echo that stream to the socket
          return chunked_stream :keep_open do |out|
            while true
              bytes = data.read(1024*10)
              if bytes.nil? || bytes.length == 0
                break
              else
                out << bytes
              end
            end
          end
        else
          # Render the file viewer
          if request.xhr? && !params[:full] && !root
            # We are rendering a subset of the view (AJAX route)
            if @object.isGroup?(@object.path)
              render :slim, :"objects/_directory", :layout => false, :locals => {
                :object => @object
              }
            else
              render :slim, :"objects/_files", :layout => false, :locals => {
                :object => @object,
                :data_mime_type => params["type"] || nil
              }
            end
          else
            renderObjectView("files")
          end
        end
      end

      # Transforms the given image with the given parameters
      get %r{#{OBJECT_ROUTE_REGEX}/dynamic/hex/(?<hex>[0-9a-zA-Z]{3,6})/(?<path>.+)?} do
        @object = resolveObject()

        start = nil
        length = nil

        # Pull out a data stream
        meta = @object.retrieveFileStat(@object.path)
        data = @object.retrieveFile(@object.path, :start => start, :length => length, :stream => true)

        if params["hex"]
          params["color"] = "##{params["hex"]}"
        elsif params["hue"] and params["sat"] and params["light"]
          params["color"] = "hsl(#{params["hue"]}, #{params["sat"]}%, #{params["light"]}%)"
        end

        embed=nil
        extra=""
        if params["color"]
          require 'base64'

          headers 'Content-Type' => "image/svg+xml"
          css = "path, rect, ellipse, polygon, circle { fill: #{params["color"]} !important; stroke: transparent !important }"
          embed = Base64.encode64(css)
          extra = "<?xml-stylesheet type=\"text/css\" href=\"data:text/css;charset=utf-8;base64,#{embed}\" ?>"
        end

        # Write out the expected length of the data
        if meta.has_key?(:size)
          headers['Content-Length'] = (meta[:size] + extra.length).to_s
        end

        # Echo the data stream on the socket
        return stream :keep_open do |out|
          while true
            bytes = data.read(1024*10)
            if bytes.nil? || bytes.length == 0
              break
            else
              if embed && bytes.include?("\n")
                parts = bytes.split("\n", 2)
                out << parts[0]
                out << extra
                out << parts[1]
                embed = nil
              else
                out << bytes
              end
            end
          end
        end
      end

      # Updates a file within an object
      post %r{#{OBJECT_ROUTE_REGEX}/files/(?<path>.+)?} do
        @object = resolveObject()

        if params[:fileToUpload]
          # Parse JSON for drag'n'drop file upload
          params[:path] ||= ""
          @object = @object.set(params[:fileToUpload][:tempfile].read, params[:path] + "/" + params[:fileToUpload][:filename], :message => params[:message])

          params[:path] = params[:path] + "/" + params[:fileToUpload][:filename]
        elsif request.xhr? && !params[:data]
          # Uploading a file by simply posting data to the correct path
          @object = @object.set(request.body.read, params[:path], :message => params[:message])
        else
          # Normal updating of JSON data
          data = params[:data] || {}

          if !data.is_a?(Hash)
            begin
              data = JSON.parse(data)
            rescue
              data = {}
            end
          end

          items = []
          data.each do |key, value|
            # Fix array references
            canonicalKey = key.gsub('(', '[').gsub(')', ']')

            # Rewrite keys
            if params[:rewrite]
              params[:rewrite].each do |k, v|
                canonicalKey.gsub!(k, v)
              end
            end

            # Delete the key when the value is blank
            # (instead of writing empty string)
            if params[:nullify] && params[:nullify][key] && value == ""
              items << [canonicalKey]
            elsif params[:datatype] && params[:datatype][key] == "list"
              # This creates an array with values that are set ('on')
              # This is used to interpret checkbox values that form an array
              #   in the metdata.
              if value.is_a?(Hash)
                value = value.filter{|_,v| v == "on"}.map{|k,v| k}
              end
              items << [canonicalKey, value.to_json]
            elsif params[:datatype] && params[:datatype][key] == "tags"
              # This converts data that is supplied by a tags input field,
              #   which is annoyingly a weird data format for no good reason.
              items << [canonicalKey, JSON.parse(value).map{|i| i["value"]}.to_json]
            else
              # The normal key = value setting...
              items << [canonicalKey, value.to_json]
            end
          end

          # Update the object
          @object = @object.set(items, params[:path], :type => "json", :message => params[:message])
        end

        # If commit bit is set, commit the object as well
        if params[:commit]
          @object = @object.commit
        end

        # Invalidate the object cache
        @@cache.invalidate(@object)

        format = request.preferred_type(['application/json', 'text/html'])
        case format
        when 'application/json'
          content_type "application/json"

          # When json is requested, respond with the API return data
          {
            :url => @object.url(:path => "files/" + params[:path])
          }.to_json
        else
          if request.xhr?
            # Render the file row
            render(:slim, :"objects/_filelist-row", :layout => false, :locals => {
              :object => @object,
              :path => File.dirname(params[:responsePath] || params[:path]),
              :info => @object.retrieveFileStat(params[:responsePath] || params[:path]),
              :basepath => ""
            })
          else
            # In the common case, redirect to the updated file
            if params[:referrer]
              redirect @object.url(:path => params[:referrer])
            else
              redirect @object.url(:path => "files/" + params[:path])
            end
          end
        end
      end

      options %r{#{OBJECT_ROUTE_REGEX}/files/(?<path>.+)?} do
        handleCORS('*')

        content_type "application/octet-stream"

        status 200
      end

      options %r{#{OBJECT_ROUTE_REGEX}/raw/(?<path>.+)?} do
        handleCORS('*')

        content_type "application/octet-stream"

        status 200
      end
    end
  end

  use Controllers::ObjectFilesController
end
