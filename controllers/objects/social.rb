# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../objects"

# @api api
# @!group Object API
class Occam
  module Controllers
    # This controller handles all social/commenting requests for an object.
    class ObjectSocialController < Occam::Controllers::ObjectController
      get %r{#{OBJECT_ROUTE_REGEX}/social(?<extension>\.json)?/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html', 'application/json'])

        if params[:extension] == ".json"
          format = "application/json"
        end

        case format
        when 'application/json'
          content_type 'application/json'
          # TODO: JSON for social tab
        else
          renderObjectView("social")
        end
      end

      # Deletes the given comment if permissions are correct and the comment exists.
      delete %r{#{OBJECT_ROUTE_REGEX}/social/comments/(?<comment_id>.*?)} do
        @object = resolveObject()
        # Fetch the original comment in order to check that we are allowed to edit
        # it.
        match = @object.viewComments(:simple => true, :only_matching => true, :id => params[:comment_id])
        if match.nil? or match.empty? or current_account.nil? or match[0][:person][:id] != current_person.id
          status 404
          return
        end

        record = @object.deleteComment(:comment_id => params[:comment_id])

        record[:numReplies] ||= 0
        record[:replies]    ||= []

        begin
          record[:person].delete(:revision)
          record[:person][:ownerInfo] = record[:person]
          record[:person][:info] = record[:person]
          record[:person][:account] = current_account
          record[:person] = Occam::Person.new(record[:person])
        rescue
          record.delete :person
        end

        if request.xhr?
          content_type "text/html"
          render :slim, :"objects/social/_comment", :layout => false, :locals => {
            :comment => record,
            :object  => @object
          }
        else
          redirect @object.url(:path => "social")
        end
      end

      # Posts the comment to the object.
      post %r{#{OBJECT_ROUTE_REGEX}/social/comments/?} do
        @object = resolveObject()
        if current_account.nil?
          status 404
          return
        end

        record = @object.addComment(:comment => params[:comment], :anon => params[:anon], :inReplyTo => params["inReplyTo"])

        record[:numReplies] ||= 0
        record[:replies] ||= []

        begin
          record[:person].delete(:revision)
          record[:person][:ownerInfo] = record[:person]
          record[:person][:info] = record[:person]
          record[:person][:account] = current_account
          record[:person] = Occam::Person.new(record[:person])
        rescue
          record.delete :person
        end

        if request.xhr?
          content_type "text/html"
          render :slim, :"objects/social/_comment", :layout => false, :locals => {
            :comment => record,
            :object  => @object
          }
        else
          redirect @object.url(:path => "social", :query => {:comment => record[:id], :inReplyTo => params["inReplyTo"]})
        end
      end

      # Edit the given comment with the provided changes.
      post %r{#{OBJECT_ROUTE_REGEX}/social/comments/(?<comment_id>.*)} do
        @object = resolveObject()
        # Fetch the original comment in order to check that we are allowed to edit
        # it.
        match = @object.viewComments(:simple => true, :only_matching => true, :id => params[:comment_id])
        if not match or current_account.nil? or match[0][:person][:id] != current_person.id
          status 404
          return
        end

        record = @object.editComment(:comment => params[:comment], :edit => params[:comment_id])

        record[:numReplies] ||= 0
        record[:replies] ||= []

        begin
          record[:person].delete(:revision)
          record[:person][:ownerInfo] = record[:person]
          record[:person][:info] = record[:person]
          record[:person][:account] = current_account
          record[:person] = Occam::Person.new(record[:person])
        rescue
          record.delete :person
        end

        if request.xhr?
          content_type "text/html"
          render :slim, :"objects/social/_comment", :layout => false, :locals => {
            :comment => record,
            :object  => @object
          }
        else
          redirect @object.url(:path => "social", :query => {:comment => record[:id], :inReplyTo => params["inReplyTo"]})
        end
      end

      # Helper function that fills up the maker feed with comments.
      def appendCommentsToRSS(maker, comments)
        max_time = nil
        comments.each do |comment|
          comment_time = Time.parse(comment[:createTime])
          maker.items.new_item do |item|
            item.guid.content = @object.url(:path => "social/comments/?comment=#{comment[:id]}")
            item.link = @object.url(:path => "social/comments/?comment=#{comment[:id]}")
            item.title = "New comment on \"#{@object.info[:name]}\""
            item.content.content = comment[:content]
            item.updated = comment_time.to_s
          end
          child_max_time = appendCommentsToRSS(maker, comment[:replies])
          # Child must have been created after the parent.
          if child_max_time
            if (max_time and child_max_time > max_time) or not max_time
              max_time = child_max_time
            end
          elsif (max_time and comment_time > max_time) or not max_time
            max_time = comment_time
          end
        end
        return max_time
      end

      # RSS feed for the object.
      get %r{#{OBJECT_ROUTE_REGEX}/social/feed/?} do
        @object = resolveObject()
        # TODO: Provide other updates about the object.
        since = request.env['If-Modified-Since']
        if since
          since = Time.parse(since).strftime("%F %T")
        end

        comments = @object.viewComments(:inReplyTo => params["inReplyTo"], :simple => true, :all => true, :since => since)
        content_type "application/atom+xml"
        last_modified = nil

        # Create an ATOM feed.
        rss = RSS::Maker.make("atom") do |maker|
          maker.channel.author = "OCCAM"
          maker.channel.title = @object.info[:name]
          maker.channel.id = @object.url()

          # If nothing was added recently, just report the current time.
          last_modified = appendCommentsToRSS(maker, comments)
          last_modified ||= Time.now

          maker.channel.updated = last_modified.to_s
        end

        headers['Last-Modified'] = last_modified.strftime("%a, %d %b %Y %T GMT")
        rss.to_s
      end

      # Gathers the comments of the object, in a generic sense.
      get %r{#{OBJECT_ROUTE_REGEX}/social/comments/?} do
        @object = resolveObject()

        # TODO: should be able to pull HTML or JSON for this
        #       the HTML should render the replies as a list

        @object.viewComments(:inReplyTo => params["inReplyTo"], :simple => true).to_json
      end

      # Retrives replies for a comment
      get %r{#{OBJECT_ROUTE_REGEX}/social/comments/(?<comment_id>.*?)/replies/?} do
        @object = resolveObject()
        # TODO: ActivityStreams

        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])
        case format
        when 'application/json'
          content_type "application/json"

          comments = @object.viewComments(:inReplyTo => params[:comment_id], :simple => true)
          comments.to_json
        else
          comments = @object.viewComments(:inReplyTo => params[:comment_id])
          render :slim, :"objects/social/_replies", :layout => !request.xhr?, :locals => {:comments => comments, :object => @object}
        end
      end

      # Retrives a single comment.
      get %r{#{OBJECT_ROUTE_REGEX}/social/comments/(?<comment_id>.*?)/?} do
        @object = resolveObject()
        if current_account.nil?
          status 404
          return
        end

        @object.viewComments(:id => params[:comment_id], :simple => true).to_json
      end
    end
  end

  use Controllers::ObjectSocialController
end
