# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Documentation API
class Occam
  module Controllers
    # Handles routes based on viewing system documentation and other resources.
    class CodeDocumentationController < Occam::Controller
      get '/styleguide' do
        redirect '/styleguide/index.html'
      end

      get '/code-documentation/?:tab?/?' do
        if !(params[:tab].nil? || ["daemon", "api", "web", "javascript", "css"].include?(params[:tab]))
          status 404
          return
        end

        if request.xhr?
          render :slim, :"code-documentation/_#{params[:tab] || "daemon"}", :layout => !request.xhr?
        else
          render :slim, :"code-documentation/index", :layout => !request.xhr?
        end
      end
    end
  end

  use Controllers::CodeDocumentationController
end
# @!endgroup
