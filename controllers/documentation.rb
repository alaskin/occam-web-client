# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Documentation API
class Occam
  module Controllers
    # Handles routes based on viewing system documentation and other resources.
    class DocumentationController < Occam::Controller
      get '/documentation/?:tab?/?' do
        if !(params[:tab].nil? || Occam::Documentation.list.include?((params[:tab] || "").intern))
          status 404
          return
        end

        if request.xhr?
          render :slim, :"documentation/_section", :layout => !request.xhr?, :locals => {:tag => params[:tab]}
        else
          render :slim, :"documentation/index", :layout => !request.xhr?
        end
      end
    end
  end

  use Controllers::DocumentationController
end
# @!endgroup
