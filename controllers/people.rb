# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group People API
class Occam
  module Controllers
    class PersonController < Occam::Controller
      def renderPersonView(tab)
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)

        params[:tab] = tab

        if !person || !person.exists?
          status 404
        else
          if request.xhr? && params[:tab]
            render :slim, :"people/_#{params[:tab]}", :layout => false, :locals => {
              :errors => nil,
              :tab    => params[:tab],
              :help   => params['help'],
              :person => person
            }
          else
            render :slim, :"people/show", :layout => !request.xhr?, :locals => {
              :errors => nil,
              :tab    => params[:tab],
              :help   => params['help'],
              :person => person
            }
          end
        end
      end

      # Searches for people known to this system or federation.
      get '/people' do
        redirect '/search?type=person'
      end

      # Form to create a new person and account on the local system.
      get '/people/new' do
        if !Occam::Config.configuration['allow-signup']
          status 404
          return
        end

        render :slim, :"people/new", :layout => !request.xhr?, :locals => {:errors => nil}
      end

      # Creates an account with the given username and password.
      post '/people' do
        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        if params["newGroup"]
          if !logged_in?
            status 406
            return
          end

          person = current_person.newPerson(params["username"], [params["subtype"]])
          redirect person.url
        else
          username = params["username"]
          password = params["password"]

          account = Occam::Account.create(username, password)
          login(username, password)

          case format
          when 'application/json'
            content_type "application/json"
            {
              :url => account.person.url
            }.to_json
          else
            redirect account.person.url
          end
        end
      rescue Occam::Daemon::Error => e
        status 422

        case format
        when 'application/json'
          {
            'errors': [e.to_s]
          }.to_json
        else
          render :slim, :"people/new", :layout => !request.xhr?, :locals => {
            :errors => [e]
          }
        end
      end

      # Adds an object to the list of recently used objects
      post '/people/:identity/recentlyUsed' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)
        if !person.exists? || person.id != current_person.id
          # TODO: confirm this is not a 406
          status 404
          return
        end

        # Get the object
        object_id = params["object_id"]
        object = Occam::Object.new(:id => object_id)

        if !object.exists?
          # TODO: this is likely an argument error
          status 404
          return
        end

        # Create bookmark
        current_person.createRecentlyUsed(object)

        if request.xhr?
        else
          if request.referrer
            redirect request.referrer
          else
            redirect object.url
          end
        end
      end

      # Form to update a person's password
      get '/people/:identity/password' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)

        if not person.exists?
          status 404
        else
          # TODO: allow password editing for administrators
          if current_person.id != person.id
            status 406
          else
            render :slim, :"people/update-password", :layout => !request.xhr?, :locals => {
              :errors  => nil,
              :person => current_person
            }
          end
        end
      end

      # Update the person's current password
      post '/people/:identity/password' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)

        if not person.exists?
          status 404
        else
          # TODO: allow password editing for administrators
          if current_person.id != person.id
            status 406
          else
            current_account.updatePassword(params[:old_password], params[:password])
            redirect request.referrer
          end
        end
      end

      get '/people/:identity/runs/running' do
        "{}"
      end
    end
  end

  use Controllers::PersonController
end
# @!endgroup
