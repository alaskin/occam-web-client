# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Search API
class Occam
  module Controllers
    # Performs a search.
    class SearchController < Occam::Controller
      options '/search' do
        handleCORS('*')
        status 200
      end

      # Performs a search with the given query and filters.
      get '/search' do
        handleCORS('*')

        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        query = params["search"] || params["query"]

        # Pass along the off types as excluded types
        excludeTypes = ["task"]
        params["search-facet-type-name"] = (params["search-facet-type-name"] || {}).values
        params["search-facet-type"] = (params["search-facet-type"] || {}).values
        (params["search-facet-type-name"] || []).zip(params["search-facet-type"] || []).each do |type, value|
          if value != "on"
            if !excludeTypes.include?(type)
              excludeTypes << type
            end
          elsif excludeTypes.include?(type)
            excludeTypes.delete(type)
          end

          if value == "on" && !params["type"]
            params["type"] = type
          end
        end

        data    = {}
        objects = []
        types   = []
        tags    = []
        excludedTags = []

        tags = []

        # Pre-populate tags
        if params["tags"]
          tags = params["tags"].split(';')
        end

        params["search-facet-tag-name"] = (params["search-facet-tag-name"] || {}).values
        params["search-facet-tag"] = (params["search-facet-tag"] || {}).values
        (params["search-facet-tag-name"] || []).zip(params["search-facet-tag"] || []).each do |tag, value|
          if value == "on"
            tags << tag
          else
            excludedTags << tag
          end
        end

        if params["types"] == "on"
          types = Object.types(:query => query).map do |type|
            { :type => type }
          end
        end

        environments = []
        knownEnvironments = {}
        params["search-facet-environment-name"] = (params["search-facet-environment-name"] || {}).values
        params["search-facet-environment"] = (params["search-facet-environment"] || {}).values
        (params["search-facet-environment-name"] || []).zip(params["search-facet-environment"] || []).each do |name, value|
          if value == "on"
            environments << name
          end
          knownEnvironments[name] = (value == "on")
        end

        architectures = []
        knownArchitectures = {}
        params["search-facet-architecture-name"] = (params["search-facet-architecture-name"] || {}).values
        params["search-facet-architecture"] = (params["search-facet-architecture"] || {}).values
        (params["search-facet-architecture-name"] || []).zip(params["search-facet-architecture"] || []).each do |name, value|
          if value == "on"
            architectures << name
          end
          knownArchitectures[name] = (value == "on")
        end

        if params["type"] == ""
          params["type"] = nil
        end

        if params["objects"] == "on" || params["types"].nil?
          data = Object.fullSearch(:name          => query,
                                   :type          => params["type"],
                                   :subtype       => params["subtype"],
                                   :environments  => environments,
                                   :architectures => architectures,
                                   :viewsType     => params["viewsType"],
                                   :viewsSubtype  => params["viewsSubtype"],
                                   :tags          => tags,
                                   :excludeTypes  => excludeTypes,
                                   :account       => current_account)
          objects = data[:objects]

          # Keep track of which types are turned off because they obviously won't be
          # in the types list. We have to render them.
          data[:excludeTypes] = excludeTypes

          # Ditto for environments/architectures
          if environments.any?
            data[:knownEnvironments] = knownEnvironments
          end

          if architectures.any?
            data[:knownArchitectures] = knownArchitectures
          end

          data[:includedTags] = tags
          data[:excludedTags] = excludedTags
        end

        case format
        when 'application/json'
          {
            "types" => types.map do |type|
              type.update(:icon => Occam::Object.iconURLFor(type[:type]),
                          :smallIcon => Occam::Object.iconURLFor(type[:type], :small => true))
            end,
            "objects" => objects.map do |object|
              object.info.update(:revision  => object.revision,
                                 :icon      => object.iconURL,
                                 :smallIcon => object.iconURL(:small => true))
            end
          }.to_json
        when 'text/html'
          render :slim, :"search/results", :layout => !request.xhr?, :locals => {
            :types   => types,
            :data    => data,
            :objects => objects,
            :query   => query
          }
        end
      end
    end
  end

  use Controllers::SearchController
end
# @!endgroup
