"use strict";

import EventComponent  from './event_component.js';
import Tabs            from './tabs.js';

class Markdown extends EventComponent {
    constructor(element) {
        super();

        var self = this;

        if (element === undefined) {
            return;
        }

        if (Markdown.renderer == undefined) {
            Markdown.renderer = new window.showdown.Converter();
        }

        this.element = element;

        this.input = element.querySelector("textarea.markdown");
        this.preview = element.querySelector(".markdown-preview");
        this.tabs = Tabs.load(element.querySelector("ul.tabs"));

        Markdown._count++;
        this.element.setAttribute('data-markdown-index', Markdown._count);

        Markdown._loaded[this.element.getAttribute('data-markdown-index')] = this;

        this.bindEvents();
    }

    static loadAll(element) {
        var elements = element.querySelectorAll('.markdown-editor');

        elements.forEach(function(element) {
            Markdown.load(element);
        });
    }

    static load(element) {
        if (element === undefined) {
            return null;
        }

        var index = element.getAttribute('data-markdown-index');

        if (index) {
            return Markdown._loaded[index];
        }

        return new Markdown(element);
    }

    updatePreview() {
    }

    showPreview() {
    }

    bindEvents() {
        this.tabs.on('change', (index) => {
            if (index == 1) {
                // Update preview
                var html = Markdown.renderer.makeHtml(this.input.value);
                this.preview.innerHTML = html;
            }
        });
    }
}

Markdown._count  = 0;
Markdown._loaded = {};

export default Markdown;
