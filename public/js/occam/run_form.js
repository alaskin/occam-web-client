"use strict";

import EventComponent from './event_component.js';
import Util           from './util.js';
import Occam          from './occam.js';
import Selector       from './selector.js';
import OccamObject    from './occam_object.js';

/**
 * This class represents a VM builder widget that allows for the selection of
 * the running object and potentially each providing object and backend.
 */
class RunForm extends EventComponent {
    constructor(element) {
        super();

        this.element = element;

        this.bindEvents();
        this.bindObjectEvents();
    }

    static load(element) {
        if (element === undefined) {
            return null;
        }

        var index = element;
        if (typeof element != "number") {
            index = element.getAttribute('data-run-form-index');
        }

        if (index) {
            return RunForm._loaded[index];
        }

        return new RunForm(element);
    }

    static loadAll(element) {
        var runForms = element.querySelectorAll('.task-form');
        runForms.forEach( (element) => {
            RunForm.load(element);
        });
    }

    /**
     * Runs the object as specified by the form.
     */
    run(button = null) {
        let info = this.info;

        if (button) {
            info.submit = button.getAttribute('name');
        }

        this.trigger("run", info);
    }

    /**
     * Retrieves the task information currently reflected by the form.
     */
    get info() {
        let ret = {
          items: [],
          eulas: [],
        };

        let taskItems = this.element.querySelectorAll(".task-form-cell.task-form-item");
        let viewing = null;

        taskItems.forEach( (taskItem) => {
            let item = {};

            if (taskItem.hasAttribute("data-object")) {
                item = this.dataFor(taskItem);

                if (viewing) {
                    item.viewing = viewing;
                    viewing = null;
                }

                if (taskItem.hasAttribute("data-eula-index")) {
                    ret.eulas.push({
                        id: item.id,
                        revision: item.revision,
                        index: parseInt(taskItem.getAttribute("data-eula-index"))
                    });
                }

                if (taskItem.hasAttribute("data-file")) {
                    item.path = taskItem.getAttribute("data-file");
                }

                ret.items.push(item);
            }
            else if (taskItem.hasAttribute("data-backend")) {
                ret.backend = taskItem.getAttribute("data-backend");
            }
            else if (taskItem.hasAttribute("data-dispatch-to")) {
                ret.dispatchTo = taskItem.getAttribute("data-dispatch-to");
            }
            else if (taskItem.hasAttribute("data-viewing")) {
                viewing = this.dataFor(taskItem);

                if (taskItem.hasAttribute("data-eula-index")) {
                    ret.eulas.push({
                        id: viewing.id + "@" + viewing.revision,
                        index: parseInt(taskItem.getAttribute("data-eula-index"))
                    });
                }
            }
        });

        return ret;
    }

    /**
     * Binds events to the form elements.
     */
    bindEvents() {
        let taskItems = this.element.querySelectorAll(".task-form-cell.task-form-item");

        taskItems.forEach( (taskItem) => {
            if (taskItem.classList.contains("bound")) {
                return;
            }

            taskItem.classList.add("bound");

            var collapseButton = taskItem.querySelector("button.collapse");
            if (collapseButton) {
                collapseButton.addEventListener("click", (event) => {
                    event.stopPropagation();

                    taskItem.classList.remove("active");
                    let taskItemQuery = taskItem.querySelector(".task-form-query");
                    if (taskItemQuery) {
                        taskItemQuery.classList.add("reveal");
                    }
                });
            }

            taskItem.addEventListener("click", (event) => {
                taskItem.classList.toggle("active");
                let taskItemQuery = taskItem.querySelector(".task-form-query");
                if (taskItemQuery) {
                    taskItemQuery.classList.toggle("reveal");
                    taskItemQuery.addEventListener("click", (event) => {
                        event.stopPropagation();
                    });
                    if (!taskItemQuery.hasAttribute("data-height-calculated")) {
                        let taskItemHeight = taskItemQuery.clientHeight + "px";
                        taskItemQuery.setAttribute("data-height-calculated", taskItemHeight);
                        taskItemQuery.style.transition = "none";
                        taskItemQuery.style.height = "0";

                        // Allows the transition to see the resolution of a frame at 0
                        window.requestAnimationFrame( () => {
                            // And then in the next frame, starts the actual animation:
                            taskItemQuery.style.transition = "";
                            taskItemQuery.style.height = taskItemHeight;
                        });
                    }
                }
            });
        });

        // The run button
        this.element.querySelectorAll("input.button.run").forEach( (button) => {
            if (button && !button.classList.contains("bound")) {
                button.classList.add("bound");
                button.addEventListener("click", (event) => {
                    // Spawn a run for this.
                    this.run(button);
                });
            }
        });
    }

    /**
     * Retrieves the data that represents the object of the given item.
     */
    dataFor(item) {
        let data = {};

        data.phase = item.getAttribute('data-phase') || 'run';
        data.type  = item.querySelector(".info .type").textContent;
        data.name  = item.querySelector(".info .name").textContent;
        data.icon = item.querySelector("img.icon").getAttribute("src").substring(27);

        data.token = item.getAttribute('data-token');
        data.fullID = item.getAttribute("data-object") || item.getAttribute("data-viewing");
        let parts = data.fullID.split("@");
        data.id = parts[0];
        parts = parts[1].split("/");
        if (parts[1]) {
            data.path = parts.slice(1).join('/');
        }
        parts = parts[0].split("#");
        data.revision = parts[0];
        if (parts[1]) {
            data.link = parts[1];
        }

        return data;
    }

    /**
     * Updates the given task form item with the given object.
     */
    update(item, data) {
        // Do not update if this object is already representing the given data.
        if ((item.getAttribute('data-phase') || 'run') == data.phase && (item.getAttribute('data-object') === data.id + "@" + data.revision)) {
            return;
        }

        item.setAttribute('data-phase', data.phase || 'run');

        let taskItemTypeElement = item.querySelector(".info .type");
        let taskItemNameElement = item.querySelector(".info .name");
        let taskItemIconElement = item.querySelector(".task-form-object > img");
        let taskItemQuery = item.querySelector(".task-form-query");

        if (taskItemTypeElement) {
            taskItemTypeElement.textContent = data.type;
        }

        if (taskItemNameElement) {
            taskItemNameElement.textContent = data.name;
        }

        if (taskItemIconElement) {
            taskItemIconElement.src = taskItemIconElement.getAttribute("src").substring(0, 27) + data.icon;
        }

        // Mark following nodes as loading
        let currentRow = item.parentNode.nextElementSibling;
        while (currentRow) {
            let nextTaskFormObject = currentRow.querySelector(".task-form-object, .task-form-dispatch");
            if (nextTaskFormObject) {
                nextTaskFormObject.classList.add("loading");
            }
            currentRow = currentRow.nextElementSibling;
        }

        let phase = item.getAttribute("data-phase") || "run"

        // Update the form
        let url = new OccamObject(data.id, data.revision).url({
            path: phase + "/queue"
        });

        // Collapse task item query form
        item.classList.remove("active");
        taskItemQuery.classList.add("reveal");

        // Disable the run button
        let button = this.element.querySelector("input.button.run");
        if (button) {
            button.setAttribute("disabled", "");
        }

        Util.get(url, (data) => {
            // Place the data into the DOM
            let dummy = document.createElement("div");
            dummy.setAttribute('hidden', '');
            dummy.innerHTML = data;
            document.body.appendChild(dummy);

            // Get the running object (which is the one we requested)
            let runningObjectTaskItem = dummy.querySelector(".task-form-cell.task-form-item.running-object");
            item.setAttribute("data-object", runningObjectTaskItem.getAttribute("data-object"));
            if (runningObjectTaskItem.hasAttribute("data-eula-index")) {
                item.setAttribute("data-eula-index", runningObjectTaskItem.getAttribute("data-eula-index"));
            }

            // Delete the item nodes
            let currentRow = item.parentNode.nextElementSibling;
            while (currentRow) {
                let nextTaskFormObject = currentRow.querySelector(".task-form-object, .task-form-dispatch");
                let thisRow = currentRow;
                currentRow = currentRow.nextElementSibling;
                if (nextTaskFormObject) {
                    thisRow.remove();
                }
            }

            // Append the other nodes below
            let submitRow = this.element.querySelector(".task-form-row.submit");
            let rows = dummy.querySelectorAll(".task-form-row.supplemental-object");
            rows.forEach( (rowElement) => {
                submitRow.parentNode.insertBefore(rowElement, submitRow);
            });

            // Re-enable button
            if (button) {
                button.removeAttribute("disabled");
            }

            // Bind Events
            this.bindEvents();

            // Trigger event when the form is ready
            this.trigger('ready');
        });
    }

    /**
     * Retrieves the element representing the given item in the task.
     *
     * @returns {HTMLElement} The element at the given index, if it exists, null otherwise.
     */
    itemAt(index) {
        return this.element.querySelector(".task-form-row:nth-of-type(" + (index + 2) + ") .task-form-cell.task-form-item");
    }

    /**
     * Binds events specific to object selection.
     */
    bindObjectEvents() {
        let taskItems = this.element.querySelectorAll(".task-form-cell.task-form-item");

        taskItems.forEach( (taskItem, i) => {
            let taskItemQuery = taskItem.querySelector(".task-form-query");

            if (taskItemQuery) {
                if (taskItemQuery.classList.contains("bound")) {
                    return;
                }

                taskItemQuery.classList.add("bound");

                let selectorElement = taskItemQuery.querySelector("select.selector.viewer");
                if (selectorElement) {
                    let selector = Selector.load(selectorElement);

                    selector.on("change", (item) => {
                        let data = selector.infoFor(item);
                        this.update(taskItem, data);
                    });
                }
            }
        });
    }
}

RunForm._count = 0;
RunForm._loaded = {};

export default RunForm;
