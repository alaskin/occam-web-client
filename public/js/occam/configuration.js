"use strict";

import EventComponent         from './event_component.js';
import Util                   from './util.js';
import Modal                  from './modal.js';
import Tooltip                from './tooltip.js';
import ConfigurationValidator from './configuration_validator.js';

/**
 * This class handles a configuration form.
 */
class Configuration extends EventComponent {
    constructor(element, callback) {
        super();

        // Element is the description of the configuration
        //   as defined in views/experiments/_configuration.slim
        //   .tab-panel.configuration
        this.element = element;

        // Give it a sequential id for local(?) use
        this.element.getAttribute('data-configuration-internal-id', Configuration._count);
        Configuration._count++;

        // Store it for fast usage
        Configuration._loaded.push(this);

        //this.element.children('.configuration-search-options').perfectScrollbar();
        //
        this.schema = {};
        this.label = atob(this.element.getAttribute('data-key'));

        // Calling the prototype Configuration.prototype.load(ready)
        this.load(callback);
    }

    static loadAll(element) {
        var configurations = element.querySelectorAll('.content .tab-panel.configuration');

        configurations.forEach(function(element) {
            // Load all configurations
            Configuration.load(element);
        });
    }

    static load(element, callback) {
        if (element.getAttribute('data-configuration-internal-id')) {
            // Already parsed.
            var configuration = Configuration._loaded[element.getAttribute('data-configuration-internal-id')];
            if (callback !== undefined) {
                callback(configuration);
            }
            return configuration;
        }
        else {
            return new Configuration(element, callback);
        }
    }

    /*
     * This helper method pulls out the values representing an array option
     * from the given ul.configuration-group element.
     */
    static pullArray(dest, element) {
        var key = 0;
        element.querySelectorAll(':scope > li').forEach((item) => {
            var data = {};

            var group = item.querySelector(':scope > .configuration-group');
            Configuration.pullData(data, group);

            dest.push(data);
            key += 1;
        });
    }

    /*
     * This helper method pulls out the values representing a group option
     * from the given ul.configuration-group element.
     */
    static pullData(dest, element) {
        var nesting = element.getAttribute('data-nesting');

        element.querySelectorAll(':scope > li').forEach( (item) => {
            var label = item.querySelector(':scope > label');
            var base64Key = item.getAttribute('data-key');
            var key = atob(base64Key);

            if (label) {
                // Simple Data Point

                var value;
                var input = item.querySelector(':scope > input');

                if (input) {
                    value = input.value;
                }

                var type = label.getAttribute('data-type');
                if (type === "enum") {
                    value = item.querySelector(':scope > .select select option:checked').textContent;
                }
                else if (type === "boolean") {
                    value = item.querySelector(':scope > .slider-checkbox > input[type="checkbox"]').checked;
                }
                else if (type === "float") {
                    value = parseFloat(value);
                }
                else if (type === "int") {
                    value = parseInt(value);
                }
                else if (type === "datapoints") {
                    value = [];

                    // Go into datapoint list
                    item.querySelector(':scope > ul > li').forEach((dataPoint) => {
                        // Form the data point metadata object
                        var dataPointMetadata = {};

                        // Pull out the object
                        var object = dataPoint.querySelector(':scope > span.output');
                        var objectId = object.getAttribute('data-object-id');
                        var objectRevision = object.getAttribute('data-object-revision');

                        dataPointMetadata.object = {};
                        dataPointMetadata.object.id = objectId;
                        dataPointMetadata.object.revision = objectRevision;

                        dataPointMetadata.nesting = [];

                        // Pull out the datapoint nesting
                        var keys = dataPoint.querySelectorAll(':scope > span:not(.output)');
                        keys.forEach((key) => {
                            if (key.classList.contains('range')) {
                                var list = [];

                                key.querySelectorAll(':scope > span').forEach((subKey) => {
                                    if (subKey.classList.contains('range')) {
                                        // TODO: Use nth-child or something instead
                                        var min = subKey.querySelector(':scope > span').textContent;
                                        var max = subKey.querySelectorAll(':scope > span').slice(1,2).textContent;
                                        list.push([parseInt(min), parseInt(max)]);
                                    }
                                    else {
                                        list.push(parseInt(subKey.textContent));
                                    }
                                });
                                dataPointMetadata.nesting.push(list);
                            }
                            else {
                                dataPointMetadata.nesting.push(key.textContent);
                            }
                        });

                        // Push the data point metadata
                        value.push(dataPointMetadata);
                    });
                }

                dest[key] = value;
            }
            else {
                // Group / Array
                var group = item.querySelector(':scope > .configuration-group');

                if (item.querySelector(':scope > .element')) {
                    dest[key] = [];
                    Configuration.pullArray(dest[key], group);
                }
                else {
                    dest[key] = {};
                    Configuration.pullData(dest[key], group);
                }
            }
        });
    }

    dataPointsData() {
        var dataPointElements = this.dataPoints();

        // For each data point element, retrieve the datapoints listed
        dataPointElements.forEach((dataPoint) => {
        });
    }

    /**
     * This method will pull out the data from the configuration form and return
     * it as an object.
     */
    data() {
        // Parse through the DOM and pull out input values
        if (this._data === undefined) {
            this._data = {};

            var start = this.element.querySelector('.configuration-search-options > .configuration-group');
            if (start) {
                Configuration.pullData(this._data, start);
            }
            else {
                this._data = undefined;
            }
        }

        return this._data;
    }

    /**
     * This method returns a collection of all of the datapoint elements.
     */
    dataPoints() {
        return this.element.querySelectorAll('label.datapoints');
    }

    dataPointLabelFor(datapoint) {
        var thisLabel   = datapoint.textContent;
        var newElement  = Util.getParents(datapoint, 'div.element')[0];
        var arrayParent = Util.getParents(datapoint, 'li.array')[0];

        var name = datapoint.textContent;
        if (arrayParent) {
            // Get the name of the array element (for example, it might be 'Groups')
            name = arrayParent.querySelector(':scope > h2 > .group-label').textContent;
        }

        if (newElement) {
            // Yield the name of the array
            var index = Util.getChildIndex(Util.getParents(datapoint, 'li.element')[0]);
            name = name + "[" + index + "]";
        }

        return [name, thisLabel];
    }

    /**
     * Adds the given data point to the given key.
     */
    addDataPoint(objectId, objectRevision, nesting, datapoint, dataViewer) {
        // Nesting is a list of keys: ['key', 'key', 'key', [0, 1, 2], 'key', ...]
        // The nesting will determine where in the document to find the data points
        //
        // datapoint is the label element of the datapoint to add
        var newElement = Util.getParents(datapoint, 'div.element')[0];
        if (newElement) {
            // Click on that "add-element" button first
            newElement.nextElementSibling.querySelector(':scope > input.add-element').trigger('click');

            // Then set datapoint to be the new element
            datapoint = newElement.nextElementSibling.querySelector(':scope > li:last-child').querySelector('label.datapoints');
        }

        // Add datapoint to the given datapoint collection
        var dataPointList = datapoint.parentNode.querySelector(':scope > ul');
        var dataPointItem = document.createElement("li");
        dataPointItem.classList.add("datapoint");
        var objectLink    = document.createElement("span");
        objectLink.classList.add("output");
        var objectURL     = "/" + objectId + "/" + objectRevision;
        objectLink.setAttribute('data-object-id', objectId);
        objectLink.setAttribute('data-object-revision', objectRevision);
        var objectAnchor = document.createElement("a");
        objectAnchor.setAttribute('href', objectURL);
        objectLink.appendChild(objectAnchor);
        dataPointItem.appendChild(objectLink);
        nesting.reverse().forEach( (key) => {
            if (key instanceof Array) {
                var rangeItem = document.createElement("span");
                rangeItem.classList.add("range");
                // Add text node
                rangeItem.appendChild(document.createTextNode('['));
                key.forEach((item) => {
                    if (item instanceof Array) {
                        // Range
                        let rangeSpan = document.createElement("span");
                        rangeSpan.classList.add("range");
                        let minSpan = document.createElement("span");
                        minSpan.classList.add("min");
                        minSpan.textContent = item[0];
                        let maxSpan = document.createElement("span");
                        maxSpan.classList.add("max");
                        maxSpan.textContent = item[1];
                        rangeSpan.appendChild(minSpan);
                        rangeSpan.appendChild(document.createTextNode(".."));
                        rangeSpan.appendChild(maxSpan);
                        rangeItem.appendChild(rangeSpan);
                    }
                    else {
                        let rangeSpan = document.createElement("span");
                        rangeSpan.classList.add("item");
                        rangeSpan.textContent = item;
                        rangeItem.appendChild(rangeSpan);
                    }
                });
                rangeItem.appendChild(document.createTextNode(']'));
                dataPointItem.appendChild(rangeItem);
            }
            else if (typeof key == "number") {
                let rangeSpan = document.createElement("span");
                rangeSpan.classList.add("range");
                let rangeKey = document.createElement("span");
                rangeKey.classList.add("item");
                rangeKey.textContent = key;

                rangeSpan.appendChild(document.createTextNode("["));
                rangeSpan.appendChild(rangeKey);
                rangeSpan.appendChild(document.createTextNode("]"));

                dataPointItem.appendChild(rangeSpan);
            }
            else {
                let rangeKey = document.createElement("span");
                rangeKey.classList.add("item");
                rangeKey.textContent = key;
                dataPointItem.appendChild(rangeKey);
            }
        });
        dataPointList.appendChild(dataPointItem);

        // Update 'data'
        this.updateKey(datapoint);

        //
        if (newElement) {
            var labels = this.dataPointLabelFor(datapoint);
            var dataLabel = labels[0];
            var dataKey   = labels[1];

            // Add the datapoint back to the data viewer
            if (dataViewer) {
                dataViewer.addTarget(dataLabel, dataKey, this, datapoint);
            }
        }
    }

    removeDataPoint(nesting) {
    }

    bindExpandEvents(element) {
        function expandField(element) {
            if (element) {
                element.classList.toggle('shown');

                // Get associated description div
                if (element.classList.contains('shown')) {
                    element.parentNode.querySelector(".description").style.display = "block";
                }
                else {
                    element.parentNode.querySelector(".description").style.display = "";
                }
            }
        }

        // Description Expanders
        element.querySelectorAll('li:not(.field) > label').forEach((label) => {
            label.addEventListener('click', function(e) {
                expandField(this.parentNode.querySelector('span.expand'));
            });
        });

        element.querySelectorAll('li:not(.field) > span.expand').forEach((expander) => {
            expander.addEventListener('click', function(e) {
                expandField(this);
            });
        });

        // Group Expanders
        element.querySelectorAll('h2').forEach((header) => {
            header.addEventListener('click', function(e) {
                var span = this.querySelector(':scope > span.expand');
                span.classList.toggle('shown');

                // Get associated description div
                if (span.classList.contains('shown')) {
                    //span.parentNode.parentNode.querySelector(':scope > ul').slideDown(150);
                    span.textContent = "\u25be";
                }
                else {
                    //span.parentNode.parentNode.querySelector(':scope > ul').slideUp(150);
                    span.textContent = "\u25b8";
                }
            });
        });

        return this;
    }

    /*
     * This method decodes the input name field or nesting fields into an array
     * of keys.
     */
    static decodeNesting(nesting) {
        // Nesting looks like this:
        // data[<connection-index>][<configuration-index>][key1][key2] ...

        // Get the parts of the nesting that matter. That is, remove the
        // "data" string preceding the group key indicies.
        // TODO: is it 3 or 1? are there indexes???
        var parts = nesting.split('[').slice(1);
        var keys  = parts.map( (e) => atob(e.slice(0, e.length-1)) );

        return keys;
    }

    updateKey(element) {
        // Update the data with any changed field
        var dataElement = Util.getParents(element, "li")[0];
        var dataGroup   = Util.getParents(element, '.configuration-group')[0];
        var base        = Util.getParents(element, '.configuration-search-options')[0];
        var nav         = base.parentNode.querySelector(':scope > ul.configuration-nav');

        // Retrieve keys
        var nesting   = Configuration.decodeNesting(dataGroup.getAttribute('data-nesting'));
        var base64Key = dataElement.getAttribute('data-key');
        var key       = atob(base64Key);

        // Update the data
        var data = this.data();
        nesting.forEach( (parentKey) => {
            if (data instanceof Array) {
                data = data[parseInt(parentKey)];
            }
            else {
                if (data[parentKey] === undefined) {
                    data[parentKey] = {};
                }
                data = data[parentKey];
            }
        });
        var value = "";
        var elemParent = element.parentNode;
        if (elemParent.classList.contains("tuple") || elemParent.classList.contains("number") || elemParent.classList.contains("slider-checkbox")) {
            elemParent = elemParent.parentNode;
        }
        var type = elemParent.querySelector(':scope > label').getAttribute('data-type');
        if (type === "enum") {
            value = element.querySelector('select option:checked').textContent;
        }
        else if (type === "boolean") {
            value = element.checked;
        }
        else if (type === "float") {
            value = element.value;
            value = parseFloat(value);
        }
        else if (type === "int") {
            value = element.value;
            value = parseInt(value);
        }
        else if (type === "string" || type === "color") {
            value = element.value;
        }
        else if (type === "datapoints") {
            value = [];

            // TODO: only parse one datapoint
            var group = {};
            Configuration.pullData(group, dataGroup);
            value = group[key];
        }

        if (data !== undefined) {
            data[key] = value;
        }

        var fullKey = btoa(dataGroup.getAttribute('data-nesting') + "[" + base64Key + "]").replace(/=/g, ''); 

        var navName  = nav.querySelector('li[data-ref-key="' + fullKey + '"] span.sublabel');
        var navColor = nav.querySelector('li span.color[data-ref-key="' + fullKey + '"]');
        if (navName) {
            navName.textContent = value;
        }
        if (navColor) {
            navColor.style.backgroundColor = value;
        }
        var headerName  = base.querySelector('h3.subheader[data-ref-key="' + fullKey + '"] span.sublabel');
        var headerColor = base.querySelector('h3.subheader span.color[data-ref-key="' + fullKey + '"]');
        if (headerName) {
            headerName.textContent = value;
        }
        if (headerColor) {
            headerColor.style.backgroundColor = value;
        }

        this.trigger('change', this.data());
    }

    // Internal method to update the positioning of the 'units' text.
    _updateUnitsPlacement(inputElement) {
        if (inputElement.hasAttribute("data-units")) {
            // Get width of text inside the inputElement
            var computedStyle = window.getComputedStyle(inputElement);
            var computedFont = computedStyle.fontWeight + " " + computedStyle.fontSize + " " + computedStyle.fontFamily;
            var textWidth = Util.getTextWidth(inputElement.value, computedStyle.font);

            var unitsElement = inputElement.nextElementSibling;
            var unitsWidth = 0;
            if (!unitsElement || !unitsElement.classList.contains("units")) {
                unitsElement = document.createElement("span");
                unitsElement.classList.add("units");
                unitsElement.textContent = "\xa0" + inputElement.getAttribute("data-units");
                unitsElement.style.font = computedStyle.font;
                inputElement.parentNode.insertBefore(unitsElement, inputElement.nextElementSibling);

                unitsWidth = Util.getTextWidth(unitsElement.textContent, computedFont);

                //inputElement.style.width = (parseInt(computedStyle.width) - unitsWidth) + "px";
                inputElement.style.paddingRight = parseInt(computedStyle.paddingRight) + unitsWidth + "px";
            }
            else {
                unitsWidth = Util.getTextWidth(unitsElement.textContent, computedFont);
            }

            var newRight = parseInt(computedStyle.width) - parseInt(computedStyle.paddingLeft) - unitsWidth + inputElement.scrollLeft - (inputElement.scrollLeft ? 0 : parseInt(computedStyle.paddingLeft)) - textWidth;
            newRight = Math.max(newRight, parseInt(computedStyle.paddingLeft));
            unitsElement.style.right = newRight + "px";
        }
    }

    /**
     * This method will bind events that are triggered when values change. For
     * instance, when values will disable or change the visibility of other
     * fields.
     */
    bindValueEvents(element) {
        element.querySelectorAll('input:not(.button)').forEach( (input) => {
            input.addEventListener('change', (event) => {
                this.updateKey(input);

                // Update placement of 'units' description
                this._updateUnitsPlacement(input);
            });

            input.addEventListener('keypress', (event) => {
                this.updateKey(input);

                // Update placement of 'units' description
                this._updateUnitsPlacement(input);
            });

            input.addEventListener('input', (event) => {
                this.updateKey(input);

                // Update placement of 'units' description
                this._updateUnitsPlacement(input);
            });

            // Initial placement of 'units' description
            this._updateUnitsPlacement(input);
        });

        // Displays values only when enabled
        element.querySelectorAll('li > .select > select').forEach( (select) => {
            function updateVisibility(element) {
                var actionList = ["enables", "disables", "shows", "hides"];

                actionList.forEach( (action) => {
                    var count = parseInt(element.getAttribute("data-" + action + "-count"));
                    var captured = {};

                    for (var i = 0; i < count; i++) {
                        var is = element.getAttribute("data-" + action + "-is-" + i);
                        var input = element.querySelector(":scope > option:checked").textContent;

                        var selection = [];
                        var keyParts = element.getAttribute("data-" + action + "-key-" + i).split(':');
                        var key = keyParts[1];
                        var group = keyParts[0];

                        if (group === "all") {
                            let selector = '.configuration-group li[data-key="'+key+'"]';
                            selection = this.element.querySelectorAll(selector);
                        }
                        else if (group === "sibling") {
                            let selector = 'li[data-key="'+key+'"]';
                            selection = element.parentNode.parentNode.parentNode.querySelectorAll(selector);
                        }

                        /* jshint ignore:start */
                        if (is === input) {
                            captured[key] = true;
                            selection.forEach((selected) => {
                                if (action == "enables" || action == "disables") {
                                    selected.querySelectorAll('input, select').forEach((selectedInput) => {
                                        selectedInput.disabled = (action == "enables");
                                    });
                                }
                                else {
                                    selected.style.display = (action == "shows" ? "" : "none");
                                }
                            });
                        }
                        else if (!captured[key]) {
                            selection.forEach( (selected) => {
                                if (action == "enables" || action == "disables") {
                                    selected.querySelectorAll('input, select').forEach( (selectedInput) => {
                                        selectedInput.disabled = (action == "enables");
                                    });
                                }
                                else {
                                    selected.style.display = (action == "hides" ? "" : "none");
                                }
                            });
                        }
                        /* jshint ignore:end */
                    }
                });
            }

            select.addEventListener('change', (e) => {
                updateVisibility(select);

                // Update data
                this.updateKey(select.parentNode);
            });

            select.addEventListener('keyup', (e) => {
                updateVisibility(select);

                // Update data
                this.updateKey(select.parentNode);
            });

            updateVisibility(select);
        });

        return this;
    }

    bindValidation(element) {
        var form = element;

        // TODO: handle the hinting for jQuery + Parsley
        if (form) {
            var parsley = window.$(form).parsley();
            parsley.destroy();
            parsley = window.$(form).parsley({
                errorsContainer: function(field) {
                    var elem = document.querySelector('input[data-parsley-id="'+field.__id__+'"]');
                    var elemParent = elem.parentNode;
                    if (elemParent.classList.contains("tuple") || elemParent.classList.contains("number")) {
                        elemParent = elemParent.parentNode;
                    }

                    var descriptionElement = elemParent.querySelector('.description');
                    var ret = descriptionElement;
                    if (descriptionElement) {
                        ret = descriptionElement.querySelector(".errors");
                        if (!ret) {
                            ret = document.createElement("div");
                            ret.classList.add("errors");
                            descriptionElement.insertBefore(ret, descriptionElement.children[0]);
                        }
                    }
                    return window.$(ret);
                }
            });
        }

        return this;
    }

    bindColorPicker(element) {
        // TODO: set the input with any new value from the color picker
        // TODO: use js color picker and not spectrum
        /*
    var inputs = element.querySelectorAll('input.color');
    inputs.forEach((input) => {
      var input = $(this);
      input.spectrum({
        showInput: true,                    // Show a textbox
        showAlpha: true,                    // Allow alpha selection
        showPalette: true,
        showSelectionPalette: true,         // Show 'recently-used' palette
        localStorageKey: "spectrum.widget", // Shared palette
        clickoutFiresChange: true,          // Allow blur to set
        replacerClassName: 'color-picker',  // Class name for custom styling
        preferredFormat: "rgb"              // Ensure set value is a hex value
      }).on("dragstop.spectrum", function(event, color) {
        input.val(color.toRgbString());
        input.trigger('change');
      }).on("hide.spectrum", function(event, color) {
        if (color.toRgbString() != input.val()) {
          input.val(color.toRgbString());
          input.trigger('change');
        }
      });
    });*/

        return this;
    }

    bindArrayEvents(element) {
        element.querySelectorAll('input.delete.circle').forEach( (deleteButton) => {
            deleteButton.addEventListener('click', (event) => {
                event.preventDefault();
                event.stopPropagation();

                var header = deleteButton.parentNode;
                var key    = header.getAttribute('data-ref-key');

                // Find the array element region
                var arrayElement = Util.getParents(deleteButton, 'li.element')[0];
                var arrayGroup   = arrayElement.querySelector(':scope > ul');
                var arrayIndex   = Util.getChildIndex(arrayElement);

                // Find the navigation region
                var navRegion = Util.getParents(deleteButton, '.configuration-search-options')[0].parentNode.querySelector('ul.configuration-nav');
                var navHeader = navRegion.querySelector('li[data-ref-key="' + key + '"]');

                var baseElement = arrayElement.parentNode.parentNode.querySelector(':scope > div.element > ul');
                var baseNesting = baseElement.getAttribute('data-nesting');

                // Delete from configuration data
                // First, retrieve the base nesting from the template array element:
                var nestingParts = Configuration.decodeNesting(baseNesting);
                var data = this.data();
                // Find the section in the data that is recording this array element
                nestingParts.forEach( (parentKey) => {
                    if (data instanceof Array) {
                        data = data[parseInt(parentKey)];
                    }
                    else {
                        data = data[parentKey];
                    }
                });
                // Remove that data
                data.splice(arrayIndex, 1);

                // Update keys/names to reflect change in index for every following element
                var elements = arrayElement.parentNode.querySelectorAll(':scope > li.element');
                elements.slice(arrayIndex+1).forEach((subElement) => {
                    var nextArrayElement = subElement.querySelector(':scope > ul.configuration-group');
                    var index = Util.getChildIndex(subElement) - 1;
                    var nextNesting = baseNesting + "[" + (btoa(index).replace(/=/g,'')) + "]";
                    nextArrayElement.setAttribute('data-nesting', nextNesting);
                    nextArrayElement.querySelectorAll('.configuration-group').forEach((groupElement) => {
                        var group = Util.getParents(groupElement, '.configuration-group')[0];
                        var groupNesting = group.getAttribute('data-nesting');

                        groupElement.setAttribute('data-nesting', groupNesting + "[" + groupElement.getAttribute('data-key') + "]");
                    });

                    // Update header and nav bar
                    var header = subElement.querySelector(':scope > h3.subheader');

                    if (header) {
                        var sublabel = header.querySelector(':scope > span.sublabel');
                        var color = header.querySelector(':scope > span.color');

                        var nextNavHeader   = navRegion.querySelector('li[data-ref-key="' + header.getAttribute('data-ref-key') + '"]');
                        var nextNavSublabel = nextNavHeader.querySelector('span.sublabel');
                        var nextNavColor    = nextNavHeader.querySelector('span.color');
                        var currentKey      = header.getAttribute('data-ref-key');
                        var currentColorKey = nextNavColor.getAttribute('data-ref-key');

                        var nextBaseNesting = nextNesting.slice(baseNesting.length);
                        var nextKey         = atob(currentKey).slice(baseNesting.length);
                        nextKey = nextKey.replace(/\[[^\]]+\]/, '');
                        nextKey = baseNesting + nextBaseNesting + nextKey;

                        var nextColorKey    = atob(currentColorKey).slice(baseNesting.length);
                        nextColorKey = nextColorKey.replace(/\[[^\]]+\]/, '');
                        nextColorKey = baseNesting + nextBaseNesting + nextColorKey;

                        nextKey = btoa(nextKey).replace(/=/g,'');
                        nextColorKey = btoa(nextColorKey).replace(/=/g,'');
                        if (color) {
                            nextNavColor.setAttribute('data-ref-key', nextColorKey);
                            color.setAttribute('data-ref-key', nextColorKey);
                        }

                        nextNavHeader.setAttribute('data-ref-key', nextKey);
                        header.setAttribute('data-ref-key', nextKey);
                    }
                });

                // Delete array data from the page
                arrayElement.remove();

                // Remove from navigation
                navHeader.remove();

                // Save changes
                this.trigger('change', this.data());
            });
        });

        // Add Button
        element.querySelectorAll('input.add-element').forEach( (addElement) => {
            addElement.addEventListener('click', (event) => {
                event.preventDefault();
                event.stopPropagation();

                var baseElement = Util.getParents(addElement, 'li.array')[0];
                var newElement = baseElement.querySelector('div.element > ul').cloneNode(true);
                newElement.querySelectorAll('.color-picker').forEach((colorPicker) => {
                    colorPicker.remove();
                });

                // Get the next index
                var index = addElement.parentNode.querySelectorAll(':scope > li').length;

                // Update nesting
                var nesting = newElement.getAttribute('data-nesting');

                // Add to data
                var nestingParts = Configuration.decodeNesting(nesting);

                var data = this.data();
                nestingParts.forEach( (parentKey) => {
                    if (data instanceof Array) {
                        data = data[parseInt(parentKey)];
                    }
                    else {
                        data = data[parentKey];
                    }
                });

                nesting = nesting + "[" + (btoa(index).replace(/=/g, '')) + "]";
                newElement.setAttribute('data-nesting', nesting);

                newElement.querySelectorAll('.configuration-group').forEach((groupElement) => {
                    var group = Util.getParents(groupElement, '.configuration-group')[0];
                    var baseNesting = group.getAttribute('data-nesting');

                    groupElement.setAttribute('data-nesting', baseNesting + "[" + groupElement.getAttribute('data-key') + "]");
                });

                // Update entries to their default values if they are in lists
                var inputs = newElement.querySelectorAll('input[default_count]');
                inputs.forEach((input) => {
                    var defaultCount = parseInt(input.getAttribute('default_count'));
                    var defaultIndex = index % defaultCount;
                    input.value = atob(input.getAttribute('default_' + defaultIndex));
                });

                // TODO: do the same for enum values in <select>

                var newItem = document.createElement("li");
                newItem.classList.add("element");
                newItem.appendChild(baseElement.querySelector('div.element > h3').cloneNode(true));
                newItem.appendChild(newElement);

                addElement.parentNode.insertBefore(newItem, addElement);

                var elementData = {};

                Configuration.pullData(elementData, newElement);

                data.push(elementData);

                // Add ref keys to headers
                var header = newItem.querySelector('h3.subheader');
                var sublabel = header.querySelector('span.sublabel');
                var color  = header.querySelector('span.color');

                var newKey = btoa(nesting).replace(/=/g, '');
                newItem.setAttribute('id', newKey);

                var newHeaderKey = btoa(nesting + atob(header.getAttribute('data-nesting'))).replace(/=/g, '');
                header.setAttribute('data-ref-key', newHeaderKey);

                var newColorKey;
                if (color) {
                    newColorKey = btoa(nesting + atob(color.getAttribute('data-nesting'))).replace(/=/g, '');
                    color.setAttribute('data-ref-key', newColorKey);
                }

                // Update the name and color of the element if necessary
                var partialNesting = atob(color.getAttribute('data-nesting'));
                var colorSelector = 'li[data-key="' + partialNesting.slice(1,partialNesting.length-1).split('"]["').join('"] li[data-key="') + '"]';
                partialNesting = atob(header.getAttribute('data-nesting'));
                var headerSelector = 'li[data-key="' + partialNesting.slice(1,partialNesting.length-1).split('"]["').join('"] li[data-key="') + '"]';
                // Get the value in the field
                //
                sublabel.textContent = newElement.querySelector(headerSelector).querySelector('input').value;
                // Get the value in the field
                if (color) {
                    color.style.backgroundColor = newElement.querySelector(colorSelector).querySelector('input').value;
                }

                // Add navigation bar entries if necessary

                // We add them to the nav bar if they are off the top-level group
                if (Util.getParents(baseElement, 'ul.configuration-group').length == 1) {
                    // Find the navigation region
                    var navRegion = Util.getParents(addElement, '.configuration-search-options')[0].parentNode.querySelector(':scope > ul.configuration-nav');

                    // Find the last array element in the navigation bar
                    var lastHeader = navRegion.querySelector('li[data-key="' + baseElement.getAttribute('data-key') + '"]:last-of-type');

                    // Add a new navigation entry after this last one
                    //$("<li><a><span class='sublabel'></span></a></li>");
                    var newSublabel = document.createElement("span");
                    newSublabel.classList.add('sublabel');
                    var newNavHeaderAnchor = document.createElement("a");
                    newNavHeaderAnchor.appendChild(newSublabel);
                    var newNavHeader = document.createElement("li");
                    newNavHeader.appendChild(newNavHeaderAnchor);
                    newNavHeader.setAttribute('data-ref-key', newHeaderKey);
                    newNavHeaderAnchor.setAttribute('href', '#' + newKey);

                    newSublabel.textContent = sublabel.textContent;

                    if (color) {
                        var newColorSpan = document.createElement("span");
                        newColorSpan.classList.add('color');
                        newColorSpan.setAttribute('data-ref-key', newColorKey);
                        newColorSpan.style.backgroundColor = color.style.backgroundColor;

                        newNavHeaderAnchor.insertBefore(newColorSpan, newSublabel);
                    }

                    lastHeader.parentNode.insertBefore(newNavHeader, lastHeader.nextElementSibling);

                    this.bindNavigationEvents(this.element);
                }

                this.bindEvents(newItem);

                addElement.addEventListener('click', (event) => {
                    this.trigger('change', this.data());
                });
            });
        });

        return this;
    }

    bindNavigationEvents(element) {
        var navigationBar = element.querySelector('.configuration-nav');

        var navigationLinks = navigationBar.querySelectorAll('li');

        navigationLinks.forEach((link) => {
            if (link.classList.contains('bound')) {
                return;
            }

            link.addEventListener('click', (event) => {
                event.preventDefault();
                event.stopPropagation();

                var panel = element.querySelector('.configuration-search-options');
                var targetId = link.querySelector('a').getAttribute('href');
                var target = element.querySelector(targetId);
                // TODO: handle the navigation scrolling
                var sectionY = target.position().top + panel.scrollTop();
                panel.stop().animate({'scrollTop': sectionY}, 400);
            });
        });
    }

    bindEvents(element) {
        // Expand/Collapse sections and descriptions
        this.bindExpandEvents(element);

        // Disabling/Hide events for value changes
        this.bindValueEvents(element);

        // Validation
        this.bindValidation(element);

        // Color Picker
        this.bindColorPicker(element);

        // Array Add
        this.bindArrayEvents(element);

        // Navigation Bar
        this.bindNavigationEvents(element);

        // Submission??
    }

    /**
     * Returns the id of the object this configuration belongs to.
     */
    objectId() {
        return this.element.getAttribute('data-object-id');
    }

    /**
     * Returns the revision of the object this configuration belongs to.
     */
    objectRevision() {
        return this.element.getAttribute('data-object-revision');
    }

    /**
     * Returns the connection index within the workflow this configuration is
     * attached to.
     */
    connectionIndex() {
        return this.element.getAttribute('data-connection-index');
    }

    /**
     * Returns the configuration index within the object this configuration
     * belongs to.
     */
    inputIndex() {
        return this.element.getAttribute('data-input-index');
    }

    load(ready) {
        var object_id            = this.objectId();
        var object_revision      = this.objectRevision();

        var base_object_id       = this.element.getAttribute('data-base-object-id');
        var base_object_revision = this.element.getAttribute('data-base-object-revision');

        var connection_index     = this.element.getAttribute('data-connection-index');
        var input_index          = this.element.getAttribute('data-input-index');

        var url = this.element.getAttribute('data-url');

        // If this is attached to an experiment, then we need to read the
        // experiment's rendering of the configuration form. This rendering will
        // also attach the experiment's current values for the options instead
        // of the defaults.
        if (!url) {
            url = "";
            /*if (experiment_id) {
          url = "/worksets/"       + workset_id    + "/" + workset_revision    +
                "/experiments/"    + experiment_id + "/" + experiment_revision +
                "/connections/"    + connection_index +
                "/configurations/" + configuration_index;
        }
        else if (paper_id) {
          url = "/worksets/"       + workset_id    + "/" + workset_revision +
                "/"        + paper_id      + "/" + paper_revision   +
                "/pages/"          + page_index    +
                "/items/"          + item_index    +
                "/configurations/" + configuration_index;
        }
        else {*/
            // Just get the default rendering of the form
            /*}*/

            if (!object_id && base_object_id) {
                url = "/"        + base_object_id + "/" + base_object_revision +
                    "/inputs/" + input_index;
            }
            else if (object_id) {
                url = "/"        + object_id + "/" + object_revision +
                    "/file";
            }
        }

        if (url === "") {
            return;
        }

        /*
        if (Occam.DEBUG) {
        */
            window.console.debug("Loading Configuration from " + url);
        /*
        }
        */

        this.element.classList.add("loading");

        // Pull down the HTML
        Util.get(url, (data) => {
            this.element.querySelector(":scope > .configuration").innerHTML = data;

            this.bindEvents(this.element);
            this.element.classList.remove('loading');
            this.element.style.height = '';

            Modal.loadAll(this.element);
            Tooltip.loadAll(this.element);

            if (ready !== undefined) {
                ready.call(this, this);
            }
        }, "text/html+configuration");

        return this;
    }
}

Configuration._count  = 0;
Configuration._loaded = [];

export default Configuration;
