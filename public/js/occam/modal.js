"use strict";

import EventComponent from './event_component.js';
import Util           from './util.js';
import Occam          from './occam.js';

class Modal extends EventComponent {
    constructor(element) {
        super();

        if (element === undefined) {
            return;
        }

        this.element = element;

        if (Modal.element === undefined) {
            Modal.element     = document.querySelector("body > .modal-window");
            Modal.content     = Modal.element.querySelector(":scope > .content");
            Modal.closeButton = Modal.element.querySelector(":scope > .close");
            Modal.element.addEventListener("click", function(event) {
                Modal.close();
            });

            window.addEventListener("keyup",   Modal.handleKeyEvent);
            window.addEventListener("keydown", Modal.handleKeyEvent);

            Modal.content.addEventListener("click", function(event) {
                event.stopPropagation();
            });
        }

        Modal._count++;
        this.element.setAttribute('data-loaded-index', Modal._count);

        Modal._loaded[this.element.getAttribute('data-loaded-index')] = this;

        this.bindEvents();
        this.events = {};
    }

    static loadAll(element) {
        var modals = element.querySelectorAll('a.modal');

        modals.forEach(function(element) {
            Modal.load(element);
        });
    }

    static load(element) {
        if (element === undefined) {
            return null;
        }

        var index = element.getAttribute('data-loaded-index');

        if (index) {
            return Modal._loaded[index];
        }

        return new Modal(element);
    }

    static handleKeyEvent(event) {
        if (Modal.isOpen()) {
            if (event.key == "Escape" || event.code == "Escape" || event.keyCode == 27) {
                Modal.close();
            }
        }
    }

    /**
     * Returns the current option set for the given key.
     */
    static optionFor(key) {
        return this._options[key];
    }

    bindEvents() {
        this.element.addEventListener("click", (event) => {
            event.preventDefault();

            var options = {};

            // Gather generic options
            let attrs = this.element.attributes;
            for (let i = 0; i < attrs.length; i++) {
                let attribute = attrs[i];
                if (attribute.name.indexOf("data-modal") == 0) {
                    if (this.element.hasAttribute(attribute.name)) {
                        options[attribute.name.split("data-modal-")[1]] = this.element.getAttribute(attribute.name);
                    }
                }
            }

            // Gather 'large' option
            if (this.element.hasAttribute("data-modal-large")) {
                options.large = true;
            }

            // Gather 'full' option
            if (this.element.hasAttribute("data-modal-full")) {
                options.full = true;
            }

            Modal.open(this.element.getAttribute("data-url") || this.element.getAttribute("href"), options);
        });
    }

    static isOpen() {
        return Modal.element && Modal.element.style.display == "flex";
    }

    static open(url, options) {
        // Reset event notifications
        Modal.onSubmit = null;

        options = options || {};
        Modal._options = options;

        // Makes the Modal full sized (almost the size of the browser)
        Modal.element.classList.remove("large");
        if (options.large) {
            Modal.element.classList.add("large");
        }

        // Makes the modal automatically its maximum height
        Modal.element.classList.remove("full");
        if (options.full) {
            Modal.element.classList.add("full");
        }

        Modal.content.style.display = "none";
        Modal.closeButton.style.display = "none";
        Modal.element.style.display = "flex";

        // Remove tabs elsewhere
        document.querySelectorAll("*[tabindex]:not([disabled]), button:not([disabled]), input:not([disabled]), select:not([disabled]), a:not([disabled])").forEach(function(tabElement) {
            // Store the previous values of the tabindex
            // It will ensure that the default value is '0' which means it is navigated
            // in the default order.
            tabElement.setAttribute("data-tabindex", tabElement.getAttribute("tabindex") || "");

            // Set tabindex to -1 so that it is not able to be tabbed to
            tabElement.setAttribute("tabindex", "-1");
        });

        // Focus on modal
        Modal.element.focus();

        Util.get(url, function(html) {
            Modal.content.innerHTML = html;
            if (options.display) {
                Modal.content.style.display = options.display;
            }
            else {
                Modal.content.style.display = "flex";
            }

            if (options.height) {
                Modal.content.style.height = options.height;
            }
            else {
                Modal.content.style.height = "";
            }

            Modal.closeButton.style.display = "block";
            Modal.content.style.top = (document.body.clientHeight - Modal.content.clientHeight)/2 + "px";
            Modal.element.querySelector(":scope > .close").style.top = ((document.body.clientHeight - Modal.content.clientHeight)/2 + 2) + "px";

            Object.keys(options).forEach( (key) => {
                Modal.content.setAttribute('data-modal-' + key, options[key]);
            });

            Occam.loadAll(Modal.content);

            // If there is an 'data-modal-check-errors' then we can submit this
            // form asynchronously and look for 422 responses and redirect to
            // the URL on success.
            let submitButton = Modal.content.querySelector('input[type="submit"][data-modal-check-errors]');
            if (submitButton) {
                submitButton.addEventListener('click', (event) => {
                    event.stopPropagation();
                    event.preventDefault();

                    let errorsList = Modal.content.querySelector('.card.errors');

                    let form = Util.getParents(submitButton, "form")[0];

                    submitButton.setAttribute("disabled", "");

                    // Show the submitting card, if exists
                    let formCard = form.querySelectorAll(".card:not(:last-child):not(.submitting):not(.errors)");

                    let submittingCard = Modal.content.querySelector('.card.submitting');
                    if (submittingCard && formCard) {
                        submittingCard.style.height = formCard[0].clientHeight + "px";
                        submittingCard.removeAttribute("hidden");
                        formCard.forEach( (card) => {
                            card.setAttribute('hidden', '');
                        });
                    }

                    Util.submitForm(form, null, {
                        onerror: (data) => {
                            submitButton.removeAttribute("disabled");

                            if (submittingCard && formCard) {
                                submittingCard.setAttribute("hidden", "");
                                formCard.forEach( (card) => {
                                    card.removeAttribute('hidden');
                                });
                            }

                            // Show the errors block
                            if (errorsList) {
                                errorsList.removeAttribute("hidden");

                                // Clear existing errors
                                let errors = errorsList.querySelectorAll("li.error");
                                errors.forEach( (errorElement) => {
                                    errorElement.remove();
                                });

                                // Add the errors
                                data.errors.forEach( (error) => {
                                    let errorListing = errorsList.querySelector("ul");

                                    let newError = document.createElement("li");
                                    newError.classList.add("error");
                                    newError.textContent = error;
                                    errorListing.appendChild(newError);
                                    newError.style.transition = "none";
                                    newError.style.opacity = "0";

                                    // Allows the transition to see the resolution of a frame at 0
                                    window.requestAnimationFrame( () => {
                                        // And then in the next frame, starts the actual animation:
                                        newError.style.transition = "";
                                        newError.style.opacity = 1.0;
                                    });
                                });
                            }
                        },
                        onload: (data) => {
                            window.location.href = data.url;
                        },
                    }, 'application/json');
                });
            }

            if (Modal.onSubmit) {
                Modal.content.querySelectorAll("form").forEach(function(form) {
                    form.addEventListener("submit", function(event) {
                        Modal.onSubmit(event);
                    });
                });
            }
            else if (options.onSubmit) {
                Modal.content.querySelectorAll("form").forEach(function(form) {
                    form.addEventListener("submit", function(event) {
                        options.onSubmit(event);
                    });
                });
            }
        }, null, Object.assign({ modal: "true" }, options.query || {}));
    }

    static close() {
        // Reset tabs elsewhere
        document.querySelectorAll("*[tabindex], input, select, a").forEach(function(tabElement) {
            // Restore the previously saved tabindexes
            // If, for some reason, that value is not available, this will set it to -1
            tabElement.setAttribute("tabindex", tabElement.getAttribute("data-tabindex") || "-1");
        });

        // Remove modal
        Modal.element.style.display = "none";
    }
}

Modal._count = 0;
Modal._loaded = {};

export default Modal;
