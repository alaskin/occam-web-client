"use strict";

import EventComponent     from './event_component.js';
import AutoComplete       from './auto_complete.js';
import Tabs               from './tabs.js';
import RunList            from './run_list.js';
import ConfigurationPanel from './configuration_panel.js';
import Terminal           from './terminal.js';
import OccamObject        from './occam_object.js';
import Util               from './util.js';
import Occam              from './occam.js';
import Modal              from './modal.js';

import { Workflow as WorkflowWidget } from '../workflow-widget/workflow.js';
import { Palette } from '../workflow-widget/palette.js';
import Node from '../workflow-widget/node.js';

// TODO: accept a lack of a 'position' in imported json
// TODO: accept a lack of 'inputs' and 'outputs'

/**
 * This class represents any Workflow component.
 *
 * A workflow pane consists of at least a Workflow widget and possibly a set of
 * sidebars for selecting objects, viewing configurations, and viewing job logs.
 */
class Workflow extends EventComponent {
    constructor(element) {
        super();

        var tabElement = element.previousElementSibling;
        if (tabElement && tabElement.classList.contains("tabs")) {
            this.tabs = Tabs.load(tabElement);
        }

        this.configurationPanel = ConfigurationPanel.loadAll(element)[0];

        var index = Occam.object().index.slice();
        index.push(0);
        this.workflowObject = new OccamObject(Occam.object().rootId, Occam.object().rootRevision, "workflow", "Main", undefined, index, Occam.object().link);

        this.element = element;
        //this.element.style.maxHeight = "0px";

        // Look for a run id
        this.runID = this.element.getAttribute("data-run-id");
        this.lastPoll = false;

        if (this.runID) {
            // This represents a run
            // The workflow should not be editable
            // And we need to get the run information
            this.runObject = new OccamObject(
                this.element.getAttribute("data-run-object-id"),
                this.element.getAttribute("data-run-object-revision")
            );

            this.runData(this.runObject, this.runID, (data) => {
                // Get the initial state (so we can react to changes since
                // the initial rendering)
                this.initialData = data;

                // Create a polling timer for updating the run (if it is not finished/failed)
                if (!data.run.failureTime && !data.run.finishTime) {
                    this.pollRunTimer = window.setInterval( () => {
                        if (this.lastPoll) {
                            window.clearInterval(this.pollRunTimer);
                        }

                        // Poll the run info and pass it to the workflow
                        this.pollRun();
                    }, Workflow.POLL_TIME);
                }

                this.initializeWidget();
            });
        }
        else {
            this.initializeWidget();
        }
    }

    static loadAll(element) {
        var zones = element.querySelectorAll('.content .workflow-widget-zone');

        zones.forEach( (zone) => {
            var options = Object.assign({}, WorkflowWidget.defaults);
            options.allowNodeDeletion = false;
            options.allowNodeMovement  = false;
            options.palette.createOnDrag = false;
            options.palette.allowSelections = true;
            var dummyPalette = new Palette(null, options);
            var info = {}
            var item = zone.querySelector("li.connection");
            if (item) {
                var subElement = item.querySelector("span.type");
                if (subElement) {
                    info.type = subElement.textContent;
                }
                subElement = item.querySelector("span.name");
                if (subElement) {
                    info.name = subElement.textContent;
                }
                subElement = item.querySelector("img.icon");
                if (subElement) {
                    info.icon = subElement.src;
                }
                ['inputs', 'outputs', 'ports:not(.inputs):not(.outputs)'].forEach( (portType) => {
                    item.querySelectorAll("ol." + portType + " > li").forEach( (port) => {
                        portType = portType.split(':')[0];
                        info[portType] = info[portType] || [];
                        let portInfo = {}

                        subElement = port.querySelector("span.name");
                        if (subElement) {
                            portInfo.name = subElement.textContent;
                        }

                        subElement = port.querySelector("span.type");
                        if (subElement) {
                            portInfo.type = subElement.textContent;
                        }

                        info[portType].push(portInfo);
                    });
                });

                console.log(info);

                dummyPalette.updateItem(zone, info);
            }
        });

        var workflows = element.querySelectorAll('.content occam-workflow');

        workflows.forEach( (element, index) => {
            var workflow = new Workflow(element, index);
        });
    }

    pollRun() {
        this.runData(this.runObject, this.runID, (data) => {
            this.updateRun(data);
        });
    }

    updateRun(data) {
        // Pass along job details to workflow
        Object.keys(data.nodes).forEach( (nodeIndex) => {
            let node = this.workflow.nodeAt(nodeIndex);

            data.nodes[nodeIndex].jobs.forEach( (job) => {
                node.updateJob(job);
            });
        });

        // Detect a change in the run status
        if (data.run.finishTime != this.initialData.run.finishTime) {
            this.trigger('change', data);
        }
        else if (data.run.failureTime != this.initialData.run.failureTime) {
            this.trigger('change', data);
        }

        if (data.run.finishTime || data.run.failureTime) {
            this.lastPoll = true;
            this.trigger('done', data);
        }

        // Also pass along relevant information to the job list panel
        var jobPanel = this.element.querySelector(".jobs.sidebar");
        if (jobPanel) {
            var jobsList = jobPanel.querySelector("ul.jobs");

            var nodeIndex = jobsList.getAttribute("data-node-index");
            if (nodeIndex) {
                if (data.nodes && data.nodes[nodeIndex] && data.nodes[nodeIndex].jobs) {
                    var runList = RunList.load(jobsList);
                    data.nodes[nodeIndex].jobs.forEach( (job, i) => {
                        // Update job information
                        var entry = runList.elementFor(i);
                        if (entry) {
                            runList.update(entry, job);
                        }
                    });
                }
            }
        }
    }

    runData(object, runID, callback) {
        Util.get(object.url({"path": "runs/" + runID}), (data) => {
            callback(data);
        }, "json");
    }

    jobList(object, runID, nodeIndex, callback) {
        Util.get(object.url({"path": "runs/" + runID + "/" + nodeIndex}), (html) => {
            callback(html);
        }, "text/html");
    }

    initializeWidget() {
        var options = {};

        options.buttons = [];

        if (this.runID || this.element.classList.contains("run")) {
            options.allowSelections     = false;
            options.allowNodeMovement   = false;
            options.allowNodeDeletion   = false;
            options.allowWireSelection  = false;
            options.allowNewConnections = false;
            options.buttons.push({
                classes: ["view-jobs-button"],
                inactive: {
                    icon: "/images/dynamic/hex/5999a6/ui/workflow-job-list.svg"
                },
                hover: {
                    icon: "/images/dynamic/hex/ffffff/ui/workflow-job-list.svg"
                }
            });
        }
        else {
            options.buttons.push({
                classes: ["view-button"],
                inactive: {
                    icon: "/images/dynamic/hex/5999a6/icons/ui/search.svg"
                },
                hover: {
                    icon: "/images/dynamic/hex/ffffff/icons/ui/search.svg"
                }
            });
            options.buttons.push({
                classes: ["configure-button"],
                inactive: {
                    icon: "/images/dynamic/hex/5999a6/icons/objects/configuration.svg"
                },
                hover: {
                    icon: "/images/dynamic/hex/ffffff/icons/objects/configuration.svg"
                }
            });
            // TODO: re-enable edit support
            /*options.buttons.push({
                classes: ["edit-button"],
                inactive: {
                    icon: "/images/dynamic/hex/5999a6/icons/ui/edit.svg"
                },
                hover: {
                    icon: "/images/dynamic/hex/ffffff/icons/ui/edit.svg"
                }
            });*/
        }

        this.workflow = new WorkflowWidget(this.element, options);
        var sidebar = this.element.querySelector(".sidebar li.connection");
        if (sidebar) {
            var selectionNode = Node.create(sidebar, this.workflow);

            var empty = !this.element.querySelector('.input.start');

            this.sidebar  = this.element.querySelector('.sidebar:not(.right)');
            this.sidebar2 = this.element.querySelector('.sidebar.right');
        }

        this.workflow.on("button-click", (event) => {
            if (event.element.classList.contains("configure-button")) {
                this.showConfigurations();
                this.configurationPanel.loadConfiguration(event.node.index, this.workflowObject, 
                    new OccamObject(event.node.element.getAttribute("data-object-id"), event.node.element.getAttribute("data-object-revision"))
                );
            }
            else if (event.element.classList.contains("view-button")) {
                // Open the view modal
                let objectURL = "";
                objectURL += "/" + event.node.element.getAttribute("data-object-id");
                objectURL += "/" + event.node.element.getAttribute("data-object-revision");
                Modal.open(objectURL, { large: true, display: "flex", height: "90%" });
            }
            else if (event.element.classList.contains("edit-button")) {
                // Open the edit modal
                let objectURL = "";
                objectURL += "/" + event.node.element.getAttribute("data-object-id");
                objectURL += "/" + event.node.element.getAttribute("data-object-revision");
                objectURL += "/file";
                Modal.open(objectURL, { large: true, display: "flex", height: "90%", query: { full: "true", fileListing: "closed" } });
            }
            else if (event.element.classList.contains("view-jobs-button")) {
                var jobPanel = this.element.querySelector(".jobs.sidebar");

                if (jobPanel) {
                    // Ensure the jobs list panel is open.
                    jobPanel.classList.remove("reveal");

                    // Tell others that the sidebar is shown.
                    this.trigger("sidebar");

                    // Make sure the loading icon is displayed
                    var jobsList = jobPanel.querySelector("ul.jobs");
                    var loadingDiv = jobPanel.querySelector(".loading");
                    var runList = RunList.load(jobsList);
                    runList.clear();

                    // Destroy existing terminal
                    var terminalElement = this.element.querySelector("*:not(template) > .terminal.job-viewer");
                    if (terminalElement) {
                        terminalElement.remove();
                    }

                    // Update object node visualization
                    var node = jobPanel.querySelector(".connection.dummy");

                    // Prevent it from being colored as the default 'help' colors
                    node.classList.remove("initial");

                    // Update name and icon
                    var name = event.node.name;
                    node.querySelector(".name").textContent = name;
                    var type = event.node.type;
                    node.querySelector(".type").textContent = type;
                    var icon = event.node.icon;
                    if (icon) {
                        node.querySelector("img.icon").src = icon;
                    }

                    if (!loadingDiv) {
                        loadingDiv = document.createElement("div");
                        loadingDiv.classList.add("loading");
                        jobsList.appendChild(loadingDiv);
                    }

                    jobsList.setAttribute("data-node-index", event.node.index);

                    // Populate the jobs list
                    this.jobList(this.runObject, this.runID, event.node.index, (html) => {
                        loadingDiv.remove();

                        runList.loadHTML(html);

                        runList.on("change", (item) => {
                            var jobID = item.getAttribute("data-job-id");

                            // Destroy existing terminal
                            var terminalElement = this.element.querySelector("*:not(template) > .terminal.job-viewer");
                            if (!terminalElement) {
                                // Clone template terminal
                                var template = this.element.querySelector("template.job-terminal");
                                var newTerminal = null;
                                if ('content' in template) {
                                    newTerminal = document.importNode(template.content, true);
                                    newTerminal = newTerminal.querySelector("div");
                                }
                                else {
                                    newTerminal = template.querySelector("div").cloneNode(true);
                                }

                                if (newTerminal) {
                                    template.parentNode.insertBefore(newTerminal, template.nextElementSibling);

                                    terminalElement = newTerminal;
                                }
                            }

                            // Update terminal
                            if (terminalElement) {
                                terminalElement.setAttribute("data-job-id", jobID);
                                var terminal = Terminal.load(terminalElement);
                                terminal.reset();
                                terminal.open();

                                runList.on("action.fullscreen", (item) => {
                                    // Fullscreen the terminal
                                    if (terminalElement.requestFullScreen) {
                                        terminalElement.requestFullscreen();
                                    }
                                    else if (terminalElement.mozRequestFullscreen) {
                                        terminalElement.mozRequestFullscreen();
                                    }
                                    else if (terminalElement.webkitRequestFullscreen) {
                                        terminalElement.webkitRequestFullscreen();
                                    }
                                    else {
                                        terminalElement.style.position = "fixed";
                                        terminalElement.style.left = 0;
                                        terminalElement.style.right = 0;
                                        terminalElement.style.top = 0;
                                        terminalElement.style.bottom = 0;
                                        terminalElement.style.zIndex = 999999;
                                        terminalElement.style.height = "100%";
                                        terminalElement.style.width  = "100%";
                                    }

                                    terminalElement.onfullscreenchange =
                                        terminalElement.onwebkitfullscreenchange =
                                        terminalElement.onmozfullscreenchange = 
                                        terminalElement.MSFullscreenChange = (event) => {
                                            terminalElement.classList.toggle("fullscreen");
                                        };

                                    terminalElement.focus();
                                });
                            }
                        });
                    });
                }
            }
        });

        this.workflow.on("node-removed", (event) => {
            var node = event.node;
            var element = event.element;

            var index = parseInt(element.getAttribute('data-index'));

            // Remove configuration tab
            //this.configurationTabs.removeTab(index);
        });

        this.connections = this.element.querySelector(":scope > ul.connections");
        this.draggable   = this.connections;

        if (this.element.classList.contains("editable")) {
            var blah = this.sidebar.querySelector('h2:first-child');
            blah.addEventListener('click', (event) => {
                this.exportJSON();
            });
            this.setupSaveButton();

            this.connectToConfigurationTabs();
            this.applySidebarEvents();
        }

        if (this.element.querySelector(".sidebar")) {
            this.initializeSidebar();
        }

        // Load from the experiment
        if (!this.element.classList.contains("mock")) {
            if (Occam.object() && Occam.object().type == "experiment") {
                Occam.object().objectInfo( (info) => {
                    (info.contains || []).forEach( (item, index) => {
                        if (item.type == "workflow") {
                            this.loadFrom(this.workflowObject);
                        }
                    });
                });
            }
        }
    }

    setupSaveButton() {
        this.save = this.tabs.element.querySelector('.save');

        if (this.save) {
            this.save.addEventListener('click', (event) => {
                var experiment_data = this.exportJSON();

                var url = Occam.object().url({path: "/0/files/data.json", query: { commit: true }});

                // POST the new workflow data
                Util.post(url, experiment_data, {
                    onload: (metadata) => {
                        var query = (window.location.href.split("?", 2)[1] || "");
                        var newURL = metadata.url.split("?", 2)[0] + "/../../../workflow";
                        if (query) {
                            newURL += "?" + query;
                        }
                        window.location.replace(newURL);
                    },
                    onprogress: (event) => {
                        /*if (event.lengthComputable) {
                            fileUploadEntry.style.backgroundSize = Math.round((event.loaded / event.total) * 100, 2) + "% 100%";
                        }*/
                    }
                }, "json");
            });
        }
    }

    /**
     * This function gets references to the tab strip for configurations.
     */
    connectToConfigurationTabs() {
        /*var tabElement = this.element.parentNode.nextElementSibling.querySelector(':scope > .tabs');
        this.configurationTabs = Tabs.load(tabElement);*/
    }

    /**
     * This function shows the object selection sidebar, if obscured.
     */
    showObjectSelector() {
        if (this.objectSelectorSidebar) {
            this.objectSelectorSidebar.classList.remove("reveal");
            if (this.tabs) {
                this.tabs.detectSidebarState();
            }
        }
    }

    /**
     * This function hides the object selection sidebar, if shown.
     */
    hideObjectSelector() {
        if (this.objectSelectorSidebar) {
            this.objectSelectorSidebar.classList.add("reveal");
            if (this.tabs) {
                this.tabs.detectSidebarState();
            }
        }
    }

    /**
     * This function shows the configuration sidebar, if obscured.
     */
    showConfigurations() {
        if (this.configurationSidebar) {
            this.configurationSidebar.classList.remove("reveal");
            if (this.tabs) {
                this.tabs.detectSidebarState();
            }
        }
    }

    /**
     * This function hides the configuration sidebar, if shown.
     */
    hideConfigurations() {
        if (this.configurationSidebar) {
            this.configurationSidebar.classList.add("reveal");
            if (this.tabs) {
                this.tabs.detectSidebarState();
            }
        }
    }

    /**
     * This function sets up the dynamic interactions with the sidebar.
     */
    initializeSidebar() {
        this.objectSelectorSidebar = this.element.parentNode.querySelector("occam-workflow .sidebar.object-select");
        this.configurationSidebar  = this.element.parentNode.querySelector("occam-workflow .sidebar.configuration");
    }

    loadObject(object) {
        // Stop dragging of node
        var objectSelected = this.sidebar.querySelector(".object-selected");
        objectSelected.classList.add("disabled");

        // Destroy contents of zone (if never loaded)
        if (!objectSelected.classList.contains("workflow-widget-zone")) {
            objectSelected.innerHTML = "";
        }

        // Ping site to add a recently-used link
        var currentPersonURL = "/people/" + document.body.querySelector("#username").getAttribute("data-identity-uri");
        var recentlyUsedURL = currentPersonURL + "/recentlyUsed";
        Util.post(recentlyUsedURL, {
            "object_id": object.uid,
            "object_revision": object.revision
        });

        // We will get the node information here
        let nodeInfo = {
            name: object.name,
            type: object.object_type,
            icon: object.icon,
            data: {
                "object-revision": object.revision,
                "object-id": object.uid
            }
        };

        // Load input/output pins
        var realized = new OccamObject(object.uid, object.revision, object.object_type);
        this.currentObject = realized;
        realized.objectInfo( (data) => {
            if (data.inputs) {
                nodeInfo.inputs = [];
                data.inputs.forEach( (input, i) => {
                    if (input.type == "configuration") {
                        input.visibility = "hidden";
                    }

                    nodeInfo.inputs.push(input);
                });
            }
            data.outputs = (data.outputs || []);
            data.outputs.unshift({
                "name": "self",
                "type": data.type
            });
            if (data.outputs) {
                nodeInfo.outputs = [];
                data.outputs.forEach( (output, i) => {
                    if (i == 0 && data.outputs.length > 1) {
                        // Hide the 'self' pin
                        output.visibility = "hidden";
                    }

                    nodeInfo.outputs.push(output);
                });
            }

            // Add the palette item
            this.workflow.palette.updateItem(objectSelected, nodeInfo);

            // Allow dragging
            objectSelected.classList.remove("disabled");
        });
    }

    /* This function will set up the events to attach the sidebar attach
     * button to the current input box.
     */
    applySidebarEvents() {
        // Close the sidebar when a node is clicked and dragged
        this.workflow.on("palette-node-drag", (_) => {
            this.sidebar.classList.add("reveal");
            if (this.tabs) {
                this.tabs.detectSidebarState();
            }
        });

        // General object autocomplete
        var autoCompleteType = AutoComplete.load(this.sidebar.querySelector('.auto-complete[name="type"]'));
        var autoCompleteElement = this.sidebar.querySelector('.auto-complete[name="name"]');
        var autoComplete = AutoComplete.load(autoCompleteElement);
        autoCompleteType.on("change", (event) => {
            autoComplete.clear();
        });

        autoComplete.on("change", (event) => {
            var object = {};
            var hidden         = autoCompleteElement.parentNode.querySelector(':scope > input[name="object-id"]');
            object.object_type = autoCompleteElement.getAttribute('data-object-type');
            object.revision    = autoCompleteElement.getAttribute('data-revision');
            object.icon        = autoCompleteElement.getAttribute('data-icon-url');
            object.name        = autoCompleteElement.value;
            object.uid         = hidden.value;

            this.loadObject(object);
        });

        this.sidebar.querySelectorAll('ul.object-list li').forEach((recentlyUsedItem) => {
            recentlyUsedItem.addEventListener('click', (event) => {
                var object = {};
                object.object_type = recentlyUsedItem.querySelector('h2 span.type, p.type').textContent.trim();
                object.revision    = recentlyUsedItem.getAttribute('data-object-revision');
                object.icon        = recentlyUsedItem.querySelector('img').getAttribute('src');
                object.name        = recentlyUsedItem.querySelector('h2 span.name').textContent.trim();
                object.uid         = recentlyUsedItem.getAttribute('data-object-id');

                this.loadObject(object);
            });
        });
    }

    loadFrom(obj) {
        // Place spinner on workflow
        this.workflow.element.classList.add("loading");
        console.log("getting info for", obj);
        obj.objectInfo( (info) => {
            if (info.file === undefined) {
                return;
            }

            obj.retrieveJSON(info.file, (data) => {
                this.workflow.element.classList.remove("loading");

                // Get workflow pan position
                var width  = this.workflow.element.offsetWidth;
                var height = this.workflow.element.offsetHeight;

                data.center = data.center || {"x": 0, "y": 0};

                // Center the workflow diagram upon the stored point
                // (The workflow's viewport position is the top left coordinate)
                var workflowLeft = data.center.x + (width  / 2);
                var workflowTop  = data.center.y + (height / 2);

                // For each node, add that node at its given position
                (data.connections || []).forEach( (nodeInfo, i) => {
                    var newNode = document.createElement("li");
                    newNode.classList.add("connection");
                    nodeInfo.position = nodeInfo.position || {"x": 0, "y": 0};
                    nodeInfo.position.x = nodeInfo.position.x || 0;
                    nodeInfo.position.y = nodeInfo.position.y || 0;

                    nodeInfo.data = {
                        "object-id": nodeInfo.id,
                        "object-revision": nodeInfo.revision
                    };

                    nodeInfo.icon = "/images/icons/for?object-type=" + nodeInfo.type;

                    // Add implicit 'self' output as index 0
                    nodeInfo.outputs.unshift(nodeInfo.self || {
                        hidden: true,
                        type: nodeInfo.type,
                        name: "self"
                    });

                    // Rewire indicies to account for 'self' wire
                    (nodeInfo.inputs || []).forEach( (port) => {
                        (port.connections || []).forEach( (wire) => {
                            if (wire.to instanceof Array) {
                                wire.to = {
                                    "node": wire.to[0],
                                    "port": wire.to[1],
                                    "wire": wire.to[2]
                                }
                            }
                            wire.to.port++;
                        });
                    });
                });

                this.workflow.fromJSON(data);

                if (this.initialData) {
                    this.updateRun(this.initialData);
                }
            });
        });
    }

    exportJSON() {
        // Attach this object to the workflow
        var workflowData = this.workflow.toJSON();

        // Mutate this into the appropriate structure
        var canonicalData = {};

        canonicalData.connections = [];
        workflowData.connections.forEach( (nodeData) => {
            var canonicalNode = {};

            canonicalNode.inputs = [];
            canonicalNode.position = {
                "x": nodeData.position.x,
                "y": nodeData.position.y
            };
            canonicalNode.name     = nodeData.name;
            canonicalNode.type     = nodeData.type;
            canonicalNode.id       = nodeData.data["object-id"];
            canonicalNode.revision = nodeData.data["object-revision"];

            (nodeData.inputs || []).forEach( (pinData, pinIndex) => {
                var canonicalPin = {};
                canonicalPin.connections = [];
                canonicalPin.name = pinData.name;
                canonicalPin.type = pinData.type;
                canonicalPin.visibility = pinData.visibility || "visible";
                console.log(pinData);
                (pinData.connections || []).forEach( (wireData) => {
                    var canonicalWire = {};
                    canonicalWire.to = [wireData.to.node, wireData.to.port - 1, wireData.to.wire];
                    if (canonicalWire.to[0] !== undefined) {
                        canonicalPin.connections.push(canonicalWire);
                    }
                });
                canonicalNode.inputs.push(canonicalPin);
            });

            canonicalNode.outputs = [];
            (nodeData.outputs || []).forEach( (pinData, pinIndex) => {
                // self pin
                var canonicalPin = {};
                canonicalPin.connections = [];
                canonicalPin.name = pinData.name;
                canonicalPin.type = pinData.type;
                canonicalPin.visibility = pinData.visibility || "visible";
                (pinData.connections || []).forEach( (wireData) => {
                    var canonicalWire = {};
                    canonicalWire.to = [wireData.to.node, wireData.to.port, wireData.to.wire];
                    if (canonicalWire.to[0] !== undefined) {
                        canonicalPin.connections.push(canonicalWire);
                    }
                });
                if(pinIndex == 0){
                    canonicalNode.self = canonicalPin;
                }
                else{
                    canonicalNode.outputs.push(canonicalPin);
                }
            });

            canonicalData.connections.push(canonicalNode);
        });

        var width  = this.workflow.element.clientWidth;
        var height = this.workflow.element.clientHeight;

        canonicalData.center = workflowData.center;

        return JSON.stringify(canonicalData);
    }

    /* This function is the event handler for when the "Attach" button is
     * clicked. It should add an "attach" event to the queue.
     */
    submitAttach(newActiveBox, container, workflow) {
        // Obviously, name and type might be wrong when somebody types something
        // weird in after selecting an object from the dropdown.

        var form = newActiveBox.find('form');

        // Get the hidden field with the object id requested to add
        var objectId = form.find(".object-id").val().trim();
        var objectRevision = form.find(".object-revision").val().trim();
        var connectionIndex = form.find("input[name=connection_index]").val().trim();
        var objectType = form.find("input[name=object_type]").val().trim();
        var objectName = form.find("input[name=object_name]").val().trim();

        Occam.object().queuePushAttach(connectionIndex, objectId, objectRevision, (success) => {
            window.console.log("Attach finished: " + success);
        });
    }
}

/**
 * The time in milliseconds between polling for updates in a run.
 */
Workflow.POLL_TIME = 2000;

export default Workflow;
