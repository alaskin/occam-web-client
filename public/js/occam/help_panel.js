"use strict";

import EventComponent from './event_component.js';
import Util from './util.js';

class HelpPanel extends EventComponent {
    constructor(element) {
        super();
        var self = this;

        if (element === undefined) {
            return;
        }

        // Turn this into a <BUTTON>
        this.element = Util.replaceTag(element, "button");

        if (HelpPanel.fakeCard === undefined) {
            // Fake card
            HelpPanel.fakeCard = document.createElement("div");
            HelpPanel.fakeCard.setAttribute("role", "presentation");
            HelpPanel.fakeCard.setAttribute("aria-hidden", "true");
            HelpPanel.fakeCard.classList.add("card");
            HelpPanel.fakeCard.style.opacity = 0;
            HelpPanel.fakeCard.style.position = "absolute";
            // Ensure that the help bubble isn't the first element so
            // the correct styling is applied (except for when the help bubble
            // *IS* the first element)
            HelpPanel.fakeCard.appendChild(document.createElement("div"));
            document.querySelector(".content").appendChild(HelpPanel.fakeCard);
        }

        HelpPanel._count++;
        this.element.setAttribute('data-help-index', HelpPanel._count);

        HelpPanel._loaded[this.element.getAttribute('data-help-index')] = this;

        this.bindEvents();
    }

    static loadAll(element) {
        var elements = element.querySelectorAll('.help-bubble');

        elements.forEach(function(element) {
            HelpPanel.load(element);
        });
    }

    static load(element) {
        if (element === undefined) {
            return null;
        }

        var index = element.getAttribute('data-help-index');

        if (index) {
            return HelpPanel._loaded[index];
        }

        return new HelpPanel(element);
    }

    bindEvents() {
        var self = this;

        // Help bubble expansion
        var helpText = this.element.nextElementSibling;
        if (helpText === null) {
            helpText = this.element.parentNode.nextElementSibling;
        }

        // Measure help text
        var dup = helpText.cloneNode(true);
        HelpPanel.fakeCard.appendChild(dup);

        dup.removeAttribute("hidden");
        dup.style.height = "";
        dup.style.minHeight = "";

        // Get the height
        var height = dup.clientHeight;

        dup.remove();

        helpText.style.minHeight = "0px";
        var initiallyHidden = helpText.getAttribute("hidden") !== null;
        if (initiallyHidden) {
            helpText.style.height = "0px";
            helpText.style.opacity = 0;
            helpText.classList.add("closed");
        }
        else{
            helpText.style.height = height + "px";
            helpText.style.opacity = 1;
            helpText.classList.add("open");
        }
        helpText.removeAttribute("hidden");

        // Add 'delete' button to help bubbles
        var deleteNode = document.createElement("div");
        deleteNode.classList.add("delete");
        helpText.insertBefore(deleteNode, helpText.children[0]);

        var openCloseHandler = function(event) {
            event.stopPropagation();
            event.preventDefault();

            if (!helpText.classList.contains("bound")) {
                // Make sure the help text has a static height to begin with

                helpText.style.minHeight = "0px";
                helpText.style.position = "";
                if (initiallyHidden) {
                    helpText.style.height = "0px";
                    helpText.classList.add("closed");
                }
                else{
                    helpText.style.height = height + "px";
                    helpText.style.opacity = 1;
                    helpText.classList.add("open");
                }

                helpText.classList.add("bound");
            }

            if (helpText.classList.contains("closed")) {
                // Show the help text
                helpText.style.height = height + "px";
                helpText.style.opacity = 1;
                helpText.style.borderTopWidth = "3px";
                helpText.style.borderBottomWidth = "1px";

                helpText.classList.remove("closed");
                helpText.classList.add("open");
                helpText.setAttribute("aria-hidden", "false");
                self.element.setAttribute("aria-expanded", "true");
            }
            else {
                // Hide the help text
                helpText.style.height = "0px";
                helpText.style.opacity = 0;
                helpText.style.borderWidth = 0;

                helpText.classList.remove("open");
                helpText.classList.add("closed");
                helpText.setAttribute("aria-hidden", "true");
                self.element.setAttribute("aria-expanded", "false");
            }
        };

        this.element.addEventListener("click", openCloseHandler);
        deleteNode.addEventListener("click", openCloseHandler);
    }
}

HelpPanel._loaded = {};
HelpPanel._count  = 0;

export default HelpPanel;
