Computing and programming is still a young discipline.
Over time, software can functionally deteriorate before the data is can read or manage becomes obsolete.
In the sciences, this degradation can influence the repeatability and reproducibility of computational results.
To this end, Occam serves as a digital software and data archive built for the very demanding and delicate needs of scientific work, but also general enough to cover the equally important software preservation needs of the arts and humanities.
