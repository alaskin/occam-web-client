Want to try it out yourself?
You can use our own instance at <https://occam.cs.pitt.edu>, however Occam is best used by installing the tools on your own machine or infrastructure.
You can refer to [this guide](/documentation/installation) for specific directions for your platform on how to install and use Occam.

Currently, Occam requires a Linux-based computer with [Docker](https://www.docker.com/) to use all of its features.
