# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Controller
    module Helpers
      module HashHelpers
        # These helper methods are for hash manipulation.

        # Returns a new hash with all keys converted to symbols, as long as they respond to to_sym.
        def symbolize_keys(hash)
          # Original source:
          # File activesupport/lib/active_support/core_ext/hash/keys.rb, line 50
          transform_keys(hash, :recursive => false, :enumerate => false){ |key| key.to_sym rescue key }
        end

        # Returns a new hash with all keys converted to symbols, recursively applied
        # to values that are also Hash types.
        def deep_symbolize_keys(hash)
          transform_keys(hash, :recursive => true, :enumerate => false){ |key| key.to_sym rescue key }
        end

        # Returns a new hash with all keys converted to symbols, recursively applied
        # to values that are also Hash types and enumerating arrays which may contain
        # elements that are also of type Hash.
        def deep_deep_symbolize_keys(hash)
          transform_keys(hash, :recursive => true, :enumerate => true){ |key| key.to_sym rescue key }
        end

        # Return a new hash with all keys converted using the block operation.
        def transform_keys(hash, options = {}, &block)
          # Original source:
          # File activesupport/lib/active_support/core_ext/hash/keys.rb, line 8
          result = {}
          hash.each_key do |key|
            item = hash[key]
            if item.is_a?(Hash) && options[:recursive]
              item = transform_keys(item, options, &block)
            elsif item.is_a?(Array) && options[:enumerate]
              item = item.map do |element|
                if element.is_a? Hash
                  transform_keys(element, options, &block)
                else
                  element
                end
              end
            end
            result[yield(key)] = item
          end
          result
        end
      end
    end

    helpers Helpers::HashHelpers
  end
end
