# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Controller
    module Helpers
      module LayoutHelpers
        # Retrieves the current Occam::Layout based on parameters.
        def current_layout
          Occam::Layout.new(:view   => (params.has_key?("embed") ? :embedded : (params.has_key?("minimal") ? :minimal : :desktop)),
                            :tab    => params[:tab],
                            :subtab => params[:subtab],
                            :modal  => !!params[:modal],
                            :ajax   => request.xhr?,
                            :webp   => request.env["HTTP_ACCEPT"].include?("image/webp"),
                            :ie     => request.env["HTTP_USER_AGENT"].include?("; MSIE ") || request.env["HTTP_USER_AGENT"].include?("Trident/7.0; "),
          )
        end

        # Retrieves the current Occam::System::Theme for the current session.
        def current_theme
          current_layout.theme
        end
      end
    end

    helpers Helpers::LayoutHelpers
  end
end
