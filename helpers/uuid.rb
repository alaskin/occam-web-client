# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Controller
    module Helpers
      # This module contains methods that help identify UUIDs and multihashes.
      module UUIDHelpers
        def isUUID(uuid)
          uuid.is_a?(String) and uuid.length == 36 and
            uuid[8]  == '-' and
            uuid[13] == '-' and
            uuid[18] == '-' and
            uuid[23] == '-'
        end

        def isHash(hash)
          hash.is_a?(String) &&
            ((hash.length == 46 && hash[0..1]  == 'Qm') ||
             (hash.length == 47 && hash[0..1]  == '6M'))
        end
      end
    end

    helpers Helpers::UUIDHelpers
  end
end
