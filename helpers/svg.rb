# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Controller
    module Helpers
      # This methods manipulate SVG content.
      module SVGHelpers
        # Returns safe HTML to embed an SVG into a page
        #
        # Taken from https://coderwall.com/p/d1vplg/embedding-and-styling-inline-svg-documents-with-css-in-rails
        # Thanks James Martin! I've added width/height setting and custom coloring.
        def embed_svg(path, options={})
          require 'nokogiri'
          url = path
          if path.start_with?("/")
            path = path[1..-1]
          end
          path = File.join("public", path)
          if !path.end_with?("svg")
            # Abort
            return "<img src='#{url}'>"
          end

          if options[:hex]
            options[:color] = "##{options[:hex]}"
          end

          svgData = File.read(path)
          doc = Nokogiri::HTML::DocumentFragment.parse svgData
          svg = doc.at_css 'svg'
          if svg.nil?
            return ""
          end

          if options[:class]
            svg['class'] = options[:class]
          end
          if options[:id]
            svg['id'] = options[:id]
          end
          if options[:width]
            svg['width'] = options[:width]
          end
          if options[:height]
            svg['height'] = options[:height]
          end
          if options[:color]
            svg.add_child("<style>rect, path, circle { fill: #{options[:color]} !important; }</style>")
          end
          doc.to_html
        end

        def imageURL(path, options = {})
          if not path.start_with?("/")
            path = "/" + path
          end

          if File.extname(path) == ".svg"
            basePath = "/images/dynamic"
            if options[:color]
              basePath = basePath + "/color/#{options[:color]}"
            elsif options[:hue]
              basePath = basePath + "/hue/#{options[:hue]}/sat/#{options[:sat]}/light/#{options[:light]}"
            elsif options[:hex]
              basePath = basePath + "/hex/#{options[:hex]}"
            else
              basePath = "/images"
            end
          else
            basePath = "/images"
          end

          "#{basePath}#{path}"
        end

        def svg(collection, tag, options = {})
          "<svg class=\"#{options[:class] || ""}\"><use xlink:href=\"/images/symbol/#{collection}.svg##{tag}\"></use></svg>"
        end
      end
    end

    helpers Helpers::SVGHelpers
  end
end
