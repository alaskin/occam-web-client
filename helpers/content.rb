# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Controller
    module Helpers
      module ContentHelpers
        def header(options = {})
          options[:alt_text] ||= I18n.t('alt-text.help')
          hidden = !((params["help"].to_s == options[:id].to_s) || options[:reveal])

          "<h2 #{options[:offscreen] ? "class=\"offscreen\" " : (options[:help] ? "class=\"has-help\" " : "")}id=#{options[:id]}>#{options[:text]}" +
            (options[:offscreen] ? "</h2>" : "") +
            (options[:help] ? "<a aria-expanded=\"false\" title=\"#{I18n.t('alt-text.help')}\" class=\"help-bubble inline#{options[:sidebar] ? " with-sidebar" : ""}\" href=\"?help=#{options[:id]}\">#{svg(:ui, :"info.small")}#{options[:alt_text]}</a>" : "") +
            (options[:offscreen] ? "" : "</h2>") +
            (options[:help] ? "<div class=\"help\" #{hidden ? "hidden" : ""}><p class='help-header'>#{svg(:ui, :info, :class => "icon")}</p>" +
                               markdown(nil, :markdown => options[:help]) +
                               "</div>" : "")
        end
      end
    end

    helpers Helpers::ContentHelpers
  end
end
