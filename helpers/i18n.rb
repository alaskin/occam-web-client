# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Controller
    module Helpers
      module I18nHelpers
        REPLACE = {
          "%login-link%" => "<a class=\"modal\" href=\"/login\">#{I18n.t("navigation.login")}</a>",
          "%signup-link%" => "<a class=\"modal\" href=\"/people/new\">#{I18n.t("navigation.signup")}</a>",
          "%current-person/storage%" => "<a href=\"/people/%current-person%/storage\">#{I18n.t("tabs.storage")}</a>",
          "%documentation/getting-started%" => "<a href=\"/documentation/getting-started\">Getting Started</a>"
        }

        def t(*args, **options)
          ret = I18n.t(*args, **options)

          if options[:replace]
            options[:replace].each do |k, v|
              ret.gsub!("%#{k}%", v)
            end
          end

          if ret.include?("%")
            REPLACE.each do |k,v|
              ret.gsub!(k, v)
            end

            if current_person
              ret.gsub!("%current-person%", current_person.id)
            end
          end

          ret
        end
      end
    end

    helpers Helpers::I18nHelpers
  end
end
