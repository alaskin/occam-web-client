# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'sinatra'

class Occam < Sinatra::Base
  module Config
    require 'yaml'

    @@config = nil
    @@prepared_config = nil
    def self.configuration(reload = false)
      if reload
        @@config = nil
        @@prepared_config = nil
      end

      if @@config.nil?
        occam_path = ENV['OCCAM_ROOT'] || File.join(ENV['HOME'], '.occam')
        config_file_path = File.join(occam_path, 'web-config.yml')
        unless File.exist?(config_file_path)

          if not File.exist?(File.dirname(config_file_path))
            puts "OCCAM not initialized. Run 'occam initialize'"
          end

          # Create a web-config file from the example web-config file
          config_sample_path = File.join(File.dirname(__FILE__), "..", "web-config-sample.yml")

          # Open and modify the sample configuration
          File.open(config_sample_path, "r") do |sample_file|
            # Read configuration file
            sample_config = sample_file.read()

            # Create a new secret key
            require 'securerandom'
            sample_config.gsub!(/^secret:.+$/, "secret: #{SecureRandom.hex(64)}")

            # Write configuration
            File.open(config_file_path, "w") do |f|
              f.write(sample_config)
            end
          end
        end

        @@config = YAML.load_file(config_file_path) || {}
      end

      if @@prepared_config.nil?
        config = @@config || {}

        # Set default values here
        config['host'] ||= "0.0.0.0"
        config['port'] ||= 9292
        config['allow-signup'] = true unless config['allow-signup'] == false
        config['daemon'] = {} unless config['daemon'].is_a? Hash
        config['daemon']['port'] ||= 32000
        config['daemon']['host'] ||= 'localhost'

        # Cache config
        @@prepared_config = config
      end

      @@prepared_config
    end
  end
end
