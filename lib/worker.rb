# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  module Worker
    def self.perform(component, command, arguments=[], options = {}, data=nil, promote = false, type="command")
      daemon = self.daemon
      if promote
        # Encourage the creation of a new daemon connection
        Thread.current[:occam_daemon] = nil
      end
      daemon.execute(component, command, arguments, options, data, promote)
    end

    def self.daemon
      if Thread.current[:occam_daemon].nil?
        Thread.current[:occam_daemon] = Occam::Daemon.new
      end

      Thread.current[:occam_daemon]
    end
  end
end
